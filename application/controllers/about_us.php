<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class About_us extends CI_Controller {
	public function index()
	{
        $css   = array('slick/slick.css','slick/slick-theme.css');
        $script=array('slick/slick.min.js');
        $lg  =$this->uri->segment(1);
        $page=$this->uri->segment(2);
        $data = array('lg'=>$lg,'page'=>$page,'content'=>'about_us_view','script'=>$script,'css'=>$css);
        
        $this->load->view('template',$data);

        }
        
        public function suppliers()
	{
        $css   = array('slick/slick.css','slick/slick-theme.css');
        $script=array('slick/slick.min.js');
        $lg  =$this->uri->segment(1);
        $page=$this->uri->segment(2);
        $data = array('lg'=>$lg,'page'=>$page,'content'=>'suppliers_view','script'=>$script,'css'=>$css);
        
        $this->load->view('template',$data);

        }
}
