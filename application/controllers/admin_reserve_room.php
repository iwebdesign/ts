<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_reserve_room extends CI_Controller{
  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('sess_admin')==false) {
          redirect('login_reserve_room');
          exit;

   }else{
          if($this->session->userdata('sess_admin')['level']!="admin"){
            redirect('login_reserve_room');
                exit;
          }
   }
    $this->load->library('grocery_crud');
    $this->load->library('image_crud');
    $this->load->model('ppr_room_model','room');
    $this->load->model('mail_seller_model','mail_seller');
  }

  public function index()
	{
	$this->_iweb_output((object)array('output' => '','namepage'=>'DASHBOARD' ,'title_page'=>'Dashboard','dashboard'=>'idashboard', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));
  }

  public function _iweb_output($output = null)
  {
    $this->load->view('admin-reserve-room/template',(array)$output);
  }

  public function _iframe_output($output = null)
  {
    $this->load->view('admin-reserve-room/template_iframe',(array)$output);
  }

  public function _gallery_output($output = null)
  {
    $this->load->view('gallery.php',$output);
  }

  public function backup_db(){
      // error_reporting(0);
      $tables = $this->db->list_tables();
      $connection = mysqli_connect('localhost',$this->db->username,$this->db->password,$this->db->database);
      mysqli_set_charset($connection,"utf8");
      $tables = $this->db->list_tables();
      $result = mysqli_query($connection,"SHOW TABLES");
      while($row = mysqli_fetch_row($result)){
        $tables[] = $row[0];
      }

      $return = '';
      foreach($tables as $table){
        $result = mysqli_query($connection,"SELECT * FROM "."`".$table."`");
        $num_fields = mysqli_num_fields($result);

        // $return .= 'DROP TABLE '.$table.';';
        $row2 = mysqli_fetch_row(mysqli_query($connection,"SHOW CREATE TABLE  "."`".$table."`"));
        $return .= "\n\n".$row2[1].";\n\n";

        for($i=0;$i<$num_fields;$i++){
          while($row = mysqli_fetch_row($result)){
            $return .= "INSERT INTO "."`".$table."`"." VALUES(";
            for($j=0;$j<$num_fields;$j++){
              $row[$j] = addslashes($row[$j]);
              if(isset($row[$j])){ $return .= '"'.$row[$j].'"';}
              else{ $return .= '""';}
              if($j<$num_fields-1){ $return .= ',';}
            }
            $return .= ");\n";
          }
        }
        $return .= "\n\n\n";
      }

      $pathfile = "./downloads/backup-".date('dmyHis').".sql";
      $namefile = "backup-".date('dmyHis').".sql";
      //save file
      $handle = fopen("./downloads/".$namefile,"w+");
      fwrite($handle,$return);
      fclose($handle);
      // echo "Successfully backed up";
  $this->load->helper('download');
  $ipath = file_get_contents($pathfile); // get file name
  $iname = $namefile; // new name for your file
  force_download($iname, $ipath); // start download`

    }

    public function project_reserve_room(){
                  $crud = new grocery_CRUD();
                  $crud->set_theme('bootstrap-v4');
                  $crud->set_table('project_reserve_room');
                  $crud->set_subject('');
                  $crud->display_as('room_code','รหัสห้อง');
                  $crud->display_as('prr_thumbnail','รูปภาพ (Thumbnail) 600x400px');
                  $crud->display_as('prr_picture','รูปภาพ 600x400px');
                  $crud->display_as('microsite_id','โครงการ');
                  $crud->display_as('room_price','ราคาห้อง');
                  $crud->display_as('room_deposit','ราคาจองห้อง');

                  $crud->display_as('room_discount','ส่วนลด');
                  $crud->display_as('room_floor','ชั้น');
                  $crud->display_as('room_floor_picture','ภาพชั้น 600x600px');
                  $crud->display_as('room_unit_type','Unit Type');
                  $crud->display_as('room_unit_type_picture','รูปภาพ Unit Type 600x600px');

                  $crud->display_as('room_design','รหัสแบบห้อง');
                  $crud->display_as('room_bed_number','จำนวนเตียง');
                  $crud->display_as('room_restroom_number','จำนวนห้องน้ำ');

                  $crud->display_as('room_size','ขนาดห้อง');
                  $crud->display_as('room_position','ตำแหน่งของห้อง');
                  $crud->display_as('room_status','สถานะของห้อง');

                  $crud->columns('slide_room','microsite_id','room_code','room_floor','prr_thumbnail','room_price','room_status');
                  $crud->callback_column('slide_room',array($this,'_callback_slide_room'));
                  $crud->set_relation('microsite_id','microsite','name_th');
                  $crud->set_relation('room_status','room_status','status_name');
                  $crud->set_field_upload('prr_thumbnail','filemanager/files/room');
                  $crud->set_field_upload('prr_picture','filemanager/files/room');
                  $crud->set_field_upload('room_floor_picture','filemanager/files/room');
                  $crud->set_field_upload('room_unit_type_picture','filemanager/files/room');
                  $crud->callback_read_field('prr_thumbnail', function ($value, $primary_key) {
                  if($value!=""){
                      $m = '<a data-fancybox="images" href="'.base_url('filemanager/files/room'.'/'.$value).'"  ><img src="'.base_url('filemanager/files/room'.'/'.$value).'" style="max-width:200px"></a>';
                       }else{
                      $m = "-";
                      }
                      return $m;
                  });

                  $crud->callback_read_field('prr_picture', function ($value, $primary_key) {
                    if($value!=""){
                        $m = '<a data-fancybox="images" href="'.base_url('filemanager/files/room'.'/'.$value).'"  ><img src="'.base_url('filemanager/files/room'.'/'.$value).'" style="max-width:200px"></a>';
                          }else{
                        $m = "-";
                        }
                    return $m;
                  });
                  $crud->unset_export();
                  $crud->unset_print();

                  $output = $crud->render();
                  $this->_iweb_output($output);
    }

    public function _callback_slide_room($value, $row)
    {
      $num = $this->db->get_where('slide_room',array('prr_id'=>$row->prr_id))->num_rows();
      return "<a id='slide-".$row->prr_id."' data-fancybox=''  data-type='iframe' class='btn btn-info' href='javascript:;' data-src='".site_url('admin_reserve_room/slide_room/'.$row->prr_id)."'> <span class='glyphicon glyphicon-edit'></span> เพิ่ม slide <span class='badge badge-light num-slide'>".$num."</span></a>";
    }

    public function slide_room($prrID){
      $p  = $this->db->get_where('project_reserve_room',array('prr_id'=>$prrID))->row();
      $mi = $this->db->get_where('microsite',array('id'=>$p->microsite_id))->row();

      $crud = new grocery_CRUD();
      $crud->set_theme('bootstrap-v4');
      $crud->set_table('slide_room');
      $crud->where('prr_id',$prrID);
      $crud->set_subject('');
      $crud->unset_export();
      $crud->unset_print();
      $crud->unset_columns('prr_id');
      // $crud->set_field_upload('picture_mobile','filemanager/files/room');
      $crud->set_field_upload('picture','filemanager/files/room');
      // $crud->callback_column('status',array($this,'_callback_column_status'));
      $crud->field_type('prr_id', 'hidden',$prrID);
      $output = $crud->render();
      $output->title_page = "โครงการ : ".$mi->name_th." <font color='magenta'>< ห้อง ".$p->room_code."> </font>";
      $this->_iframe_output($output);
    }

    public function checkNumSlideRoom(){
     if($this->input->post()):
      $rid = $this->input->post('id');
      $id  = explode('-',$rid);
     echo  $this->room->getNumSlideRoom($id[1]);
     else:
      echo "not access";
     endif;
    }

    public function reserve_room(){
      $crud = new grocery_CRUD();
      $crud->set_theme('bootstrap-v4');
      $crud->set_table('reserve_room');
      $crud->set_subject('รายการสั่งจองห้อง');
      $crud->unset_columns('fail_datetime','queue','reserve_filename','reserve_message');
      // $crud->unset_fields('project_reserve_id');
      $crud->unset_export();
      $crud->unset_print();
      $crud->unset_delete();
      $crud->unset_add();
      $crud->unset_edit();
      //$crud->unset_read();
      $output = $crud->render();
      // $output->title_page = $p->name_th."<br/>รหัสโครงการ : ".$p->project_code;
      $this->_iweb_output($output);
    }


    public function prr_slide($micrositeId=null){

      // echo "test";
      $mi = $this->db->get_where('microsite',array('id'=>$micrositeId))->row();

      $crud = new grocery_CRUD();
      $crud->set_theme('bootstrap-v4');
      $crud->set_table('prr_slide');
      $crud->where('microsite_id',$micrositeId);
      $crud->set_subject('');
      $crud->unset_export();
      $crud->unset_print();
      $crud->unset_columns('microsite_id');
      $crud->set_field_upload('picture_mobile','filemanager/files/room');
      $crud->set_field_upload('picture','filemanager/files/room');
      $crud->callback_column('status',array($this,'_callback_column_status'));
      $crud->field_type('microsite_id', 'hidden',$micrositeId);
      $output = $crud->render();
      $output->title_page = "โครงการ : ".$mi->name_th;
      $this->_iframe_output($output);

    }


    public function _callback_column_status($value, $row)
    {
      $page=$this->uri->segment(3);
      if($value=="open"){
        $checked = "checked";
      }else{
        $checked = "";
      }

      if($page=="category_products" || $page=="category_news"){
        $id = $row->category_id;
      }elseif($page=="list_responsibility"){
        $id = $row->res_id;
      }elseif($page=="reserve_rights"){
          $id = $row->project_reserve_id;
        }
      else{
        $id = $row->id;
      }
      $m = '<label class="switch isw" data-id="'.$id.'" data-page="'.$page.'"><input type="checkbox" '.$checked.' class="success"><span class="slider round"></span></label>';
      return $m;
    }


    public function contact_customer(){
      $crud = new grocery_CRUD();
      $crud->set_theme('bootstrap-v4');
      $crud->set_table('contact_customer');
      if($this->input->get()){
      $crud->where('contact_customer.microsite_id',$this->input->get('microsite_id'));
      }
      $crud->set_subject('');
      $crud->display_as('name','ชื่อ - นามสกุล');
      $crud->display_as('email','อีเมล');
      $crud->display_as('message','ข้อความ');
      $crud->display_as('phone','เบอร์โทร');
      $crud->display_as('microsite','โครงการ');
      $crud->display_as('send_datetime','วันและเวลาส่ง');
      $crud->display_as('prr_id','ส่งจากห้อง');
      $crud->display_as('read_status','สถานะ');
      $crud->set_relation('prr_id','project_reserve_room','room_code');
      $crud->unset_columns('microsite_id');
      $crud->unset_fields('read_status');
        if($this->input->get()){
          $crud->add_action('Read', '', '','ui-icon-image',array($this,'link_read'));
       }
      $crud->callback_column('read_status',array($this,'_callback_read_mail'));
      $crud->callback_read_field('read_status', function ($value, $primary_key) {
        $icon = $value==1?'<font class="text-success h3"><i class="fas fa-envelope-open"></i></font>':'<font class="text-danger h3"><i class="fas fa-envelope"></i></font>';
        return $icon;
           });
      $crud->unset_print();
      $crud->unset_add();
      $crud->unset_edit();
      $crud->unset_delete();

      $output = $crud->render();

      $state = $crud->getState();
      $state_info = $crud->getStateInfo();

      if($state == 'read')
        {
             $primary_key = $state_info->primary_key;
             $this->db->where('id',$primary_key);
             $this->db->update('contact_customer',array('read_status'=>'1'));
        }
       if($this->input->get()){
        $mi = $this->db->get_where('microsite',array('id'=>$this->input->get('microsite_id')))->row();
        $output->title_page = "โครงการ : ".$mi->name_th;


        $this->_iframe_output($output);
        }else{
        $this->_iweb_output($output);
        }

    }

      function link_read($primary_key , $row)
      {
          return site_url('admin_reserve_room/contact_customer/read/'.$primary_key).'?microsite_id='.$row->microsite_id;
      }


    public function _callback_read_mail($value, $row)
    {
      $icon = $value==1?'<font class="text-success h3"><i class="fas fa-envelope-open"></i></font>':'<font class="text-danger h3"><i class="fas fa-envelope"></i></font>';
      return $icon;
    }




    public function ajax_sw(){

      if($this->input->post()){

        $status = $this->input->post('status');
        $id     = $this->input->post('id');
        $page   = $this->input->post('page');
        if($page=="category_products"|| $page=="category_news"){
          $this->db->where('category_id',$id);
          $this->db->update($page,array('category_status'=>$status));
        }elseif($page=="list_responsibility"){
          $this->db->where('res_id',$id);
          $this->db->update('list_responsibility',array('status'=>$status));
        }elseif($page=="reserve_rights"){
          $this->db->where('project_reserve_id',$id);
          $this->db->update('project_reserve',array('status'=>$status));
        }else{
          $this->db->where('id',$id);
          $this->db->update($page,array('status'=>$status));
        }

        echo "success";
      }

    }

    public function ajaxMode(){
    if($this->input->post()){
    $mode = $this->input->post('mode');
    $data = array(
                   'mode' => $mode,
                );
    $this->db->where('id', '1');
    $this->db->update('dev_mode', $data);
    echo "success";
    }

    }

    public function ajaxSelectSeller(){

      // var_dump(json_encode($this->input->post('seller')));
      if($this->input->post('seller')!=false):
      $seller = json_encode($this->input->post('seller'));
      else:
      $seller = "";
      endif;
      $id = $this->input->post('id');
      $s =$this->db->get_where('seller_room',array('microsite_id'=>$id))->row();
      if($s){
       $this->db->where('microsite_id',$id);
       $this->db->update('seller_room',array('seller'=>$seller));
      }else{
        $this->db->insert('seller_room',array('microsite_id'=>$id,'seller'=>$seller));
      }

      // $this->db->update()
    }


    public function project_information_room(){

      $mi = $this->db->get_where('microsite',array('id'=>$this->uri->segment(5)))->row();
      $num= $this->db->get_where('project_information_room',array('microsite_id'=>$this->uri->segment(5)))->num_rows();

      if($num < 1 ){
         $sql = $this->db->insert('project_information_room',array('microsite_id'=>$this->uri->segment(5)));

         if($sql){
           $crud = new grocery_CRUD();
           $crud->set_theme('bootstrap-v4');
           $crud->set_table('project_information_room');
           $crud->set_subject('');
           $crud->unset_export();
           $crud->unset_print();
           $crud->field_type('microsite_id', 'hidden',$this->uri->segment(5));
           $crud->set_relation('bank','setting_bank','bank_name');
           $crud->set_relation('receipt','receipt_reserve','receipt_name');
           $crud->callback_edit_field('bank', function ($value, $primary_key) {
             $b = $this->db->get('setting_bank')->result();
             if($value!=0){
                   $g = $this->db->get_where('setting_bank',array('bank_id'=>$value))->row();
                   if($g!=false){
                     $pic = '<a href="'.base_url('filemanager/files/bank/'.$g->bank_picture).'" data-fancybox="images" data-caption="'.$g->bank_name.'"><img width="300" src="'.base_url('filemanager/files/bank/'.$g->bank_picture).'"></a>';
                   }else{
                     $pic = "";
                   }
                 }else{
                   $pic = "";
                 }
                     $m  = '<select class="see-bank" id="seeBank" name="bank" style="min-width:300px;">';
                     foreach($b as $r):
                       $selected = $r->bank_id==$value?"selected":"";
                       $m .= '<option value="'.$r->bank_id.'" '.$selected.'>'.$r->bank_name.'</option>';
                     endforeach;
                     $m .= '</select>';
                     $m .= '<div class="show-bank">'.$pic.'</div>';

                     return $m;
              });


              $crud->callback_edit_field('receipt', function ($value, $primary_key) {
                $b = $this->db->get('receipt_reserve')->result();
                if($value!=0){
                      $g = $this->db->get_where('receipt_reserve',array('receipt_id'=>$value))->row();
                      if($g!=false){
                        $pic = '<a href="'.base_url('filemanager/files/'.$g->receipt_picture).'" data-fancybox="images" data-caption="'.$g->receipt_name.'"><img width="300" src="'.base_url('filemanager/files/'.$g->receipt_picture).'"></a>';
                      }else{
                        $pic = "";
                      }
                    }else{
                      $pic = "";
                    }
                        $m  = '<select class="see-receipt" id="seeReceipt" name="receipt" style="min-width:300px;">';
                        foreach($b as $r):
                          $selected = $r->receipt_id==$value?"selected":"";
                          $m .= '<option value="'.$r->receipt_id.'" '.$selected.'>'.$r->receipt_name.'</option>';
                        endforeach;
                        $m .= '</select>';
                        $m .= '<div class="show-receipt">'.$pic.'</div>';

                        return $m;
                 });



           $output = $crud->render();
           $output->title_page = "โครงการ : ".$mi->name_th;
           $this->_iframe_output($output);
         }
      }else{
        $crud = new grocery_CRUD();
        $crud->set_theme('bootstrap-v4');
        $crud->set_table('project_information_room');
        $crud->set_subject('');
        $crud->unset_export();
        $crud->unset_print();
        $crud->field_type('microsite_id', 'hidden',$this->uri->segment(5));
        $crud->set_relation('bank','setting_bank','bank_name');
        $crud->set_relation('receipt','receipt_reserve','receipt_name');
        $crud->callback_edit_field('bank', function ($value, $primary_key) {
          $b = $this->db->get('setting_bank')->result();
          if($value!=0){
                $g = $this->db->get_where('setting_bank',array('bank_id'=>$value))->row();
                if($g!=false){
                  $pic = '<a href="'.base_url('filemanager/files/bank/'.$g->bank_picture).'" data-fancybox="images" data-caption="'.$g->bank_name.'"><img width="300" src="'.base_url('filemanager/files/bank/'.$g->bank_picture).'"></a>';
                }else{
                  $pic = "";
                }
              }else{
                $pic = "";
              }
                  $m  = '<select class="see-bank" id="seeBank" name="bank" style="min-width:300px;">';
                  foreach($b as $r):
                    $selected = $r->bank_id==$value?"selected":"";
                    $m .= '<option value="'.$r->bank_id.'" '.$selected.'>'.$r->bank_name.'</option>';
                  endforeach;
                  $m .= '</select>';
                  $m .= '<div class="show-bank">'.$pic.'</div>';

                  return $m;
           });


           $crud->callback_edit_field('receipt', function ($value, $primary_key) {
             $b = $this->db->get('receipt_reserve')->result();
             if($value!=0){
                   $g = $this->db->get_where('receipt_reserve',array('receipt_id'=>$value))->row();
                   if($g!=false){
                     $pic = '<a href="'.base_url('filemanager/files/'.$g->receipt_picture).'" data-fancybox="images" data-caption="'.$g->receipt_name.'"><img width="300" src="'.base_url('filemanager/files/'.$g->receipt_picture).'"></a>';
                   }else{
                     $pic = "";
                   }
                 }else{
                   $pic = "";
                 }
                     $m  = '<select class="see-receipt" id="seeReceipt" name="receipt" style="min-width:300px;">';
                     foreach($b as $r):
                       $selected = $r->receipt_id==$value?"selected":"";
                       $m .= '<option value="'.$r->receipt_id.'" '.$selected.'>'.$r->receipt_name.'</option>';
                     endforeach;
                     $m .= '</select>';
                     $m .= '<div class="show-receipt">'.$pic.'</div>';

                     return $m;
              });


        $output = $crud->render();
        $output->title_page = "โครงการ : ".$mi->name_th;
        $this->_iframe_output($output);

      }

    }

    public function extra_room(){
    $this->_iweb_output((object)array('output' => 'component_extra_room','control'=>'no','namepage'=>'ห้องเปิดขายแบบพิเศษ' ,'title_page'=>'Extra Room', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));
    }

    public function ajaxPicBank(){
       $bankid =  $this->input->post('bank_id');

      $b=$this->db->get_where('setting_bank',array('bank_id'=>$bankid))->row();

      echo '<a href="'.base_url('filemanager/files/bank/'.$b->bank_picture).'" data-fancybox="images" data-caption="'.$b->bank_name.'"><img src="'.base_url('filemanager/files/bank/'.$b->bank_picture).'" width="300"></a>';

    }
    public function ajaxPicRecipt(){
      $receiptid =  $this->input->post('receipt_id');
      $b=$this->db->get_where('receipt_reserve',array('receipt_id'=>$receiptid))->row();
      echo '<a href="'.base_url('filemanager/files/'.$b->receipt_picture).'" data-fancybox="images" data-caption="'.$b->receipt_name.'"><img src="'.base_url('filemanager/files/'.$b->receipt_picture).'" width="300"></a>';

    }


    public function setting_bank(){
      $crud = new grocery_CRUD();
      $crud->set_theme('bootstrap-v4');
      $crud->set_table('setting_bank');
      $crud->set_subject('');
      $crud->unset_export();
      $crud->unset_print();
      $crud->set_field_upload('bank_picture','filemanager/files/bank');
      $crud->set_field_upload('bank_qrcode','filemanager/files/bank');
      $output = $crud->render();
      $this->_iweb_output($output);

    }


    public function receipt_reserve(){
      $crud = new grocery_CRUD();
      $crud->set_theme('bootstrap-v4');
      $crud->set_table('receipt_reserve');
      $crud->set_subject('');
      $crud->set_field_upload('receipt_picture','filemanager/files');
      $crud->unset_export();
      $crud->unset_print();
      $crud->unset_delete();
      $crud->callback_read_field('receipt_picture', function ($value, $primary_key) {
             if($value!=""){
               $m = '<a data-fancybox="images" href="'.base_url('filemanager/files'.'/'.$value).'"  ><img src="'.base_url('filemanager/files'.'/'.$value).'" style="max-width:200px"></a>';
             }else{
               $m = "-";
             }
                    return $m;
           });
      $output = $crud->render();
      $this->_iweb_output($output);

    }

    function confirm_payment(){

      $crud = new grocery_CRUD();
      $crud->set_theme('bootstrap-v4');
      $crud->set_table('confirm_payment_room');
      $crud->set_subject('จัดการชำระเงินจองห้อง');

      // $crud->set_relation('microsite_id','microsite','name_th');
      $crud->set_field_upload('filename','filemanager/files/receipt');
      $crud->callback_column('admin_confirm',array($this,'_callback_admin_confirm'));
      $crud->display_as('price','ยอดเงิน');
      $crud->display_as('invoice','เลขที่ใบสั่งซื้อ');
      $crud->display_as('transfer_datetime','เวลาโอน');
      $crud->display_as('confirm_datetime','เวลายืนยัน');
      $crud->display_as('filename','ไฟล์แนบ');
      $crud->display_as('bank','ธนาคาร');
      $crud->display_as('firstname','ชื่อ');
      $crud->display_as('lastname','นามสกุล');
      $crud->display_as('email','อีเมล');
      $crud->display_as('tel','โทรศัพท์');

      $crud->display_as('bank','ธนาคาร');
      $crud->display_as('firstname','ชื่อ');
      $crud->display_as('lastname','นามสกุล');
      $crud->display_as('email','อีเมล');
      $crud->display_as('tel','โทรศัพท์');
      $crud->unset_columns('file','message');
      $crud->unset_fields('file');
      // $crud->unset_export();
      $crud->unset_print();
      $crud->unset_delete();
      $crud->unset_add();
       $crud->unset_edit();
       $crud->unset_read();
       // $crud->add_action('Send mail ', '', 'admin_reserve/sendmail_confirm', 'el-book');
       $crud->callback_read_field('filename', function ($value, $primary_key) {
              if($value!=""){
                $m = '<a data-fancybox="images" href="'.base_url('filemanager/files/receipt'.'/'.$value).'"  ><img src="'.base_url('filemanager/files/receipt'.'/'.$value).'" style="max-width:200px"></a>';
              }else{
                $m = "-";
              }
                     return $m;
            });
      // $crud->unset_read();
      $output = $crud->render();
      $output->description = "description_confirm_payment";

      $this->_iweb_output($output);
    }

    public function sendmail_confirm($id=null){

      $g=$this->db->get_where('confirm_payment_room',array('id'=>$id))->row();
      $mm=$this->db->get_where('reserve_room',array('invoice'=>$g->invoice))->row();

      // echo $g->invoice;
      $data = array(
                     'status' => 'paid',
                     'success_datetime' => date('d/m/Y H:i:s')
                  );

       $this->db->where('invoice', $g->invoice);
       $this->db->update('reserve_room', $data);


               $this->db->where('prr_id',$mm->prr_id);
       $room = $this->db->update('project_reserve_room',array('room_status'=>'3'));


    if($this->_member_reserve_mail($g->invoice)){
      $this->db->where('id',$g->id);
      $this->db->update('confirm_payment_room',array('admin_confirm'=>'already'));
      echo "success";
    }else{
      echo "fail";
    }
    }

            private function _member_reserve_mail($invoice=null)
            {
              $createPDFFile = $invoice.'.pdf';
              $filepdf = $this->ipdf($invoice,$createPDFFile);
                  if($filepdf){
                 if($this->_mail_pdf($createPDFFile,$invoice)){
                     return TRUE;
                   }
                 return FALSE;
               }
            }

  public   function ipdf($invoice=null,$namefile=null){
    $data = array('invoice'=>$invoice);
    $html=$this->load->view('pdf-reserve-room/receipt', $data, TRUE);
                        // $pdfFilePath = "./uploads/".$namefile;
                        $pdfFilePath = "./assets/pdfs/".$namefile;

                        //load mPDF library
                        $this->load->library('m_pdf');

                       //generate the PDF from the given html
                        $this->m_pdf->pdf->WriteHTML($html);

                        //download it.
                        $this->m_pdf->pdf->Output($pdfFilePath, "F");
                        // $html=$this->load->view('pdf/pdf_confirm');
                        return true;
    }


    private function _mail_pdf($fileName=null,$invoice=null){
      $g= $this->db->get_where('reserve_room',array('invoice'=>$invoice))->row();
      $user = $this->db->get_where('member',array('email'=>$g->email))->row();
      $pro = $this->db->join('project_reserve_room','project_reserve_room.prr_id=reserve_room.prr_id')->get_where('reserve_room',array('reserve_room.prr_id'=>$g->prr_id))->row();
      $mi  = $this->db->get_where('microsite',array('id'=>$pro->microsite_id))->row();
      $v = $this->mail_seller->getSeller($g->prr_id);
      $sellerMail = array();
      if($v['num']!=0){
        foreach($v['query'] as $k){
          $sellerMail[]  = $k->email;
        }
      }
      $name  = $user->name_title." ".$user->name." ".$user->lastname;
      $email = array($user->email);


      $subject="จองห้อง โครงการ ".$mi->name_th;
      $path    = "./assets/pdfs/".$fileName;
      $config = Array(
                      'protocol' => 'smtp',
                      'smtp_host' => 'ssl://smtp.googlemail.com',
                      'smtp_port' => 465,
                      'smtp_user' => 'iwebdevthai@gmail.com', // change it to yours
                        'smtp_pass' => '00260926', // change it to yours
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'wordwrap' => TRUE,
                        'starttls' => TRUE,
                        'crlf'     => '\r\n'
                      );

                      $this->load->library('email', $config);
                      $this->email->set_newline("\r\n");
                      $this->email->from("webmaster@richy.co.th", "Richy จองห้อง");
                      $this->email->to($email);
                      if(!empty($sellerMail)){
                        $this->email->cc($sellerMail);
                      }
                      // $this->email->cc('boyz@thinkercorp.com');

                      $this->email->subject($subject.'#'.date('YmdHis'));
                      $this->email->message($this->load->view('mail-reserve-room/mail_sent_receipt',array('invoice'=>$invoice),TRUE));
                      $this->email->attach($path);

                      $send = $this->email->send();
                      if($send){
                        return TRUE;
                      }else{
                        return FALSE;
                      }

                    }

    public function _callback_admin_confirm($value, $row)
    {
      $m = '<div class="text-center">';
      if($value=="already"){
        $m .= '<div class="show-icon" id="row-'.$row->id.'"><font color="green" size="5"><i class="far fa-check-circle"></i></font></div>';
      }else{
        $m .= '<div class="show-icon" id="row-'.$row->id.'"><font color="red" size="5"><i class="fas fa-exclamation-circle"></i></font></div>';
      }
      $m .= '<button type="button" class="btn btn-info isend-confirm" style="width:150px;" data-id="'.$row->id.'">ยืนยันชำระเงิน</button>';
      $m .= "</div>";

      return $m;
    }


    public function admin_manager(){
    $r = $this->db->get('users_main')->num_rows();
    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('users_main');
    $crud->set_subject('');
    // $crud->columns('name_th','detail_th');
  //  $crud->set_relation('level','users_level','level_name');
    $crud->callback_edit_field('password',array($this,'set_password_input_to_empty'));
    $crud->callback_add_field('password',array($this,'set_password_input_to_empty'));
    $crud->callback_insert(array($this,'encrypt_password_and_insert_callback'));
    $crud->callback_before_update(array($this,'encrypt_password_callback'));
    $crud->set_field_upload('picture','filemanager/files');
    $crud->unset_columns('password');
    $crud->unset_export();
    $crud->unset_print();
    if($r<=1){
    $crud->unset_delete();
    }
    $crud->callback_read_field('picture', function ($value, $primary_key) {
      if($value!=""){
        $m = '<a data-fancybox="images" href="'.base_url('filemanager/files'.'/'.$value).'"  ><img src="'.base_url('filemanager/files'.'/'.$value).'" style="max-width:200px"></a>';
      }else{
        $m = "-";
      }
      return $m;
    });
    $output = $crud->render();
    $this->_iweb_output($output);
    }
    function encrypt_password_and_insert_callback($post_array) {
      unset($post_array['s5456fc54']);
      $post_array['password'] = md5($post_array['password']);

      return $this->db->insert('users_main',$post_array);
    }

    function encrypt_password_callback($post_array, $primary_key) {
        $this->load->library('encrypt');

        //Encrypt password only if is not empty. Else don't change the password to an empty field
        if(!empty($post_array['password']))
        {
            $key = 'super-secret-key';
            $post_array['password'] = md5($post_array['password']);
        }
        else
        {
            unset($post_array['password']);
        }

      return $post_array;
    }

    function set_password_input_to_empty() {
        return "<input type='password' name='password' value='' />";
    }

    public function filemanager(){
        $this->_iweb_output((object)array('output' => '','namepage'=>'FILE MANAGER' ,'iframe'=>'iframeweb' , 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));

    }
    public function setting(){

      $crud = new grocery_CRUD();
      $crud->set_theme('bootstrap-v4');
      $crud->set_table('setting');
      $crud->set_subject('');
      $crud->unset_export();
      $crud->unset_print();
      $crud->unset_delete();
      $crud->unset_add();
      $output = $crud->render();
      $this->_iweb_output($output);
    }


    public function logout(){
      $this->session->unset_userdata('sess_admin');
      redirect('login_reserve_room');
    }


   public function checkNumSlide(){
    if($this->input->post()):
     $rid = $this->input->post('id');
     $id  = explode('-',$rid);
    echo  $this->room->getNumSlide($id[1]);
    else:
     echo "not access";
    endif;
   }


   public function getSeller(){
     if($this->input->post()):

      $id = $this->input->post('id');
      $r = $this->db->get_where('seller_room',array('microsite_id'=>$id))->row();

      if($r!=false){
        if($r->seller!=""){
          $arr = json_decode($r->seller);
          $i=1;
          foreach($arr as $rr):
            $u = $this->room->getDataSeller($rr);
             echo "            <tr>\n";
             echo "              <th scope=\"row\">".$i++."</th>\n";
             echo "              <td><i class=\"far fa-user-circle h4\"></i></td>\n";
             echo "              <td>".$u->username."</td>\n";
             echo "              <td>".$u->email."</td>\n";
             echo "              <td>".$u->level."</td>\n";
             echo "            </tr>";
          endforeach;
        }else{

          echo "<tr><td class='text-center' colspan='5'>--- ไม่พบผู้ดูแลขาย ---</td></tr>";
        }

      }else{
          echo "<tr><td class='text-center' colspan='5'>--- ไม่พบผู้ดูแลขาย ---</td></tr>";
      }

     else:
      echo "not access";
     endif;


   }

   public function getTickSeller(){
     $id = $this->input->post('id');
     $s =$this->db->get_where('seller_room',array('microsite_id'=>$id))->row();

     if($s){

        if($s->seller!=""){

          echo $s->seller;
        }else{

          echo "[]";
        }


     }else{
       echo "[]";
     }


   }



   public function ajax_list_room(){

     $q = $this->db->get_where('project_reserve_room',array('microsite_id'=>$this->input->post('mid')))->result_array();

     $this->load->view('admin-reserve-room/ajax_list_room',array('q'=>$q));

   }

   public function ajax_list_room_select(){
     $this->db->where('prr_id',$this->input->post('prr_id'));
     $this->db->update('project_reserve_room',array('extra_room'=>'yes'));

     $q = $this->db->join('microsite','microsite.id=project_reserve_room.microsite_id')->get_where('project_reserve_room',array('extra_room'=>'yes'))->result_array();
     $this->load->view('admin-reserve-room/ajax_list_room_select',array('q'=>$q));

   }

   public function ajax_list_room_select_delete(){
     $this->db->where('prr_id',$this->input->post('prr_id'));
     $this->db->update('project_reserve_room',array('extra_room'=>'no'));

     $q = $this->db->join('microsite','microsite.id=project_reserve_room.microsite_id')->get_where('project_reserve_room',array('extra_room'=>'yes'))->result_array();
     $this->load->view('admin-reserve-room/ajax_list_room_select',array('q'=>$q));

   }


   public function ajaxOnOffProject(){
      $post = $this->input->post();


      if($post!=false){
      $status = $post['status'];
      $pInfo = $this->db->get_where('project_information_room',array('microsite_id'=>$post['id']))->row();
      if($pInfo==false){

       $this->db->insert('project_information_room',array('microsite_id'=>$post['id'],'bank'=>1,'receipt'=>1,'on_off_project'=>$status));

      }else{

        $this->db->where('microsite_id',$post['id']);
        $this->db->update('project_information_room',array('on_off_project'=>$status));
      }

       echo "success";
      }else{
        echo "fail";
      }



   }


   // booking

   public function project_booking(){
     $crud = new grocery_CRUD();
     $crud->set_theme('bootstrap-v4');
     $crud->set_table('project_booking');
     $crud->order_by('project_booking_id','desc');
     $crud->display_as('microsite_id','โครงการ');
     $crud->display_as('project_name','ชื่อโครงการ');
     $crud->display_as('project_booking_id','รหัส campaign');
     $crud->display_as('project_booking_status','สถานะ');
     $crud->display_as('project_booking_intro_picture','Picture intro (1800x910 px)');
     $crud->display_as('project_booking_intro_picture_mobile','Picture intro mobile (750x1334 px)');
     $crud->display_as('project_picture','Picture project (1800x910 px)');
     $crud->display_as('manage','จัดการ');
     $crud->set_subject('สร้างระบบ Booking');
     $crud->columns('project_booking_id','project_name','project_booking_status','manage');
     $crud->set_field_upload('project_picture','img');
     $crud->set_field_upload('project_logo','img');
     $crud->set_field_upload('project_map','img');
     $crud->set_field_upload('project_booking_intro_picture','img');
     $crud->set_field_upload('project_booking_intro_picture_mobile','img');
     $crud->callback_column('manage',array($this,'_callback_button_url'));
     $crud->unset_read();
     $crud->unset_delete();
     $crud->unset_print();
     $crud->unset_export();
     $output = $crud->render();

     $this->_iweb_output($output);
   }

   public function _callback_button_url($value, $row)
   {
     return "<div style='min-width:390px;'><a data-fancybox='' data-type='iframe' class='btn btn-success' href='javascript:;' data-src='".site_url('up_excel/file/'.$row->project_booking_id)."'> <span class='glyphicon glyphicon-edit'></span> อัพ Excel ห้อง</a>
     &nbsp; <a data-fancybox='' data-type='iframe' class='btn btn-primary' href='javascript:;' data-src='".site_url('admin_reserve_room/room_booking/'.$row->project_booking_id)."'> <span class='glyphicon glyphicon-edit'></span> จัดการห้อง</a>
     &nbsp; <a data-fancybox='' data-type='iframe' class='btn btn-info' href='javascript:;' data-src='".site_url('admin_reserve_room/floor_booking/'.$row->project_booking_id)."'> <span class='glyphicon glyphicon-edit'></span> จัดการชั้น</a>
     &nbsp; <a data-fancybox='' data-type='iframe' class='btn btn-warning' href='javascript:;' data-src='".site_url('admin_reserve_room/gallery_booking/'.$row->project_booking_id)."'> <span class='glyphicon glyphicon-edit'></span> แกลอรี่</a></div>";
   }


   public function room_booking($id){
     $crud = new grocery_CRUD();
     $crud->set_theme('bootstrap-v4');
     $crud->set_table('room_booking');
     $crud->where('room_booking.project_booking_id',$id);
     $crud->unset_columns('project_booking_id');
     $crud->field_type('project_booking_id', 'hidden', $id);
     $crud->set_relation('room_status','room_status','status_name');
     $crud->display_as('room_code','รหัสห้อง');
     $crud->display_as('room_picture','รูปภาพ');
     $crud->display_as('room_design','แบบ');
     $crud->display_as('room_floor','ชั้น');
     $crud->display_as('room_type','ประเภทห้อง');
     $crud->display_as('room_size','ขนาด');
     $crud->display_as('room_position','ตำแหน่ง');
     $crud->display_as('room_status','สถานะ');
     $crud->display_as('room_price','ราคา');
     $crud->display_as('room_deposit','ราคาจอง');
     $crud->display_as('room_view','ผู้ชมห้อง');
     $crud->display_as('room_discount','โปรโมชั่น');
     $crud->set_subject('จัดการห้อง');
     //   $crud->columns('microsite_id','project_booking_status','manage');
     $crud->unset_columns('room_view','project_booking_id');
     $crud->field_type('room_view', 'readonly');

     $crud->unset_print();
     $crud->unset_export();
     $crud->set_field_upload('room_picture','img/room');
     $output = $crud->render();
     $this->_iframe_output($output);
   }

   public function floor_booking($id){
       $crud = new grocery_CRUD();
       $crud->set_theme('bootstrap-v4');
       $crud->set_table('booking_floor');
       $crud->where('booking_floor.project_booking_id',$id);
       $crud->field_type('project_booking_id', 'hidden', $id);
       $crud->set_subject('แบบชั้น');
       $crud->set_field_upload('booking_floor_picture','img/floor');
       $crud->display_as('booking_floor_picture','รูปชั้น');
       $crud->display_as('booking_floor_name','ชื่อชั้น');
       $crud->display_as('booking_floor_number','ชั้น');
       $crud->display_as('manage','จัดการ');
       $crud->columns('booking_floor_picture','booking_floor_name','booking_floor_number','manage');
       $crud->callback_column('manage',array($this,'_callback_webpage_url1'));
       $crud->unset_print();
       $crud->unset_export();
       $crud->unset_read();
       $crud->unset_columns('project_booking_id');
       $output = $crud->render();

       $this->_iframe_output($output);
   }
   public function _callback_webpage_url1($value, $row)
   {
     return "<a class='btn btn-default' target='_blank' href='".site_url('admin_booking/room_type_design/'.$row->booking_floor_id)."'> <span class='glyphicon glyphicon-edit'></span> แบบประเภทห้อง</a>";
   }



   function gallery_booking()
   {
       $image_crud = new image_CRUD();
       $image_crud->set_primary_key_field('id');
       $image_crud->set_url_field('url');
       $image_crud->set_table('example_3')
       ->set_relation_field('project_booking_id')
       ->set_ordering_field('priority')
       ->set_image_path('assets/uploads');

       $output = $image_crud->render();
       $this->_gallery_output($output);
   }

   public function member_booking(){
     $crud = new grocery_CRUD();
     $crud->set_theme('bootstrap-v4');
     $crud->set_table('booking');
     $crud->order_by('booking_id','desc');
     $crud->set_subject('สมาชิกที่ทำการจองห้องทั้งหมด');
     $crud->display_as('room_code','Room ID');
     $crud->display_as('room_code_real','Room Code');
     $crud->display_as('booking_status','สถานะ');
     $crud->display_as('booking_datetime','วันและเวลาจอง');
     $crud->columns('inv','member','room_code','booking_status','booking_datetime','project_booking_id');
     $crud->callback_column('inv',array($this,'_columns_getInv'));
     $crud->callback_column('member',array($this,'_columns_getMember'));
     // $crud->add_action('pdf receipt', '', 'admin_booking/member_booking_pdf','fa fa-plus ','',array($this,'some_row'));
     // $crud->add_action('pdf send mail', '', 'admin_booking/member_booking_mail','fa fa-plus ','',array($this,'some_row'));
     $crud->unset_delete();
     $crud->unset_read();
     $crud->unset_print();
     $crud->unset_export();
     $crud->unset_add();
     $crud->unset_edit();
     $output = $crud->render();
     $output->control  = "yes";
     $output->control_view  = "control_member_booking";
     $this->_iweb_output($output);
   }

   public function _columns_getMember($value, $row)
    { $g=$this->db->get_where('member',array('mid'=>$row->mid))->row();
      if($g!=false){
        $email = $g->email;
        $name  = $g->name." ".$g->lastname;
      }else{
        $email = "-";
        $name  = "-";
      }
      $m  = "<div style='min-width:220px;font-weight:bold;color:#2D6073'>";
      $m .= "รหัสสมาชิก : ".$row->mid."<br/>";
      $m .= "ชื่อ : ".$name."<br/>";
      $m .= "email : ".$email."<br/>";
      $m .= "</div>";

      return $m;
    }

    public function _columns_getInv($value,$row){
      $g=$this->db->get_where('member',array('mid'=>$row->mid))->row();
      $m = "<div style='min-width:250px;'>";
      $m .= "ใบสั่งจองเลขที่ : ".$value."<br/><br/>";
      if($row->booking_status=="paid"):
      $m .= '<a href="'.site_url('admin_reserve_room/member_booking_pdf/'.$row->booking_id).'" class="btn btn-outline-danger"><i class="fas fa-file-pdf"></i> PDF</a>
       <button type="button" class="btn btn-outline-info imail-booking" data-inv="'.$row->inv.'" data-id="'.$row->booking_id.'" data-email="'.$g->email.'" data-name="'.$g->name.' '.$g->lastname.'"><i class="far fa-envelope"></i> SEND EMAIL</button>';
      endif;
      $m .= "</div>";

      return $m;


    }


        public function member_booking_pdf($id=null){
          error_reporting(0);
          @ini_set('display_errors', 0);
          if($id==""){
            redirect('admin_reserve_room','refresh',301);
          }

          $this->load->model('booking_model','book');
          $this->load->view('pdf-booking/receipt_booking',array('booking_id'=>$id));
        }



        public function member_booking_mail($id=null)
        {
          $post = $this->input->post();
          $this->load->model('booking_model','book');
          $data = array(
            'booking_id'=>$post['id']
          );

          $htmlContent = $this->load->view('pdf-booking/receipt_booking', $data, TRUE);
          $createPDFFile = $post['inv'].'.pdf';
          $filepdf =  $this->createPDF(FCPATH."assets/pdfs/".$createPDFFile, $htmlContent,$post['id']);
          if($filepdf){
            $this->_mail_booking_pdf($createPDFFile,$post['id']);
          }
        }


        private function _mail_booking_pdf($fileName=null,$booking_id=null){
          $this->load->model('booking_model','book');
          $book = $this->book->get_user_receipt($booking_id);
          $dateTime = $book->booking_datetime;
          $roomCode = $book->room_code_real;
          $room = $this->book->get_room($roomCode);
          $projectId = $room->project_booking_id;
          $project = $this->book->get_project($projectId);

          $user  = $this->book->get_user($book->mid);
          $name  = $user->name_title." ".$user->name." ".$user->lastname;
          // $email = array($user->email,$this->emailMain);
          $email = array($user->email);
          $data  = array('inv'=>$book->inv,'name'=>$name,'roomCode'=>$room->room_code,'projectName'=>$project->name_th,'dateTime'=>$dateTime);
          $path    = "./assets/pdfs/".$fileName;

          $config = Array(
    				'protocol' => 'smtp',
    				'smtp_host' => 'ssl://smtp.googlemail.com',
    				'smtp_port' => 465,
    				'smtp_user' => 'iwebdevthai@gmail.com', // change it to yours
    				'smtp_pass' => '00260926', // change it to yours
    				'mailtype' => 'html',
    				'charset' => 'utf-8',
    				'wordwrap' => TRUE,
    				'starttls' => TRUE,
    				'crlf'     => '\r\n'
    			);




    			$this->load->library('email', $config);
    			$this->email->set_newline("\r\n");
    			$this->email->from("webmaster@richy.co.th", "Booking Receipt");
    			$this->email->to($email);

    			//$this->email->cc($this->input->post('email',TRUE));
    			// $this->email->subject($name." #".date('dmYHis'));
          $this->email->subject('Booking - '.$project->name_th.' <ห้อง '.$room->room_code.'> #'.date('YmdHis'));
    			$this->email->message($this->load->view('mail/mail_receipt',$data,true));
          $this->email->attach($path);

          $send = $this->email->send();

          if($send){
            // echo " Send Email Success";
            $this->load->view('mail/mail_receipt_success');
          }

        }


        public function member(){
          $crud = new grocery_CRUD();
          $crud->set_theme('bootstrap-v4');
          $crud->set_table('member');
          $crud->order_by('id','desc');
          $crud->set_subject('รายการสั่งจองห้อง');
          $crud->columns('name','lastname','phone1','email','card_number','regis_datetime');
          // $crud->unset_fields('project_reserve_id');
          // $crud->unset_export();
          $crud->unset_print();
          $crud->unset_delete();
          $crud->unset_add();
          $crud->unset_edit();
          $crud->unset_read();
          $output = $crud->render();
          // $output->title_page = $p->name_th."<br/>รหัสโครงการ : ".$p->project_code;
          $this->_iweb_output($output);
        }


   public function analytic_intro(){
     $crud = new grocery_CRUD();
     $crud->set_theme('bootstrap-v4');
     $crud->set_table('analytic_intro');
     $crud->unset_print();
     $crud->unset_export();
     $crud->unset_read();
     $crud->unset_delete();
     $crud->unset_texteditor('ga_header','full_text');
     $crud->unset_texteditor('ga_body','full_text');
     $output = $crud->render();
     $this->_iweb_output($output);


   }

   public function dashboard_campaign(){

    $this->_iweb_output((object)array('output' => '','namepage'=>'DASHBOARD CAMPAIGN' ,'title_page'=>'Dashboard','dashboard'=>'idashboard_campaign', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));

   }

   public function onoff(){

   $data = array('status'=>$this->input->post('status'));
   $this->db->where('id',$this->input->post('id',TRUE));
   $sql =   $this->db->update('offon_booking',$data);

   if($sql){
     echo "ok";
   }else{
     echo "no";
   }

   }

   public function onoff_booking_intro(){

   $data = array('status'=>$this->input->post('status'));
   $this->db->where('id',$this->input->post('id',TRUE));
   $sql =   $this->db->update('offon_booking_intro',$data);

   if($sql){
     echo "ok";
   }else{
     echo "no";
   }

   }

   public function onoff_production(){

   $data = array('status'=>$this->input->post('status'));
   $this->db->where('id',$this->input->post('id',TRUE));
   $sql =   $this->db->update('offon_production',$data);

   if($sql){
     echo "ok";
   }else{
     echo "no";
   }

   }


public function sort_management(){

  $this->_iweb_output((object)array('output' => 'component_sort_management','control'=>'no','namepage'=>'จัดลำดับ project และ เรียงห้องแบบพิเศษ' ,'title_page'=>'Sort Project and Extra Room', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));

}

public function ajaxSortProject(){

$data = $this->input->post('idata');
$data = json_decode($data);
// var_dump($data[0]);
$i=1;
foreach($data[0] as $k){
  // echo $k->name." id".$k->id."<br/>";
  $this->db->where('microsite_id',$k->id);
  $this->db->update('project_information_room',array('sort_project'=>$i++));
}

}

public function ajaxSortRoom(){
  $data = $this->input->post('idata');
  $data = json_decode($data);
  // var_dump($data[0]);
  $i=1;
foreach($data[0] as $k){
  // echo $k->name." id".$k->id."<br/>";
  $this->db->where('prr_id',$k->id);
  $this->db->update('project_reserve_room',array('sort_room'=>$i++));
}

}

}
