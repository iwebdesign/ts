<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('sess_admin')==false) {
          redirect('login?mode=session_incorrect');
          exit;

   }else{
          if($this->session->userdata('sess_admin')['level']!="super admin"){
            redirect('login');
                exit;
          }
   }


    $this->load->library('grocery_crud');
    $this->load->library('image_crud');
  }

  public function index()
	{
	$this->_iweb_output((object)array('output' => '','namepage'=>'DASHBOARD' ,'title_page'=>'Dashboard','dashboard'=>'idashboard', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));
  }

  public function _iweb_output($output = null)
  {
    $this->load->view('backend/template',(array)$output);
  }

  public function _iframe_output($output = null)
  {
    $this->load->view('backend/template_iframe',(array)$output);
  }

  public function setting(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('setting');
    $crud->set_subject('SETTING <i class="fas fa-cogs"></i>');
    $crud->set_field_upload('logo','img');
    $crud->unset_texteditor('google_analytics','robots');
    $crud->callback_after_update(array($this, 'write_robots'));
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    $crud->unset_add();
    $output = $crud->render();
    $this->_iweb_output($output);
  }


  function write_robots($post_array,$primary_key)
{

  $this->load->helper('file');

    $data = $post_array['robots'];
      if ( ! write_file('./robots.txt', $data))
      {
              return false;
      }
      else
      {
              return true;
      }

}



  public function admin_manager(){
  $r = $this->db->get('users')->num_rows();
  $crud = new grocery_CRUD();
  $crud->set_theme('bootstrap-v4');
  $crud->set_table('users');
  $crud->set_subject('ผู้ดูแลเว็บ <i class="fas fa-users"></i>');
  // $crud->columns('name_th','detail_th');
//  $crud->set_relation('level','users_level','level_name');
  $crud->callback_edit_field('password',array($this,'set_password_input_to_empty'));
  $crud->callback_add_field('password',array($this,'set_password_input_to_empty'));
  $crud->callback_insert(array($this,'encrypt_password_and_insert_callback'));
  $crud->callback_before_update(array($this,'encrypt_password_callback'));
  $crud->set_field_upload('picture','img');
  $crud->unset_columns('password');
  $crud->unset_export();
  $crud->unset_print();
  if($r<=1){
  $crud->unset_delete();
  }
  $crud->callback_read_field('picture', function ($value, $primary_key) {
    if($value!=""){
      $m = '<a data-fancybox="images" href="'.base_url('filemanager/files'.'/'.$value).'"  ><img src="'.base_url('filemanager/files'.'/'.$value).'" style="max-width:200px"></a>';
    }else{
      $m = "-";
    }
    return $m;
  });
  $output = $crud->render();
  $this->_iweb_output($output);
  }
  function encrypt_password_and_insert_callback($post_array) {
    $this->load->library('encrypt');
    unset($post_array['s5456fc54']);
    $post_array['password'] = $this->encrypt->encode($post_array['password']);

    return $this->db->insert('users',$post_array);
  }

  function encrypt_password_callback($post_array, $primary_key) {
      $this->load->library('encrypt');

      //Encrypt password only if is not empty. Else don't change the password to an empty field
      if(!empty($post_array['password']))
      {
          // $key = 'super-secret-key';
          $post_array['password'] = $this->encrypt->encode($post_array['password']);
      }
      else
      {
          unset($post_array['password']);
      }

    return $post_array;
  }

  function set_password_input_to_empty() {
      return "<input type='password' name='password' value='' />";
  }



  public function home_slide(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('home_slide');
    $crud->set_subject('Home slide');
    $crud->set_field_upload('picture','img/slide');
    $crud->display_as('picture','Picture <br/> 1600x880px');
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    $crud->required_fields('status');
    // $crud->unset_add();
    $crud->callback_read_field('picture', function ($value, $primary_key) {
    if($value!=""){
        $m = '<a data-fancybox="images" href="'.base_url('img/slide'.'/'.$value).'"  ><img src="'.base_url('img/slide'.'/'.$value).'" style="max-width:200px"></a>';
         }else{
        $m = "-";
        }
        return $m;
    });
    $output = $crud->render();
    $this->_iweb_output($output);
  }

  public function box_intro(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('box_intro');
    $crud->set_subject('Box intro');
    $crud->set_field_upload('icon','img');
    $crud->callback_column('icon',array($this,'_callback_icon'));
    $crud->callback_read_field('icon', function ($value, $primary_key) {
    if($value!=""){
        $m = '<div style="padding:10px;background-color:#ccc;text-align:center;max-width:200px;"><img src="'.base_url('img/'.$value).'" width="50"></div>';
         }else{
        $m = "-";
        }
        return $m;
    });
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    $crud->unset_add();
    $output = $crud->render();
    $this->_iweb_output($output);
  }

  public function _callback_icon($value, $row)
  {
    if($value!=""||$value!=null){
      $m = '<div style="padding:10px;background-color:#ccc;text-align:center;"><img src="'.base_url('img/'.$value).'" width="50"></div>';
    }else{
      $m = '';
    }
    return $m;
  }

  public function home_intro(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('home_intro');
    $crud->set_subject('');
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    // $crud->unset_add();
    $output = $crud->render();
    $this->_iweb_output($output);
  }


  public function home_intro_design(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('home_intro_design');
    $crud->set_subject('Home intro design');
    $crud->set_field_upload('picture','img');
    $crud->display_as('picture','Picture <br/> 1600x880px');
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    $crud->required_fields('status');
    // $crud->unset_add();
    $crud->callback_read_field('picture', function ($value, $primary_key) {
    if($value!=""){
        $m = '<a data-fancybox="images" href="'.base_url('img'.'/'.$value).'"  ><img src="'.base_url('img'.'/'.$value).'" style="max-width:200px"></a>';
         }else{
        $m = "-";
        }
        return $m;
    });
    $output = $crud->render();
    $this->_iweb_output($output);
  }


  public function head_picture_about(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('head_picture_about');
    $crud->set_subject('Head picture <i class="far fa-images"></i>');
    $crud->set_field_upload('picture','img');
    $crud->display_as('picture','Picture <br/> 1280x250px');
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    $crud->unset_add();

    $crud->callback_read_field('picture', function ($value, $primary_key) {
    if($value!=""){
        $m = '<a data-fancybox="images" href="'.base_url('img'.'/'.$value).'"  ><img src="'.base_url('img'.'/'.$value).'" style="max-width:200px"></a>';
         }else{
        $m = "-";
        }
        return $m;
    });
    $output = $crud->render();
    $this->_iweb_output($output);
  }

  public function about()
	{
	$this->_iweb_output((object)array('output' => '','namepage'=>'ABOUT US' ,'title_page'=>'About us','iframeEdit'=>'edit_about', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));
  }


  public function ajaxAddAbout(){

    $data=$this->input->post();
    $this->db->where('id',1);
    $this->db->update('about',$data);
    echo "ok";
  }


  public function suppliers()
  {
  $this->_iweb_output((object)array('output' => '','namepage'=>'ABOUT US / SUPPLIERS' ,'title_page'=>'Suppliers','iframeEdit'=>'edit_suppliers', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));
  }


    public function ajaxAddSuppliers(){

      $data=$this->input->post();
      $this->db->where('id',1);
      $this->db->update('suppliers',$data);
      echo "ok";
    }

    public function roll()
    {
    $this->_iweb_output((object)array('output' => '','namepage'=>'Why Roll Forming' ,'title_page'=>'Why Roll Forming','iframeEdit'=>'edit_roll', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));
    }



      public function ajaxAddRoll(){

        $data=$this->input->post();
        $this->db->where('id',1);
        $this->db->update('roll',$data);
        echo "ok";
      }


      public function roll_parts()
      {
      $this->_iweb_output((object)array('output' => '','namepage'=>'Roll Forming Parts' ,'title_page'=>'Roll Forming Parts','iframeEdit'=>'edit_roll_parts', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));
      }

      public function ajaxAddRollParts(){

        $data=$this->input->post();
        $this->db->where('id',1);
        $this->db->update('roll_parts',$data);
        echo "ok";
      }

      public function engineer()
      {
      $this->_iweb_output((object)array('output' => '','namepage'=>'TECHNOLOGY / ENGINEERING' ,'title_page'=>'TECHNOLOGY / ENGINEERING','iframeEdit'=>'edit_engineer', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));
      }

      public function ajaxAddEngineer(){

        $data=$this->input->post();
        $this->db->where('id',1);
        $this->db->update('engineer',$data);
        echo "ok";
      }


      public function safety_quality()
      {
      $this->_iweb_output((object)array('output' => '','namepage'=>'TECHNOLOGY / SAFETY QUALITY' ,'title_page'=>'TECHNOLOGY / SAFETY QUALITY','iframeEdit'=>'edit_safety', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));
      }

      public function ajaxAddSafety(){

        $data=$this->input->post();
        $this->db->where('id',1);
        $this->db->update('safety',$data);
        echo "ok";
      }



  public function head_picture_engineer(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('head_picture_engineer');
    $crud->set_subject('Head picture <i class="far fa-images"></i>');
    $crud->set_field_upload('picture','img');
    $crud->display_as('picture','Picture <br/> 1280x250px');
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    $crud->unset_add();
    $crud->callback_read_field('picture', function ($value, $primary_key) {
    if($value!=""){
        $m = '<a data-fancybox="images" href="'.base_url('img'.'/'.$value).'"  ><img src="'.base_url('img'.'/'.$value).'" style="max-width:200px"></a>';
         }else{
        $m = "-";
        }
        return $m;
    });
    $output = $crud->render();
    $this->_iweb_output($output);
  }

  public function head_picture_news(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('head_picture_news');
    $crud->set_subject('Head picture <i class="far fa-images"></i>');
    $crud->set_field_upload('picture','img');
    $crud->display_as('picture','Picture <br/> 1280x250px');
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    $crud->unset_add();

    $crud->callback_read_field('picture', function ($value, $primary_key) {
    if($value!=""){
        $m = '<a data-fancybox="images" href="'.base_url('img'.'/'.$value).'"  ><img src="'.base_url('img'.'/'.$value).'" style="max-width:200px"></a>';
         }else{
        $m = "-";
        }
        return $m;
    });
    $output = $crud->render();
    $this->_iweb_output($output);
  }



    public function news(){

      $crud = new grocery_CRUD();
      $crud->set_theme('bootstrap-v4');
      $crud->set_table('news');
      $crud->set_subject('News   <i class="far fa-newspaper"></i>');
      $crud->set_field_upload('picture_thumbnail','img');
      $crud->set_field_upload('picture','img');
      $crud->display_as('picture','Picture <br/> 1200x420px');
      $crud->display_as('picture_thumbnail','Picture thumbnail <br/> 400x270px');
      $crud->unset_export();
      $crud->unset_print();
      $crud->unset_delete();
      $crud->unset_columns('picture','tag','short_detail_en','short_detail_th','detail_en','detail_th','post_by');
      $crud->callback_column('picture_thumbnail',array($this,'_callback_image_news'));

      $crud->required_fields('status');
      // $crud->unset_add();
      $crud->callback_read_field('picture_thumbnail', function ($value, $primary_key) {
      if($value!=""){
          $m = '<a data-fancybox="images" href="'.base_url('img'.'/'.$value).'"  ><img src="'.base_url('img'.'/'.$value).'" style="max-width:200px"></a>';
           }else{
          $m = "-";
          }
          return $m;
      });
      $crud->callback_read_field('picture', function ($value, $primary_key) {
      if($value!=""){
          $m = '<a data-fancybox="images" href="'.base_url('img'.'/'.$value).'"  ><img src="'.base_url('img'.'/'.$value).'" style="max-width:200px"></a>';
           }else{
          $m = "-";
          }
          return $m;
      });
      $output = $crud->render();
      $this->_iweb_output($output);
    }

    public function _callback_image_news($value, $row)
    {
      if($value!=""||$value!=null){
        $m = '<a data-fancybox="images" href="'.base_url('img'.'/'.$value).'"  ><img src="'.base_url('img/'.$value).'" width="150"></a>';
      }else{
        $m = '<div style="width:150px;"><img src="'.base_url('img/no-image.png?v=1').'" width="150"></div>';
      }
      return $m;
    }

  public function head_picture_career(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('head_picture_career');
    $crud->set_subject('Head picture <i class="far fa-images"></i>');
    $crud->set_field_upload('picture','img');
    $crud->display_as('picture','Picture <br/> 1280x250px');
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    $crud->unset_add();
    $crud->callback_read_field('picture', function ($value, $primary_key) {
    if($value!=""){
        $m = '<a data-fancybox="images" href="'.base_url('img'.'/'.$value).'"  ><img src="'.base_url('img'.'/'.$value).'" style="max-width:200px"></a>';
         }else{
        $m = "-";
        }
        return $m;
    });
    $output = $crud->render();
    $this->_iweb_output($output);
  }

  public function career(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('career');
    $crud->set_subject('Career');
    $crud->columns('position_th','position_en','number','datetime');
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    // $crud->unset_add();
    $output = $crud->render();
    $this->_iweb_output($output);
  }



  public function head_picture_contact(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('head_picture_contact');
    $crud->set_subject('Head picture <i class="far fa-images"></i>');
    $crud->set_field_upload('picture','img');
    $crud->display_as('picture','Picture <br/> 1280x250px');
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    $crud->unset_add();
    $crud->callback_read_field('picture', function ($value, $primary_key) {
    if($value!=""){
        $m = '<a data-fancybox="images" href="'.base_url('img'.'/'.$value).'"  ><img src="'.base_url('img'.'/'.$value).'" style="max-width:200px"></a>';
         }else{
        $m = "-";
        }
        return $m;
    });
    $output = $crud->render();
    $this->_iweb_output($output);
  }

  public function list_contact(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('list_contact');
    $crud->order_by('id','desc');
    $crud->set_subject('Contact');
    // $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    $crud->unset_add();
    $output = $crud->render();
    $this->_iweb_output($output);
  }

  public function global_location(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('global_location');
    $crud->set_subject('Global location');
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    // $crud->unset_add();
    $output = $crud->render();
    $this->_iweb_output($output);
  }



  public function head_picture_roll(){

    $crud = new grocery_CRUD();
    $crud->set_theme('bootstrap-v4');
    $crud->set_table('head_picture_roll');
    $crud->set_subject('Head picture <i class="far fa-images"></i>');
    $crud->set_field_upload('picture','img');
    $crud->display_as('picture','Picture <br/> 1280x250px');
    $crud->unset_export();
    $crud->unset_print();
    $crud->unset_delete();
    $crud->unset_add();
    $crud->callback_read_field('picture', function ($value, $primary_key) {
    if($value!=""){
        $m = '<a data-fancybox="images" href="'.base_url('img'.'/'.$value).'"  ><img src="'.base_url('img'.'/'.$value).'" style="max-width:200px"></a>';
         }else{
        $m = "-";
        }
        return $m;
    });
    $output = $crud->render();
    $this->_iweb_output($output);
  }

  public function filemanager()
  {
  $sess =$this->session->set_userdata('RF',array('subfolder'=>'./source/user1/'));
  $this->_iweb_output((object)array('output' => '','namepage'=>'FILE MANAGER' ,'title_page'=>'FILEMANAGER','iframeEdit'=>'filemanager', 'js_files' => array(base_url('assets/grocery_crud/js/jquery-1.11.1.min.js')) , 'css_files' => array(base_url('assets/grocery_crud/themes/bootstrap-v4/css/bootstrap/bootstrap.min.css'))));
  }


  public function logout(){
    $this->session->unset_userdata('sess_admin');
    redirect('login');
  }



}
