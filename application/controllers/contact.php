<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact extends CI_Controller {
	public function index()
	{
                $css   = array('slick/slick.css','slick/slick-theme.css');
                $script=array('slick/slick.min.js');
                $lg  =$this->uri->segment(1);
                $page=$this->uri->segment(2);
                $data = array('lg'=>$lg,'page'=>$page,'content'=>'contact_view','script'=>$script,'css'=>$css,'seo'=>$this->_seo());
                $this->load->view('template',$data);

        }
        private function _seo($title=null,$lg=null){  
                $s = "<title>CONTACT - TS-Shape :: THAI SUMMIT SHAPE CORP CO.,LTD. </title>";
                return $s;

        }
}
