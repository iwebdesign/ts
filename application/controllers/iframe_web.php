<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Iframe_web extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

    $this->load->view('iframe_web');

  }


  function suppliers($mode=null)
  {
    $this->load->view('iframe_web_suppliers');
  }

  function roll($mode=null)
  {
    $this->load->view('iframe_web_roll');
  }

  function roll_parts($mode=null)
  {
    $this->load->view('iframe_web_roll_parts');
  }

  function engineer($mode=null)
  {
    $this->load->view('iframe_web_engineer');
  }

  function safety($mode=null)
  {
    $this->load->view('iframe_web_safety');
  }


  public function ajaxUploadImage(){
    $config['upload_path']="./img";
    $config['allowed_types']='jpg|png';
    $this->load->library('upload',$config);
    if($this->upload->do_upload("image")){
      $data = array('upload_data' => $this->upload->data());
      $fileName= $data['upload_data']['file_name'];
      $path =  '/img'.'/'.$fileName;

      echo   json_encode(array('path'=>$path,'status'=>'ok'));
    }else{
      echo   json_encode(array('path'=>$path,'status'=>'error'));
    }


  }

  public function ajaxUploadFile(){
    $config['upload_path']="./filebox";
    $config['allowed_types']='jpg|png|zip|pdf';
    $this->load->library('upload',$config);
    if($this->upload->do_upload("file")){
      $data = array('upload_data' => $this->upload->data());
      $fileName= $data['upload_data']['file_name'];
      $path =  '/filebox'.'/'.$fileName;

      echo   json_encode(array('path'=>$path,'namefile'=>$this->input->post('namefile'),'typefile'=>$data['upload_data']['file_ext'],'status'=>'ok'));
    }else{
      echo   json_encode(array('path'=>$path,'status'=>'error'));
    }


  }


  public function ajaxUploadFileAll(){
    $config['upload_path']="./filebox/roll";
    $config['allowed_types']='jpg|png|pdf';
    $this->load->library('upload',$config);
    if($this->upload->do_upload("image")) {
      $data = array('upload_data' => $this->upload->data());
      $filePic = $data['upload_data']['file_name'];
      $pathImg =  '/filebox'.'/'.'roll'.'/'.$filePic;

    }

    if($this->upload->do_upload("file")) {
      $data = array('upload_data' => $this->upload->data());
      $fileName = $data['upload_data']['file_name'];
      $pathFile =  '/filebox'.'/'.'roll'.'/'.$fileName;

    }

   if($this->upload->do_upload("file") && $this->upload->do_upload("image")){
       echo   json_encode(array('pathImg'=>$pathImg,'pathFile'=>$pathFile,'namefile'=>$this->input->post('namefile'),'status'=>'ok'));
   }else{
        echo  json_encode(array('status'=>'error'));
   }

    // echo $filePic." - ".$fileName;


  }




  public function gallery_roll(){
    $this->load->library('image_crud');

    $image_crud = new image_CRUD();

    $image_crud->set_primary_key_field('id');
    $image_crud->set_url_field('url');
    $image_crud->set_title_field('title');
    $image_crud->set_table('gallery_roll')
    ->set_ordering_field('priority')
    ->set_image_path('img/gallery');

    $output = $image_crud->render();

    $this->_gallery_output($output);
  }

  public function gallery_engineer(){
    $this->load->library('image_crud');

    $image_crud = new image_CRUD();

    $image_crud->set_primary_key_field('id');
    $image_crud->set_url_field('url');
    $image_crud->set_title_field('title');
    $image_crud->set_table('gallery_engineer')
    ->set_ordering_field('priority')
    ->set_image_path('img/gallery');

    $output = $image_crud->render();

    $this->_gallery_output($output);
  }


  function _gallery_output($output = null)
  {
    $this->load->view('iframe_gallery.php',$output);
  }




  public function ajaxReLoadGalRoll(){

    $data = $this->input->post();

    if($data==false){
      echo   "error";
    }else{
      $gal = $this->db->order_by('priority','asc')->get('gallery_roll')->result_array();

      if($gal!=false){
        foreach($gal as $r){
          echo '<div class="cell small-6 medium-3 item">';
          echo '<a href="/img/gallery/'.$r['url'].'" data-fancybox="gallery" data-caption="'.$r['title'].'">';
          echo '  <img src="/img/gallery/'.$r['url'].'" width="100%">';
          echo '  </a>';
          echo '</div>';
        }


      }else{
        echo '<div class="text-center margin-2-">--- Galley ---</div>';
      }


    }


  }


    public function ajaxReLoadGalEngineer(){

      $data = $this->input->post();

      if($data==false){
        echo   "error";
      }else{
        $gal = $this->db->order_by('priority','asc')->get('gallery_engineer')->result_array();

        if($gal!=false){
          foreach($gal as $r){
            echo '<div class="cell small-6 medium-3 item">';
            echo '<a href="/img/gallery/'.$r['url'].'" data-fancybox="gallery" data-caption="'.$r['title'].'">';
            echo '  <img src="/img/gallery/'.$r['url'].'" width="100%">';
            echo '  </a>';
            echo '</div>';
          }


        }else{
          echo '<div class="text-center margin-2-">--- Galley ---</div>';
        }


      }


    }

  public function upload_image(){


    $config['upload_path']="./filebox/image";
    $config['allowed_types']='jpg|png|pdf';
    $this->load->library('upload',$config);

    if($this->upload->do_upload("file")) {
      $data = array('upload_data' => $this->upload->data());
      $fileName = $data['upload_data']['file_name'];
      $pathFile =  '/filebox'.'/'.'image'.'/'.$fileName;
       echo   json_encode(array('link'=>$pathFile,'status'=>'ok'));
    }else{
       http_response_code(404);
    }




  }

  public function upload_image_tiny(){


    $config['upload_path']="./filebox/image";
    $config['allowed_types']='jpg|png|pdf';
    $this->load->library('upload',$config);

    if($this->upload->do_upload("file")) {
      $data = array('upload_data' => $this->upload->data());
      $fileName = $data['upload_data']['file_name'];
      $pathFile =  '/filebox'.'/'.'image'.'/'.$fileName;
    
           echo json_encode(array('location' => $pathFile));
    }else{
       http_response_code(404);
    }




  }




}
