<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {
   $this->load->view('backend/login');
  }

  public function checklogin(){
    $this->load->library('encrypt');
    $this->load->library('user_agent');
    $data = $this->input->post();

    if($data==false){
      echo "not allow access";
    }
    $this->db->where('email',$data['username']);
    $this->db->or_where('username',$data['username']);
    $sql =  $this->db->get('users')->row();
    $pass = $this->encrypt->encode($data['password']);


    if($sql==false){
        echo   json_encode(array('status'=>0,'text'=>'This member cannot be found.'));
    }else{
      if($data['password']!=$this->encrypt->decode($sql->password)){
        echo   json_encode(array('status'=>1,'text'=>'password is incorrect'));
      }else{

        $sess = array('level'=>$sql->level,'username'=>$sql->username,'picture'=>$sql->picture,'login_time'=>date('d/m/Y H:i:s'));

        $this->session->set_userdata('sess_admin',$sess);

        $log = array('ip'=>$this->input->ip_address(),
                      'username'=>$sql->username,
                      'datetime'=>date('d/m/Y H:i:s'),
                      'agent'=>$this->agent->platform(),
                      'level'=>$sql->level
                    );

        $this->db->insert('log_admin',$log);
        echo   json_encode(array('status'=>2,'text'=>'Login success.'));
      }

    }
    // var_dump($pass);
    // $pass = $this->encrypt->encode($data['password']);
    // var_dump($pass);
  }

}
