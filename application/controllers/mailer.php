<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailer extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

  }

  function contact(){
    $this->load->model('contact_model','contact');
    $data=$this->input->post();

    if($data==false){
      echo "Not allowed";

    }else{

      $data['ip']= $this->input->ip_address();
      $data['datetime'] = date('d/m/Y H:i:s');

      if($this->_sendMail($data)){

        $insert = $this->contact->list_contact($data);

        if($insert){
          echo json_encode(array('status'=>'ok','text'=>'insert already.'));
        }else{
          echo json_encode(array('status'=>'no','text'=>'not can insert to table.'));
        }


      }else{
          echo json_encode(array('status'=>'no','text'=>'Not sent email.'));
      }


    }
  }

  private function _sendMail($data=null){
    $sql = $this->db->order_by('id','desc')->get('list_contact');
    $numRow = $sql->num_rows();
    $row = $sql->row();

    if($numRow==0){
      $iorder = "00001";
    }else{
      $idLast = $row->id + 1;
     $iorder = str_pad($row->id,'6','0',STR_PAD_LEFT);
    }

    if($this->setting_model->get()['mode_environment']=="productions"){
     $emailAdmin =  $this->setting_model->get()['email_admin'];
    }else{
     $emailAdmin =  $this->setting_model->get()['email_dev'];
    }
    $this->load->library('email');
    //SMTP & mail configuration
    $config = Array(
    'protocol' => 'smtp',
    'smtp_host' => 'ssl://smtp.googlemail.com',
    'smtp_port' => 465,
    'smtp_user' => 'mailer.thinkercorp@gmail.com', // change it to yours
    'smtp_pass' => '00260926', // change it to yours
    'mailtype' => 'html',
    'charset' => 'utf-8',
    'wordwrap' => TRUE,
    'starttls' => TRUE,
    'crlf'     => '\r\n'
    );

    $this->email->initialize($config);
    $this->email->set_mailtype("html");
    $this->email->set_newline("\r\n");

    $this->email->to($date['email']);
    $this->email->cc($emailAdmin);
    $this->email->from('web@ts-shape.co.th','TS-SHAPE THAILAND');
    $this->email->subject('CONTACT #'.$iorder);
    $this->email->message($this->load->view('mail/contact',array('data'=>$data),true));

    //Send email
    $send = $this->email->send();
    if($send){
      return true;
    }
    return false;

  }


  function dev($page=null){

    $this->load->view('mail/contact_demo');
  }

}
