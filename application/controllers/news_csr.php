<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News_csr extends CI_Controller {
	public function index()
	{

        $css   = array('slick/slick.css','slick/slick-theme.css');
        $script=array('slick/slick.min.js');
        $lg  =$this->uri->segment(1);
        $page=$this->uri->segment(2);

				$this->load->library("pagination");
			         $config = array();
			         $config["base_url"] = site_url("news_csr/index");
			         $config["total_rows"] = $this->db->order_by('id','desc')->get_where('news',array('status !='=>'close'))->num_rows();
			         $config["per_page"] = 6;
			         $config["uri_segment"] = 4;

							 $config['full_tag_open'] = "<ul class='ipagi'>";
							 $config['full_tag_close'] = '</ul>';
							 $config['num_tag_open'] = '<li>';
							 $config['num_tag_close'] = '</li>';
							 $config['cur_tag_open'] = '<li class="active"><a href="#">';
							 $config['cur_tag_close'] = '</a></li>';
							 $config['prev_tag_open'] = '<li>';
							 $config['prev_tag_close'] = '</li>';
							 $config['first_tag_open'] = '<li>';
							 $config['first_tag_close'] = '</li>';
							 $config['last_tag_open'] = '<li>';
							 $config['last_tag_close'] = '</li>';

							 $config['last_link'] = '';
						   $config['last_tag_open'] = '';
							 $config['last_tag_close'] = '';

							 $config['prev_link'] = '« Previous';
							 $config['prev_tag_open'] = '<li>';
							 $config['prev_tag_close'] = '</li>';
							 $config['next_link'] = 'Next » </i>';
							 $config['next_tag_open'] = '<li>';
							 $config['next_tag_close'] = '</li>';


			         $this->pagination->initialize($config);
			         $ipage = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

							 $this->db->limit($config["per_page"],$ipage);
							 $sql=$this->db->order_by('id','desc')->get_where('news',array('status !='=>'close'));
							 $numRow= $sql->num_rows();

							 if($numRow>0){
								 $nn = $sql->result_array();
							 }else{
								 $nn = false;
							 }



				// $nn = $this->db->order_by('id','desc')->get_where('news',array('status !='=>'close'))->result_array();
        $data = array('lg'=>$lg,'page'=>$page,'content'=>'news_csr_view','script'=>$script,'css'=>$css,'seo'=>$this->_seo(),'nn'=>$nn);

        $this->load->view('template',$data);

        }

        public function detail($id=null,$title=null)
	{

        $css   = array('slick/slick.css','slick/slick-theme.css');
        $script=array('slick/slick.min.js');
        $lg  =$this->uri->segment(1);
        $page=$this->uri->segment(2);


				$n=$this->db->get_where('news',array('id'=>$id))->row_array();
        $data = array('lg'=>$lg,'page'=>$page,'content'=>'news_csr_detail_view','script'=>$script,'css'=>$css,'seo'=>$this->_seo(),'n'=>$n);

        $this->load->view('template',$data);

        }
        private function _seo($title=null,$lg=null){

                $s = "<title>NEWS / CSR - TS-Shape :: THAI SUMMIT SHAPE CORP CO.,LTD. </title>";
                return $s;

        }
}
