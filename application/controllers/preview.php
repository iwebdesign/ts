<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preview extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index($guPage=null)
  {


    $css   = array('slick/slick.css','slick/slick-theme.css');
    $script=array('slick/slick.min.js');
    $lg  =$this->uri->segment(1);
    $page=$this->uri->segment(2);
    $data = array('lg'=>$lg,'page'=>$page,'content'=>'preview_view','script'=>$script,'css'=>$css,'seo'=>$this->_seo($guPage),'guPage'=>$guPage);
    $this->load->view('template',$data);
  }



  function suppliers($guPage="suppliers")
  {


    $css   = array('slick/slick.css','slick/slick-theme.css');
    $script=array('slick/slick.min.js');
    $lg  =$this->uri->segment(1);
    $page=$this->uri->segment(2);
    $data = array('lg'=>$lg,'page'=>$page,'content'=>'preview_suppliers_view','script'=>$script,'css'=>$css,'seo'=>$this->_seo($guPage),'guPage'=>$guPage);
    $this->load->view('template',$data);
  }

  function roll($guPage="roll")
  {


    $css   = array('slick/slick.css','slick/slick-theme.css');
    $script=array('slick/slick.min.js');
    $lg  =$this->uri->segment(1);
    $page=$this->uri->segment(2);
    $data = array('lg'=>$lg,'page'=>$page,'content'=>'preview_roll_view','script'=>$script,'css'=>$css,'seo'=>$this->_seo($guPage),'guPage'=>$guPage);
    $this->load->view('template',$data);
  }


  function roll_parts($guPage="roll_parts")
  {


    $css   = array('slick/slick.css','slick/slick-theme.css');
    $script=array('slick/slick.min.js');
    $lg  =$this->uri->segment(1);
    $page=$this->uri->segment(2);
    $data = array('lg'=>$lg,'page'=>$page,'content'=>'preview_roll_parts_view','script'=>$script,'css'=>$css,'seo'=>$this->_seo($guPage),'guPage'=>$guPage);
    $this->load->view('template',$data);
  }


  function engineer($guPage="engineer")
  {


    $css   = array('slick/slick.css','slick/slick-theme.css');
    $script=array('slick/slick.min.js');
    $lg  =$this->uri->segment(1);
    $page=$this->uri->segment(2);
    $data = array('lg'=>$lg,'page'=>$page,'content'=>'preview_engineer_view','script'=>$script,'css'=>$css,'seo'=>$this->_seo($guPage),'guPage'=>$guPage);
    $this->load->view('template',$data);
  }

  function safety($guPage="safety")
  {


    $css   = array('slick/slick.css','slick/slick-theme.css');
    $script=array('slick/slick.min.js');
    $lg  =$this->uri->segment(1);
    $page=$this->uri->segment(2);
    $data = array('lg'=>$lg,'page'=>$page,'content'=>'preview_safety_view','script'=>$script,'css'=>$css,'seo'=>$this->_seo($guPage),'guPage'=>$guPage);
    $this->load->view('template',$data);
  }




  private function _seo($title=null,$lg=null){
          $s = "<title>Preview ".$title." - TS-Shape :: THAI SUMMIT SHAPE CORP CO.,LTD. </title>";
          return $s;

  }

}
