<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Roll_forming extends CI_Controller {

        public function index()
	{
        $css   = array('slick/slick.css','slick/slick-theme.css');
        $script=array('slick/slick.min.js');
        $lg  =$this->uri->segment(1);
        $page=$this->uri->segment(2);
        $data = array('lg'=>$lg,'page'=>$page,'content'=>'roll_forming_view','script'=>$script,'css'=>$css);
        
        $this->load->view('template',$data);

        }
        
        public function why_roll_forming()
	{
        $css   = array('slick/slick.css','slick/slick-theme.css');
        $script=array('slick/slick.min.js');
        $lg  =$this->uri->segment(1);
        $page=$this->uri->segment(2);
        $data = array('lg'=>$lg,'page'=>$page,'content'=>'why_roll_forming','script'=>$script,'css'=>$css);
        
        $this->load->view('template',$data);

        }
        

        public function roll_forming_parts()
	{
        $css   = array('slick/slick.css','slick/slick-theme.css');
        $script=array('slick/slick.min.js');
        $lg  =$this->uri->segment(1);
        $page=$this->uri->segment(2);
        $data = array('lg'=>$lg,'page'=>$page,'content'=>'roll_forming_parts','script'=>$script,'css'=>$css);
        
        $this->load->view('template',$data);

	}
}
