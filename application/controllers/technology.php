<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Technology extends CI_Controller {
	public function engineering()
	{

                $css   = array('slick/slick.css','slick/slick-theme.css');
                $script=array('slick/slick.min.js');
                $lg  =$this->uri->segment(1);
                $page=$this->uri->segment(2);
                $data = array('lg'=>$lg,'page'=>$page,'content'=>'engineering_view','script'=>$script,'css'=>$css,'seo'=>$this->_seo());
                
                $this->load->view('template',$data);

        }
        
        public function safety_quality()
	{

                $css   = array('slick/slick.css','slick/slick-theme.css');
                $script=array('slick/slick.min.js');
                $lg  =$this->uri->segment(1);
                $page=$this->uri->segment(2);
                $data = array('lg'=>$lg,'page'=>$page,'content'=>'safety_quality_view','script'=>$script,'css'=>$css,'seo'=>$this->_seo());
                
                $this->load->view('template',$data);

        }
        
        private function _seo($title=null,$lg=null){
            
                $s = "<title>TECHNOLOGY - TS-Shape :: THAI SUMMIT SHAPE CORP CO.,LTD. </title>";
                return $s;

        }
}
