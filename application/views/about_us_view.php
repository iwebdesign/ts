<?php
$headpic = "head_picture_about";
$h = $this->db->get($headpic)->row();
$g = $this->db->get('about')->row_array();
 ?>
<div class="slide other-page">
   <?php if($h->picture!=""): ?>
     <img src="<?=base_url('img/'.$h->picture.'?v=777');?>">
   <?php else: ?>
   <img src="<?=base_url('img/head-about.jpg?v=77');?>">
   <?php endif;?>
</div>


<section class="about-1">
<div class="grid-container">
  <?=$g['company_profile_'.$lg];?>
  <?=$g['management_'.$lg];?>
  <?=$g['mission_vision_'.$lg];?>
  <?=$g['parent_companies_'.$lg];?>
  <?=$g['company_policy_'.$lg];?>

</div>
</section>


<script type="text/javascript">
    $(document).ready(function(){
      $('.slide').slick({
          lazyLoad: 'ondemand'
      });
    });

    $(".modal-pic").on('click', function() {

     var img = $(this).find('img');
     var path= img.attr('src');
      $.fancybox.open([
        {
          src  : path,
          opts : {
            caption : '',
            // thumb   : path
          }
        }
      ], {
        thumbs : {
          autoStart : false
        }
      });

    });
  </script>
