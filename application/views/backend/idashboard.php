<?php

$log = $this->db->limit(5)->order_by('id','desc')->get('log_admin')->result();
// $d = $this->db->get('dev_mode')->row();
// $mi= $this->db->get('microsite')->result();
// $admin = $this->db->get('users_main')->result();
?>

<!--
<div class="row">
<div class="col-md-12">
<div class="card shadow mb-4">
<div class="card-header py-3">
<h6 class="m-0 font-weight-bold text-primary"><i class="far fa-user-circle h2 align-middle"></i>  ผู้ดูแลโครงการจองห้อง (สำหรับจองห้อง)</h6>
</div>
</div>
</div>
</div> -->

<div class="col-lg-7 mb-4">

  <!-- Approach -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Admin login (5 latest)</h6>
    </div>
    <div class="card-body">
      <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Username</th>
          <th scope="col">Level</th>
          <th scope="col">Platform</th>
          <th scope="col">ip</th>
          <th scope="col">DateTime</th>
        </tr>
      </thead>
      <tbody>
      <?php $i=1;foreach($log as $r): ?>
        <tr>
          <th scope="row"><?=$i++;?></th>
          <td><?=$r->username;?></td>
          <td><?=$r->level?></td>
          <td><?=$r->agent?></td>
          <td><?=$r->ip;?></td>
          <td><?=$r->datetime;?></td>
        </tr>
      <?php endforeach;?>
      </tbody>
    </table>
    </div>
  </div>

</div>
