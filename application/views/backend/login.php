<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>LOGIN :: ADMINISTATOR</title>
    <link rel="stylesheet" href="<?=base_url('css/login.css?v='.rand());?>">
  </head>
  <body>

<div class="box-login">
  <form id="formLogin" data-abide novalidate>
   <div class="grid-x grid-padding-x padding-1">
    <div class="cell small-12 text-center">
      <img src="<?=base_url('img/logo.png');?>" width="120" alt="TS SHAPE">
      <hr/>
    </div>
    <div class="medium-12 cell padding-top-1">
     <label>Email or Username
       <input type="text" placeholder="" name="username" required>
     </label>
   </div>

   <div class="medium-12 cell">
    <label>Password
      <input type="password" placeholder="" name="password" required>
    </label>
  </div>


   <div class="medium-12 cell">
     <button class="button small expanded" type="submit">LOGIN</button>
   </div>
  </div>
</form>
</div>
<script src="<?=base_url('js/jquery.js?v=1');?>"></script>
<script src="<?=base_url('js/foundation.min.js?v=1');?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
$(document).foundation();
$(document)
  // field element is invalid
  .on("invalid.zf.abide", function(ev,elem) {
    console.log("Field id "+ev.target.id+" is invalid");
  })
  // field element is valid
  .on("valid.zf.abide", function(ev,elem) {
    console.log("Field name "+elem.attr('name')+" is valid");
  })
  // form validation failed
  .on("forminvalid.zf.abide", function(ev,frm) {
    console.log("Form id "+ev.target.id+" is invalid");
  })
  // form validation passed, form will submit if submit event not returned false
  .on("formvalid.zf.abide", function(ev,frm) {
    console.log("Form id "+frm.attr('id')+" is valid");
    // ajax post form
  })
  // to prevent form from submitting upon successful validation
  .on("submit", function(ev) {
    ev.preventDefault();
    console.log("Submit for form id "+ev.target.id+" intercepted");

    $.ajax({
                  type: "POST",
                  url: "<?=site_url('login/checklogin');?>",
                  data: $('#formLogin').serialize(),
                  success: function(msg) {
                      var msg = $.parseJSON(msg);
                      if(msg.status==0)
                      {
                          Swal.fire({
                          position: 'center-center',
                          icon: 'waring',
                          title: msg.text,
                          showConfirmButton: false,
                          timer: 1500
                        })
                      }
                      if(msg.status==1)
                      {
                        Swal.fire({
                        position: 'center-center',
                        icon: 'waring',
                        title: msg.text,
                        showConfirmButton: false,
                        timer: 1500
                      })
                      }
                      if(msg.status==2)
                      {
                        Swal.fire({
                        position: 'center-center',
                        icon: 'success',
                        title: msg.text,
                        showConfirmButton: false,
                        timer: 1500
                      }).then((result) => {
                          window.location.replace("<?=site_url('backend');?>");
                        })



                      }
                  }
        });



  });

</script>

  </body>
</html>
