<?php
$page = $this->uri->segment(2);
?>
<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion <?=$page==""?"toggled":"";?>" id="accordionSidebar">
  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=site_url('backend');?>">
    <div class="sidebar-brand-icon">
      <img src="<?=base_url('img/logo.png?v=1');?>" alt="Logo" width="70" >
    </div>
    <div class="sidebar-brand-text mx-3">TS - SHAPE</div>
  </a>
  <!-- Divider -->
  <hr class="sidebar-divider my-0">
  <!-- Nav Item - Dashboard -->
  <hr class="sidebar-divider d-none d-md-block">
  <div class="sidebar-heading"> TS-SHAPE</div>
  <li class="nav-item <?=$page==""||$page==false?"active":"";?>">
    <a class="nav-link" href="<?=site_url('backend');?>">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>DASHBOARD</span></a>
    </li>

    <li class="nav-item <?=$page=="home_slide"||$page=="box_intro"||$page=="home_intro"||$page=="home_intro_design"?"active":"";?>">
      <a class="nav-link <?=$page=="home_slide"||$page=="box_intro"||$page=="home_intro"||$page=="home_intro_design"?"":"collapsed";?>" href="#" data-toggle="collapse" data-target="#collapseUtilitieshome_slide" aria-expanded="<?=$page=="home_slide"||$page=="box_intro"||$page=="home_intro"||$page=="home_intro_design"?"true":"false";?>" aria-controls="collapseUtilities">
        <i class="fas fa-home"></i>
        <span>HOME PAGE</span>
      </a>
      <div id="collapseUtilitieshome_slide" class="collapse <?=$page=="home_slide"||$page=="box_intro"||$page=="home_intro"||$page=="home_intro_design"?"show":"";?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">MANAGEMENT</h6>
          <a class="collapse-item <?=$page=="home_slide"?"active":"";?>" href="<?=site_url('backend/home_slide');?>"> <?=$page=="home_slide"?"&#8658;":"";?> Home slide</a>
          <a class="collapse-item <?=$page=="box_intro"?"active":"";?>" href="<?=site_url('backend/box_intro');?>"> <?=$page=="box_intro"?"&#8658;":"";?> Box intro</a>
          <a class="collapse-item <?=$page=="home_intro"?"active":"";?>" href="<?=site_url('backend/home_intro/edit/1');?>"> <?=$page=="home_intro"?"&#8658;":"";?> Home intro</a>
          <a class="collapse-item <?=$page=="home_intro_design"?"active":"";?>" href="<?=site_url('backend/home_intro_design');?>"> <?=$page=="home_intro_design"?"&#8658;":"";?> Home intro design</a>
        </div>
      </div>
    </li>

    <li class="nav-item <?=$page=="head_picture_about"||$page=="about"||$page=="suppliers"?"active":"";?>">
      <a class="nav-link <?=$page=="head_picture_about"||$page=="about"||$page=="suppliers"?"":"collapsed";?>" href="#" data-toggle="collapse" data-target="#collapseUtilitieshead_picture_about" aria-expanded="<?=$page=="head_picture_about"||$page=="about"||$page=="suppliers"?"true":"false";?>" aria-controls="collapseUtilities">
      <i class="far fa-star"></i>
        <span>ABOUT US PAGE</span>
      </a>
      <div id="collapseUtilitieshead_picture_about" class="collapse <?=$page=="head_picture_about"||$page=="about"||$page=="suppliers"?"show":"";?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">MANAGEMENT</h6>
          <a class="collapse-item <?=$page=="head_picture_about"?"active":"";?>" href="<?=site_url('backend/head_picture_about');?>"> <?=$page=="head_picture_about"?"&#8658;":"";?> Head picture</a>
          <a class="collapse-item <?=$page=="about"?"active":"";?>" href="<?=site_url('backend/about');?>"> <?=$page=="about"?"&#8658;":"";?> About us </a>
            <a class="collapse-item <?=$page=="suppliers"?"active":"";?>" href="<?=site_url('backend/suppliers');?>"> <?=$page=="suppliers"?"&#8658;":"";?> Suppliers </a>
        </div>
      </div>
    </li>



    <li class="nav-item <?=$page=="head_picture_roll"||$page=="roll"||$page=="roll_parts"?"active":"";?>">
      <a class="nav-link <?=$page=="head_picture_roll"||$page=="roll"||$page=="roll_parts"?"":"collapsed";?>" href="#" data-toggle="collapse" data-target="#collapseUtilitieshead_picture_roll" aria-expanded="<?=$page=="head_picture_roll"||$page=="roll"||$page=="roll_parts"?"true":"false";?>" aria-controls="collapseUtilities">
      <i class="fas fa-cubes"></i>
        <span>ROLL FORMING PAGE</span>
      </a>
      <div id="collapseUtilitieshead_picture_roll" class="collapse <?=$page=="head_picture_roll"||$page=="roll"||$page=="roll_parts"?"show":"";?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">MANAGEMENT</h6>
          <a class="collapse-item <?=$page=="head_picture_roll"?"active":"";?>" href="<?=site_url('backend/head_picture_roll');?>"> <?=$page=="head_picture_roll"?"&#8658;":"";?> Head picture</a>
          <a class="collapse-item <?=$page=="roll"?"active":"";?>" href="<?=site_url('backend/roll');?>"> <?=$page=="roll"?"&#8658;":"";?> Why roll forming </a>
            <a class="collapse-item <?=$page=="roll_parts"?"active":"";?>" href="<?=site_url('backend/roll_parts');?>"> <?=$page=="roll_parts"?"&#8658;":"";?> Roll forming parts</a>
        </div>
      </div>
    </li>

    <li class="nav-item <?=$page=="head_picture_engineer"||$page=="engineer"||$page=="safety_quality"?"active":"";?>">
      <a class="nav-link <?=$page=="head_picture_engineer"||$page=="engineer"||$page=="safety_quality"?"":"collapsed";?>" href="#" data-toggle="collapse" data-target="#collapseUtilitieshead_picture_engineer" aria-expanded="<?=$page=="head_picture_engineer"||$page=="engineer"||$page=="safety_quality"?"true":"false";?>" aria-controls="collapseUtilities">
      <i class="fas fa-microchip"></i>
        <span>TECHNOLOGY PAGE</span>
      </a>
      <div id="collapseUtilitieshead_picture_engineer" class="collapse <?=$page=="head_picture_engineer"||$page=="engineer"||$page=="safety_quality"?"show":"";?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">MANAGEMENT</h6>
          <a class="collapse-item <?=$page=="head_picture_engineer"?"active":"";?>" href="<?=site_url('backend/head_picture_engineer');?>"> <?=$page=="head_picture_engineer"?"&#8658;":"";?> Head picture</a>
          <a class="collapse-item <?=$page=="engineer"?"active":"";?>" href="<?=site_url('backend/engineer');?>"> <?=$page=="engineer"?"&#8658;":"";?> Engineering </a>
          <a class="collapse-item <?=$page=="safety_quality"?"active":"";?>" href="<?=site_url('backend/safety_quality');?>"> <?=$page=="safety_quality"?"&#8658;":"";?> Safety Quality </a>
        </div>
      </div>
    </li>

    <li class="nav-item <?=$page=="head_picture_news"||$page=="news"?"active":"";?>">
      <a class="nav-link <?=$page=="head_picture_news"||$page=="news"?"":"collapsed";?>" href="#" data-toggle="collapse" data-target="#collapseUtilitieshead_picture_news" aria-expanded="<?=$page=="head_picture_news"||$page=="news"?"true":"false";?>" aria-controls="collapseUtilities">
      <i class="far fa-newspaper"></i>
        <span>NEWS PAGE</span>
      </a>
      <div id="collapseUtilitieshead_picture_news" class="collapse <?=$page=="head_picture_news"||$page=="news"?"show":"";?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">MANAGEMENT</h6>
          <a class="collapse-item <?=$page=="head_picture_news"?"active":"";?>" href="<?=site_url('backend/head_picture_news');?>"> <?=$page=="head_picture_news"?"&#8658;":"";?> Head picture</a>
          <a class="collapse-item <?=$page=="news"?"active":"";?>" href="<?=site_url('backend/news');?>"> <?=$page=="news"?"&#8658;":"";?> NEWS</a>

        </div>
      </div>
    </li>

    <li class="nav-item <?=$page=="head_picture_career"||$page=="career"?"active":"";?>">
      <a class="nav-link <?=$page=="head_picture_career"||$page=="career"?"":"collapsed";?>" href="#" data-toggle="collapse" data-target="#collapseUtilitieshead_picture_career" aria-expanded="<?=$page=="head_picture_career"||$page=="career"?"true":"false";?>" aria-controls="collapseUtilities">
      <i class="fas fa-user-tie"></i>
        <span>CAREER PAGE</span>
      </a>
      <div id="collapseUtilitieshead_picture_career" class="collapse <?=$page=="head_picture_career"||$page=="career"?"show":"";?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">MANAGEMENT</h6>
          <a class="collapse-item <?=$page=="head_picture_career"?"active":"";?>" href="<?=site_url('backend/head_picture_career');?>"> <?=$page=="head_picture_career"?"&#8658;":"";?> Head picture</a>
          <a class="collapse-item <?=$page=="career"?"active":"";?>" href="<?=site_url('backend/career');?>"> <?=$page=="career"?"&#8658;":"";?> Career</a>

        </div>
      </div>
    </li>




    <li class="nav-item <?=$page=="head_picture_contact"||$page=="global_location"||$page=="list_contact"?"active":"";?>">
      <a class="nav-link <?=$page=="head_picture_contact"||$page=="global_location"||$page=="list_contact"?"":"collapsed";?>" href="#" data-toggle="collapse" data-target="#collapseUtilitieshead_picture_contact" aria-expanded="<?=$page=="head_picture_contact"||$page=="global_location"||$page=="list_contact"?"true":"false";?>" aria-controls="collapseUtilities">
        <i class="fas fa-envelope-square"></i>
        <span>CONTACT PAGE</span>
      </a>
      <div id="collapseUtilitieshead_picture_contact" class="collapse <?=$page=="head_picture_contact"||$page=="global_location"||$page=="list_contact"?"show":"";?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">MANAGEMENT</h6>
          <a class="collapse-item <?=$page=="head_picture_contact"?"active":"";?>" href="<?=site_url('backend/head_picture_contact');?>"> <?=$page=="head_picture_contact"?"&#8658;":"";?> Head picture</a>
          <a class="collapse-item <?=$page=="global_location"?"active":"";?>" href="<?=site_url('backend/global_location');?>"> <?=$page=="global_location"?"&#8658;":"";?> Global location</a>
          <a class="collapse-item <?=$page=="list_contact"?"active":"";?>" href="<?=site_url('backend/list_contact');?>"> <?=$page=="list_contact"?"&#8658;":"";?> List contact</a>
        </div>
      </div>
    </li>
    <hr class="sidebar-divider">

    <div class="sidebar-heading">Addons</div>

    <li class="nav-item <?=$page=="admin_manager"?"active":"";?>">
      <a class="nav-link <?=$page=="admin_manager"?"":"collapsed";?>" href="#" data-toggle="collapse" data-target="#collapseUtilitiesadmin_manager" aria-expanded="<?=$page=="admin_manager"?"true":"false";?>" aria-controls="collapseUtilities">
        <i class="fas fa-users"></i>
        <span>จัดการผู้ดูแลเว็บ</span>
      </a>
      <div id="collapseUtilitiesadmin_manager" class="collapse <?=$page=="admin_manager"?"show":"";?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">MANAGEMENT</h6>
          <a class="collapse-item <?=$page=="admin_manager"?"active":"";?>" href="<?=site_url('backend/admin_manager');?>"> <?=$page=="admin_manager"?"&#8658;":"";?>ผู้ดูแลเว็บ</a>
        </div>
      </div>
    </li>

    <!--
    <li class="nav-item <?=$page=="filemanager"?"active":"";?>">
    <a class="nav-link" href="<?=site_url('backend/filemanager');?>">
    <i class="fas fa-folder-open"></i>
    <span>FILE MANAGER</span></a>
  </li>
-->

<li class="nav-item <?=$page=="setting"?"active":"";?>">
  <a class="nav-link" href="<?=site_url('backend/setting/edit/1');?>">
    <i class="fas fa-cogs"></i>
    <span>SETTING</span></a>
</li>

<li class="nav-item <?=$page=="filemanager"?"active":"";?>">
  <a class="nav-link" href="<?=site_url('backend/filemanager');?>">
    <i class="far fa-folder"></i>
    <span>FILE MANAGER</span></a>
</li>



  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">
  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->
