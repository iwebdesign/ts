<?php
$ipage = $this->uri->segment(2);
?>
<!DOCTYPE html>
<html>
<head><title><?=isset($title_page)?$title_page:"";?>  Admin :: TS-SHAPE</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <link href="<?=base_url('favicon.ico');?>" rel="shortcut icon" type="image/x-icon" />
  <link href="<?=base_url('vendor/fontawesome-free/css/all.min.css?V=1');?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css?v=1"/>

  <?php
  foreach($css_files as $file): ?>
  <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<link href="<?=base_url('css/iadmin.css?v=1');?>" rel="stylesheet" type="text/css">
<?php foreach($js_files as $file): ?>
  <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style>
body{
  font-size:0.75rem;
}
.btn,.form-control{
  font-size:0.85rem;
}
a.ijax .text{
  color:#fff;
}
.gc-container span.page-number-input-container {
  padding: 0px 10px !important;
}
.sidebar .nav-item .nav-link span {
  font-size: .68rem;
}
.sidebar .nav-item .collapse .collapse-inner, .sidebar .nav-item .collapsing .collapse-inner {
  font-size: .68rem;
}
.sidebar .nav-item .nav-link {
  padding: 0.7rem;
}
.thumbnail-sortable .caption{
  padding:10px;
}
.isort .card{
  min-height:150px;
}

.isort h3{
  font-size:18px;
  color:#4C71DD
}
.fancybox-slide--iframe .fancybox-content {
  max-width  : 1300px;
  width:96%;
  min-height : 500px;
  max-height : 98%;
  margin: 0;
}
.numpack{
  background-color:#fff;
  padding:2px 5px;
  color:#4d4d4d;
  font-size:9px;
}
.swal2-container {
  z-index: 999999 !important;
}

#report-error p{
  font-size:1rem;
  color:red;
}

#report-error{
  background:transparent !important;
}

.spinner {
  width: 40px;
  height: 40px;
  position: relative;
  margin: 200px auto;
}

.double-bounce1, .double-bounce2 {
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background-color: #fff;
  opacity: 0.6;
  position: absolute;
  top: 0;
  left: 0;
  -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
  animation: sk-bounce 2.0s infinite ease-in-out;
}

.double-bounce2 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

@-webkit-keyframes sk-bounce {
  0%, 100% { -webkit-transform: scale(0.0) }
  50% { -webkit-transform: scale(1.0) }
}

@keyframes sk-bounce {
  0%, 100% {
    transform: scale(0.0);
    -webkit-transform: scale(0.0);
    } 50% {
      transform: scale(1.0);
      -webkit-transform: scale(1.0);
    }
  }

  .iloading{
    display:none;
    width:100%;
    height:100%;
    position: fixed;
    z-index: 999;
    background-color: rgba(0, 1, 0, 0.6);
    background: rgba(0, 1, 0, 0.6);
    color: rgba(0, 1, 0, 0.6);
  }
  a.send-confirm{
    color:#fff !important;width:100px;margin-top:5px;margin-bottom:5px;
    cursor: pointer;
  }

  .icursor{
    cursor:pointer;
    transition: transform .5s;
  }
  .icursor:hover{
    transform: scale(1.04);
  }

  .sidebar{
    width: 20rem !important;
  }
  .sidebar .nav-item .nav-link{
    width:16rem;
  }


  <?php  if($ipage=="head_picture_engineer"||$ipage=="head_picture_contact"||$ipage=="head_picture_career"||$ipage=="head_picture_news"||$ipage=="head_picture_roll"||$ipage=="head_picture_about"){ ?>
    .gc-search-row,.search-button,.t20,.r10,.r5 {
      display:none !important;
    }

    /* #save-and-go-back-button,#cancel-button,#report-success a{
      display:none !important;
    } */

    <?php } ?>
    <?php  if($ipage=="home_intro"||$ipage=="setting"||$ipage=="analytic_intro"){ ?>
      #save-and-go-back-button,#cancel-button,#report-success a{
        display:none !important;
      }
      <?php } ?>

      <?php  if($ipage=="reserve_rights"):?>
      .only-desktops,.only-mobiles{
        display:none !important;
      }

      <?php endif;?>



      .bg-gradient-primary {
        background-color: #334E5C;
        background-image: linear-gradient(to top, #051937, #0c2641, #17334b, #244153, #334e5c);
      }

      span_pseudo, .chiller_cb span:before, .chiller_cb span:after {
        content: "";
        display: inline-block;
        background: #fff;
        width: 0;
        height: 0.2rem;
        position: absolute;
        transform-origin: 0% 0%;
      }

      .chiller_cb {
        position: relative;
        height: 2rem;
        display: flex;
        align-items: center;
      }
      .chiller_cb input {
        display: none;
      }
      .chiller_cb input:checked ~ span {
        background: #00804A;
        border-color: #00804A;
      }
      .chiller_cb input:checked ~ span:before {
        width: 1rem;
        height: 0.15rem;
        transition: width 0.1s;
        transition-delay: 0.3s;
      }
      .chiller_cb input:checked ~ span:after {
        width: 0.4rem;
        height: 0.15rem;
        transition: width 0.1s;
        transition-delay: 0.2s;
      }
      .chiller_cb input:disabled ~ span {
        background: #ececec;
        border-color: #dcdcdc;
      }
      .chiller_cb input:disabled ~ label {
        color: #dcdcdc;
      }
      .chiller_cb input:disabled ~ label:hover {
        cursor: default;
      }
      .chiller_cb label {
        padding-left: 2rem;
        position: relative;
        z-index: 2;
        cursor: pointer;
        margin-bottom:0;
      }
      .chiller_cb span {
        display: inline-block;
        width: 1.2rem;
        height: 1.2rem;
        border: 2px solid #ccc;
        position: absolute;
        left: 0;
        transition: all 0.2s;
        z-index: 1;
        box-sizing: content-box;
      }
      .chiller_cb span:before {
        transform: rotate(-55deg);
        top: 1rem;
        left: 0.37rem;
      }
      .chiller_cb span:after {
        transform: rotate(35deg);
        bottom: 0.35rem;
        left: 0.2rem;
      }


      tr.itr{
        position:relative;

      }
      .iclose{
        display:none;
        color:#fff;
        text-align:center;
        position:absolute;
        width:100%;
        height:100%;
        font-size: 21px;

        z-index: 2;
        background-color: rgba(34, 44, 44, 0.7);
        background: rgba(34, 44, 44, 0.7);
        left:0;top:0;

      }
      /* tr.iclose::before{
      content:'Double Click open Project';
      color:#fff;
      text-align:center;
      position:absolute;
      z-index: 3;
      width:100%;
      height:100%;
      top:0;left:0;
      display:block;
      } */


      #field-ga_header,#field-ga_body{
        width:100%;
        min-height:250px;
      }

      a.list-group-item{
        cursor: pointer;
      }

      a.list-group-item.active{
        color:#fff;
      }
      .table-label {
        background: #334E5C;
        color:#fff;
        min-height:35px;
      }

      .form-group textarea{
        width:100%;
        min-height:350px;
      }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
  </head>
  <body id="page-top">
    <div class="iloading"><div class="spinner"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div>

    <!-- Page Wrapper -->
    <div id="wrapper">
      <?=$this->load->view('backend/menu');?>

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
          <?=$this->load->view('backend/topbar_admin');?>
          <!-- Begin Page Content -->
          <div class="container-fluid">
            <?php if(isset($breadcrumb)):?>
              <div>
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <!-- <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Library</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Data</li> -->

                    <?php foreach($breadcrumb as $k => $v): ?>
                      <?php if($v!=""): ?>
                        <li class="breadcrumb-item"><a href="<?=$v;?>"><?=$k;?></a></li>
                      <?php else: ?>
                        <li class="breadcrumb-item active" aria-current="page"><?=$k;?></li>
                      <?php endif;?>

                    <?php endforeach;?>
                  </ol>
                </nav>
              </div>
            <?php endif;?>
            <div>
              <h1 class="h3 mb-2 text-gray-800"><?=isset($namepage)?$namepage:"";?></h1>
            </div>

            <?php if(isset($dashboard)): ?>

              <?php if($dashboard=="idashboard"): ?>
                <div class="row">
                  <!-- Earnings (Monthly) Card Example -->
                  <div class="col-xl-3 col-md-6 mb-4 icursor" onclick="window.open('<?=site_url('backend/admin_manager');?>', '_self');">
                    <!-- <div class="col-xl-3 col-md-6 mb-4 icursor"> -->
                    <div class="card border-left-primary shadow h-100 py-2">
                      <div class="card-body">
                        <div class="row no-gutters align-items-center">
                          <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">ผู้ดูแลเว็บ</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->db->get('users')->num_rows();?></div>
                          </div>
                          <div class="col-auto">
                            <i class="fas fa-user fa-2x text-gray-300"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xl-3 col-md-6 mb-4 icursor" onclick="window.open('<?=site_url('backend/list_contact');?>', '_self');">
                    <div class="card border-left-success shadow h-100 py-2">
                      <div class="card-body">
                        <div class="row no-gutters align-items-center">
                          <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">ติดต่อ TS - SHAPE</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->db->get('list_contact')->num_rows();?></div>
                          </div>
                          <div class="col-auto">
                            <i class="fas fa-project-diagram fa-2x text-gray-300"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              <?php endif;?>
            <?php endif; ?>
            <div class="row">
              <div class="col-md-12">
                <div class="card shadow mb-4">
                  <div class="card-body">
                    <?php if(isset($dashboard)): ?>
                      <?=$this->load->view('backend/'.$dashboard);?>
                    <?php endif;?>
                    <?php if(isset($iframe)): ?>
                      <iframe src="<?=site_url();?>filemanager/elfinder.php"  scrolling="no" style=" width:100%;border:none; height: 500px;  overflow: hidden;" ></iframe>
                    <?php else: ?>

                      <?php if(isset($description)): ?>
                        <?=$this->load->view('backend/'.$description);?>
                      <?php endif;?>

                      <?php if(isset($control)&&$control=="yes"){

                        $this->load->view('backend/'.$control_view);
                      } ?>


                      <?php if($output=="component_extra_room"){
                        $this->load->view('backend/component_extra_room');


                      }elseif($output=="component_sort_management"){
                        $this->load->view('backend/component_sort_management');
                      }else{
                        echo $output;
                      }

                      ?>



                    <?php endif;?>

                    <?php if(isset($iframeEdit)): ?>
                      <?=$this->load->view('backend/'.$iframeEdit);?>
                    <?php endif;?>

                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright &copy; TS - Shape 2019</span></a>
            </div>
          </div>
        </footer>
        <!-- End of Footer -->

      </div>
      <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->



    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="logoutModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="logoutModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="<?=site_url('backend/logout');?>">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <script src="<?=base_url('vendor/bootstrap/js/bootstrap.bundle.min.js?v=1');?>"></script>
    <script src="<?=base_url('vendor/jquery-easing/jquery.easing.min.js?v=1');?>"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js?v=2"></script>
    <script src="<?=base_url('js/sb-admin-2.min.js?v=1');?>"></script>
    <script>
    $(document).on('click','.ilogout',function(){
      $('#logoutModal').modal('show')
    });


    $(document).on('change', '.isw', function() {
      var id   = $(this).closest("label.switch").data('id');
      var page = $(this).closest("label.switch").data('page');
      var da = $(this).closest("label.switch").find('input[type="checkbox"]');
      var status;

      if(da.prop("checked") == true){
        status = 'open';
      }

      else if(da.prop("checked") == false){
        status = 'close';
      }

      $.ajax({
        type: "POST",
        url: "<?=site_url('admin_reserve_room/ajax_sw');?>",
        data:{id:id,status:status,page:page},
        success: function(res) {
          console.log(res)
        }
      });
    });


    $().fancybox({selector : '.image-thumbnail'});


    $(document).on('click','.isend-confirm',function(){
      Swal.fire({
        title: 'คุณต้องการส่งเมลยืนยัน?',
        text: "คุณต้องการส่งอีเมลเพื่อยืนยันว่าลูกค้าชำระเงินจองเรียบร้อยหรือไม่",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
      }).then((result) => {
        if (result.value) {

          var id = $(this).data('id');
          var url = "<?=site_url('admin_reserve_room/sendmail_confirm');?>/"+id;
          $('.iloading').fadeIn();
          $.ajax({
            type: "POST",
            url: url,
            data:{id:id},
            success: function(data)
            {
              if(data=="success"){

                $('.iloading').delay(500).fadeOut('300', function() {

                  Swal.fire(
                    {
                      type: 'success',
                      title: '',
                      text: 'ส่งอีเมลยืนยัน ชำระเงินเรียบร้อย',
                    }
                  ).then((result)=>{
                    $('#row-'+id).html('<font color="green" size="5"><i class="far fa-check-circle"></i></font>')
                  })
                });

              }

              if(data=="fail"){
                Swal.fire(
                  {
                    type: 'error',
                    title: '',
                    text: 'ไม่สามารถส่งข้อมูลได้',
                  }
                ).then((result)=>{
                  // window.location.replace("<?=site_url('backend/confirm_payment');?>");
                })
              }

            }
          });
        }
      })



    })


    <?php if($ipage=="project_reserve_room"): ?>
    $(document).on('afterClose.fb', function( e, instance, slide ) {
      // Your code goes here
      // alert('test')
      var id =  slide.opts.$orig[0].id;
      // console.info( slide.opts.$orig[0].id );
      $.ajax({
        type: "POST",
        url: '<?=site_url('admin_reserve_room/checkNumSlideRoom');?>',
        data:{id:id},
        success: function(data)
        {
          $('#'+id).find('.num-slide').html(data);
        }
      });

    });


    <?php endif;?>

  </script>
</body>
</html>
