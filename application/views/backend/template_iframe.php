<?php
 $ipage = $this->uri->segment(3);
?>
<!DOCTYPE html>
<html>
<head><title>  Admin</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="shortcut icon" href="<?=base_url('logo.ico?v=777');?>" type="image/x-icon"/>
<link href="<?=base_url('vendor/fontawesome-free/css/all.min.css?V=1');?>" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css?v=1" />
<?php
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<link href="<?=base_url('css/iadmin.css?v=1');?>" rel="stylesheet" type="text/css">
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style>
body{
	font-size:0.85rem;
}

body{
	font-size:0.85rem;
}
.btn,.form-control{
	font-size:0.85rem;
}

.icard .card-header{
  font-size:14px;
}

.icard .card-body{
  font-size:12px;
}
#report-error p{
	font-size:1rem;
	color:red;
}

#report-error{
	background:transparent !important;
}

<?php  if($ipage=="project_information_room"){ ?>
	#save-and-go-back-button,#cancel-button,#report-success a{
		display:none !important;
	}
<?php } ?>
<?php  if($ipage=="contact_customer"&&$this->input->get('microsite_id')!=""):?>
.button-read{
  display:none !important;
}
<?php endif;?>
</style>
</head>
<body>
  <div style="padding:10px;">
    <div class="row">
      <div class="col-md-12">
      <h6 class="text-info" style="padding-left:20px;"><?=isset($title_page)?$title_page:"";?></h6>
      </div>
    </div>
  <div>
    <?php echo $output; ?>
  </div>

  </div>

  <script src="<?=base_url('vendor/bootstrap/js/bootstrap.bundle.min.js?v=1');?>"></script>
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
  <script src="<?=base_url('vendor/jquery-easing/jquery.easing.min.js?v=1');?>"></script>
  <script src="<?=base_url('js/sb-admin-2.min.js?v=1');?>"></script>
	  <!-- <script src="<?=base_url('js/dropzone.js?v=1');?>"></script> -->
<script>



function goBack() {
  window.history.back();
}

$(document).on('change', '.isw', function() {
  var id   = $(this).closest("label.switch").data('id');
	var page = $(this).closest("label.switch").data('page');
  var da = $(this).closest("label.switch").find('input[type="checkbox"]');
	var status;

    if(da.prop("checked") == true){
      status = 'open';
    //  console.log('open');
    }

    else if(da.prop("checked") == false){
      status = 'close';
      //  da.prop( "checked", true );
      console.log('close');

    }

    $.ajax({
        type: "POST",
        url: "<?=site_url('admin_reserve_room/ajax_sw');?>",
        data:{id:id,status:status,page:page},
        success: function(res) {

					console.log(res)
        }
    });


});


$().fancybox({
  selector : '.image-thumbnail',

});
$(".see-bank").chosen().change(function(){

  $.ajax({
      type: "POST",
      url: "<?=site_url('admin_reserve_room/ajaxPicBank');?>",
      data: {bank_id:$(this).val()},
      success: function(msg) {

          $('.show-bank').html(msg)
      }
  });
})

$(".see-receipt").chosen().change(function(){

  $.ajax({
      type: "POST",
      url: "<?=site_url('admin_reserve_room/ajaxPicRecipt');?>",
      data: {receipt_id:$(this).val()},
      success: function(msg) {

          $('.show-receipt').html(msg)
      }
  });
})

</script>
</body>
</html>
