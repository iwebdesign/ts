<?php


 ?>
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
  <i class="fa fa-bars"></i>
</button>
<ul class="navbar-nav ml-auto">

  <!-- Nav Item - Search Dropdown (Visible Only XS) -->
  <li class="nav-item dropdown no-arrow d-sm-none">
    <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-search fa-fw"></i>
    </a>
    <!-- Dropdown - Messages -->
    <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
      <form class="form-inline mr-auto w-100 navbar-search">
        <div class="input-group">

        </div>
      </form>
    </div>
  </li>


  <li class="nav-item  no-arrow mx-1">
    <a class="nav-link " href="<?=site_url();?>" target="_blank" id="messagesDropdown"  aria-haspopup="true" >
      <i class="fas fa-globe-europe fa-fw" style="font-size:2rem;"></i>
      <!-- Counter - Messages -->
      <span class="badge badge-danger badge-counter">web</span>
    </a>
  </li>


  <div class="topbar-divider d-none d-sm-block"></div>

  <!-- Nav Item - User Information -->
  <li class="nav-item dropdown no-arrow">
    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?=$this->session->userdata('sess_admin')['username'];?></span>
      <?php if($this->session->userdata('sess_admin')['picture']!="" || $this->session->userdata('sess_admin')['picture']!=false): ?>
        <img class="img-profile rounded-circle" src="<?=base_url('img/'.$this->session->userdata('sess_admin')['picture']);?>">
      <?php else: ?>
        <img class="img-profile rounded-circle" src="https://www.chcoc.org/wp-content/uploads/2016/01/avatar.jpeg">
      <?php endif;?>
    </a>
    <!-- Dropdown - User Information -->
    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
      <a class="dropdown-item" href="<?=site_url('backend/admin_manager');?>" >
        <i class="fas fa-user-alt fa-sm fa-fw mr-2 text-gray-400"></i>
          ผู้ดูแลเว็บ
      </a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item ilogout" href="#" >
        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
        ออกจากระบบ (Logout)
      </a>
    </div>
  </li>

</ul>

</nav>
<!-- End of Topbar -->
