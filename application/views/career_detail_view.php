<div class="slide other-page">
  <img src="<?=base_url('img/head-career.jpg?v=77');?>">
</div>
<section class="career about-1 padding-bottom-2">
  <div class="grid-container">
    <div class="grid-x grid-padding-x">
      <div class="cell small-12 margin-bottom-2">
        <h1 class="title-h"><?=$c['position_'.$lg];?></h1>
      </div>
    </div>

    <div class="grid-x grid-padding-x">
      <div class="cell small-12 margin-bottom-2">
        <?=$c['detail_'.$lg];?>
      </div>


      <div class="cell small-12 padding-bottom-2 padding-top-2 detail-career">
        <div class="grid-x grid-padding-x margin-bottom-2">

          <div class="cell small-12 medium-3 h-cereer padding-bottom-1">
            Positions :

          </div>
          <div class="cell small-12 medium-9">
            <?=$c['number'];?>
          </div>

        </div>
        <div class="grid-x grid-padding-x margin-bottom-2">

          <div class="cell small-12 medium-3 h-cereer padding-bottom-1">
            Description :

          </div>
          <div class="cell small-12 medium-9">
            <?=$c['description_'.$lg];?>
          </div>

        </div>


        <div class="grid-x grid-padding-x margin-bottom-2">

          <div class="cell small-12 medium-3 h-cereer padding-bottom-1">
            Location :

          </div>
          <div class="cell small-12 medium-9">
            <?=$c['location_'.$lg];?>
          </div>

        </div>


        <div class="grid-x grid-padding-x margin-bottom-2">

          <div class="cell small-12 medium-3 h-cereer padding-bottom-1">
            Compensation :

          </div>
          <div class="cell small-12 medium-9">
            <?=$c['compensation_'.$lg];?>
          </div>

        </div>


        <div class="grid-x grid-padding-x margin-bottom-2">

          <div class="cell small-12 medium-3 h-cereer padding-bottom-1">
            Contact :

          </div>
          <div class="cell small-12 medium-9">
            <?=$c['contact_'.$lg];?>
          </div>
        </div>


      </div>


      <div class="cell small-12 text-center padding-bottom-3 padding-top-3">
        <a class="back text-uppercase"><< back</a>
      </div>

    </div>
  </div>
</section>
