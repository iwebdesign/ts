<?php

 ?>


<div class="slide other-page">
<img src="<?=base_url('img/head-career.jpg?v=77');?>">
</div>
<section class="career about-1 padding-bottom-2">
<div class="grid-container">
    <div class="grid-x grid-padding-x">
        <div class="cell small-12 margin-bottom-2">
            <h1 class="title-h">CAREER</h1>
        </div>
    </div>

 <div style="padding:0 7px;">


<?php foreach($cc as $c):?>

 <div class="grid-x grid-padding-x align-middle icareer" onclick="window.open('<?=site_url('career/detail/'.$c['id'].'/'.url_title($c['position_en']));?>', '_self');">
        <div class="cell small-8 medium-9 padding-top-1 padding-bottom-1">
            <h4><?=$c['position_'.$lg];?></h4>
            <h5><?=$c['number'];?> positions</h5>
            <h6><?=$c['short_detail_'.$lg];?></h6>

        </div>
        <div class="cell small-1 medium-1">

        </div>
        <div class="cell small-3 medium-2 text-center read-more">
        <i class="fas fa-plus"></i> &nbsp;READ MORE
        </div>
  </div>

<?php endforeach;?>




    </div>
    <div class="grid-x grid-padding-x margin-top-3" >
    <div class="cell small-12 text-center">
              <?=$this->pagination->create_links();?>
    </div>
    </div>
</div>
</section>
