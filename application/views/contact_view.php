<?php
$headpic = "head_picture_contact";
$h = $this->db->get($headpic)->row();
$gg= $this->db->get('global_location')->result_array();

 ?>


<div class="slide other-page">
  <?php if($h->picture!=""): ?>
    <img src="<?=base_url('img/'.$h->picture.'?v=777');?>">
  <?php else: ?>
  <img src="<?=base_url('img/head-contact.jpg?v=777');?>">
  <?php endif;?>

</div>
<section class="contact about-1">
  <div class="grid-container">
    <div class="grid-x grid-padding-x">
      <!-- global location -->
      <div class="cell small-12 medium-5 margin-bottom-1">
        <h1 class="title-h margin-bottom-3">GLOBAL LOCATIONS</h1>
        <div class="grid-x">
          <div class="cell small-12">

            <?php if($gg!=false):
              $i=1; foreach($gg as $g): $x=$i++;
               ?>

            <div class="ibox-address <?=$x > 1 ?"margin-top-2":"";?>">
              <div class="title-address <?=$x == 1 ?"open":"";?> title-address<?=$x;?>">  <?=$g['title_'.$lg];?></div>
                 <div class="box-address box-address<?=$x;?> <?=$x==1?"open":"";?>">
                  <?=$g['detail_'.$lg];?>
                 </div>
              </div>
            <?php endforeach; endif;?>

            <script>

          <?php if($gg!=false):  $ii=1; foreach($gg as $g): $xx=$ii++; ?>
            $('.title-address<?=$xx;?>').on('click',function(){
              $(this).toggleClass('open')
              $('.box-address<?=$xx;?>').toggleClass('open')
            })
            <?php endforeach; endif; ?>
            </script>

              </div>
            </div>


          </div>

          <div class="cell small-12 medium-1"></div>
          <!-- end global location  -->
          <div class="cell small-12 medium-6 margin-bottom-1">
            <h1 class="title-h margin-bottom-3">CONTACT</h1>
            <!-- form -->
            <form id="formContact" data-abide novalidate>
              <div class="grid-x">
                <div class="small-12 cell in-form">
                  <label>Name <font color="#FF9900">*</font></label>
                  <input type="text" id="name" name="name" class="input" placeholder="" required>
                </div>
                <div class="small-12 cell in-form">
                  <label>Email <font color="#FF9900">*</font></label>
                  <input type="email" id="email" class="input" name="email" placeholder="" required>
                </div>
                <div class="small-12 cell in-form">
                  <label>Phone <font color="#FF9900">*</font></label>
                  <input type="text" id="phone" class="input" name="phone" placeholder="" required>
                </div>
                <div class="small-12 cell in-form">
                  <label>Message <font color="#FF9900">*</font></label>
                  <textarea class="input" name="message" id="message" rows="5" required></textarea>
                </div>
                <div class="small-12 cell  text-center padding-2">
                  <button type="submit" class="button ibutton text-uppercase">send</button>
                </div>
              </div>
            </form>
            <!-- end form -->
          </div>


        </div>
      </div>
    </section>
    <div id="map" class="map" style="min-height:600px"></div>

    <script>
    function checkVal(){
      $.each($('.in-form'),function(index, el){
        var va =    $(this).find('.input').val();
        console.log(va)
        if(va!=""){
          $('.in-form').eq(index).find('label').hide()
        }
      });
    }

    $(".in-form").hover(function(){
      $(this).find('label').fadeOut();
      checkVal()
    }, function(){
      $(this).find('label').fadeIn();
      checkVal()
    });


  </script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGnE7yUytjE4zYESPpKBEAETMBPD6LgDM"></script>
<script src="<?=base_url('js/gmap3.min.js?v=1');?>"></script>

<script>
$(window).load(function(){
  var map;
  var mapDiv = $("#map");
  var styles = [
    {
      stylers: [
        { hue: "#4c87d8" },
        { saturation: "0" },
        { lightness: "0" },
      ]
    },
    { featureType: "landscape", stylers: [
      { hue: "#a87732"},
      { saturation: " 0 "},
      { lightness: " 0 "},
    ]
  },
  { featureType: "administrative", stylers: [
    { hue: ""},
    { saturation: ""},
    { lightness: ""},
  ]
},
{ featureType: "road", stylers: [
  { hue: ""},
  { saturation: ""},
  { lightness: ""},
]
},
{ featureType: "water", stylers: [
  { hue: ""},
  { saturation: ""},
  { lightness: ""},
]
},
{ featureType: "poi", stylers: [
  { hue: ""},
  { saturation: ""},
  { lightness: ""},
]
},
];

mapDiv.gmap3({
  map:{

    options:{
      center: [40.46366700000001,-3.7492200000000366],
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl:false,streetViewControl:false,
      zoom: 2,
      scrollwheel: false,
      styles: styles,
    }
  },
  marker:{
    values:[
      {
        address: "1900 Hayes Street, Grand Haven, MI 49417",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape Corp.</h3><p>Grand Haven, Michigan</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Athens, Alabama",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape Corp.</h3><p>Athens, Alabama</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Novi, Michigan",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape Corp.</h3><p>Novi, Michigan</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Hermosillo, Mexico",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape Mexico</h3><p>Hermosillo, Mexico</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Querétaro, Mexico",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape Mexico</h3><p>Querétaro, Mexico</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Paris, France",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape Europe</h3><p>Paris, France</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Frankfurt, Germany",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape Europe</h3><p>Frankfurt, Germany</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Plzeň, Czech Republic",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape Europe</h3><p>Plzeň, Czech Republic</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Pune, India",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape India</h3><p>Pune, India</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Rayong, Thailand",
        data: "<div class=\"gmap-infowindow-content\"><h3>TS-Shape</h3><p>Rayong, Thailand</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker-ts.png');?>"
        }
      },
      {
        address: "Chennai, India",
        data: "<div class=\"gmap-infowindow-content\"><h3>TS-Shape</h3><p>Chennai, India</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker-ts.png');?>"
        }
      },
      {
        address: "Kunshan, China",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape China</h3><p>Kunshan, China</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Guangzhou, China",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape China</h3><p>Guangzhou, China</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Toyota City, Aichi-Ken, JAPAN",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape Japan</h3><p>Toyota, Japan</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Sun building 4F 1-17-10 Asahi Machi Atsugi city Kanagawa, Japan",
        data: "<div class=\"gmap-infowindow-content\"><h3>Shape Japan</h3><p>Atsugi, Japan</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
      {
        address: "Trenton, Ohio",
        data: "<div class=\"gmap-infowindow-content\"><h3>Magnode</h3><p>Trenton, Ohio</p></div>",
        options:
        {
          icon: "<?=base_url('img/marker1.png?v=1');?>"
        }
      },
    ],
    options:{
      draggable: false
    },
    events:{
      click: function(marker, event, context){
        map = $(this).gmap3("get"),
        infowindow = $(this).gmap3({ get:{ name:"infowindow" } });
        if (infowindow){
          infowindow.open(map, marker);
          infowindow.setContent(context.data);
        } else {
          $(this).gmap3({
            infowindow:{
              anchor:marker,
              options:{ content: context.data }
            }
          });
        }
      },
    },
  }
});

});
</script>
