<footer>
<div class="gotop">
 <img src="<?=base_url('img/gotop.svg?v=1');?>">
</div>
<div class="grid-container">
 <div class="grid-x">
    <div class="cell small-5 medium-12 small-order-1">
    <a href="<?=site_url();?>"><img src="<?=base_url('img/'.$this->setting_model->get()['logo'].'?v=11');?>" width="170" alt="ts shape"></a>
    </div>

 <div class="cell small-7 medium-4 small-order-2 ">
     <div class="text-company">
       <?=$this->setting_model->get()['slogan_'.$lg];?>
       </div>
  </div>
  <div class="cell small-12 medium-1 medium-order-3"></div>
  <div class="cell small-12 medium-3 small-order-4 medium-order-4">
      <ul class="menu-footer">
          <li class="<?=$page=="home"||$page==false?"active":"";?>">
              <a href="<?=site_url();?>">TS-SHAPE</a>
          </li>
          <li class="<?=$page=="about_us"?"active":"";?>">
              <a href="<?=site_url('about_us');?>">About us</a>
          </li>
          <li class="<?=$page=="roll_forming"?"active":"";?>">
              <a href="<?=site_url('roll_forming');?>">Roll forming</a>
          </li>
          <li class="<?=$page=="technology"?"active":"";?>">
              <a href="<?=site_url('technology/engineering');?>">Technology</a>
          </li>
          <li class="<?=$page=="news_csr"?"active":"";?>">
              <a href="<?=site_url('news_csr');?>">News / CSR</a>
          </li>
          <li class="<?=$page=="career"?"active":"";?>">
              <a href="<?=site_url('career');?>">Career</a>
          </li>
          <li class="<?=$page=="contact"?"active":"";?>">
              <a href="<?=site_url('contact');?>">Contact</a>
          </li>
      </ul>
  </div>
  <div class="cell small-12 medium-3 small-order-3 medium-order-5">
      <br/>
      CONTACT
<table>
  <tbody>
    <tr>
      <td valign="top" class="icon-footer"><i class="fas fa-map-marker-alt"></i></td>
      <td valign="top">
      <?=$this->setting_model->get()['address_'.$lg];?>

      </td>
    </tr>
    <tr>
      <td valign="middle" class="icon-footer"><i class="fas fa-phone-alt"></i></td>
      <td valign="middle">
      <a href="tel:0038923900"><?=$this->setting_model->get()['phone'];?></a>
      </td>
    </tr>
    <tr>
      <td valign="middle" class="icon-footer"><i class="far fa-envelope"></i></td>
      <td valign="middle">
      <a href="mailto:marketing@ts-shape.co.th"><?=$this->setting_model->get()['email_admin'];?></a>

      </td>
    </tr>
  </tbody>
</table>

  </div>


</div>
</div>
</footer>
