<div class="top-web">
 <div class="grid-container">
 <div class="grid-x">
    <div class="cell small-7 medium-8">
         <div class="text-top">
            <span class="tel-top"><font><i class="fas fa-phone-alt"></i></font><?=$this->setting_model->get()['phone'];?></span>
            <div class="text-address"><font> <i class="fas fa-map-marker-alt"></i> </font> <?=$this->setting_model->get()['address_'.$lg];?></div>
          </div>
    </div>
    <div class="cell small-5 medium-4">
      <div class="float-right">
      <ul class="social">
      <li><a href="<?=$this->setting_model->get()['facebook'];?>"><i class="fab fa-facebook-f"></i></a></li>
      <li><a href="<?=$this->setting_model->get()['google_plus'];?>" ><i class="fab fa-google-plus-g"></i></a></li>
      <li><a href="<?=$this->setting_model->get()['in'];?>" ><i class="fab fa-linkedin-in"></i></a></li>
      </ul>
      </div>
    </div>

  </div>
 </div>
</div>

<header>
<div class="grid-container po-relative">
<div class="grid-x">
<div class="cell small-3 medium-2 large-2">
<a href="<?=site_url('home');?>" class="logo"><img src="<?=base_url('img/'.$this->setting_model->get()['logo'].'?v=11');?>" width="170" alt="ts shape"></a>
</div>
<div class="cell small-5 medium-8 large-9">
 <ul class="menu text-uppercase">
          <li class="<?=$page=="about_us"?"active":"";?>">
              <a href="<?=site_url('about_us');?>">About us</a>
              <ul class="drop-down">
                  <li><a href="<?=site_url('about_us/#companyProfile');?>">Company Profile</a></li>
                  <li><a href="<?=site_url('about_us/#missionVision');?>">Mission & Vision</a></li>
                  <li><a href="<?=site_url('about_us/#parentCompanies');?>">Parent Companies</a></li>
                  <li><a href="<?=site_url('about_us/#companyPolicies');?>">Company Policies</a></li>
                  <li <?=$this->uri->segment(3)=="suppliers"?"class='active'":"";?>><a href="<?=site_url('about_us/suppliers');?>">Suppliers</a></li>
             </ul>
          </li>
          <li class="<?=$page=="roll_forming"?"active":"";?>">
              <a href="#">Roll forming</a>
              <ul class="drop-down">
                  <li><a href="<?=site_url('roll_forming/why_roll_forming');?>">Why Roll Forming</a></li>
                  <li><a href="<?=site_url('roll_forming/roll_forming_parts');?>">Roll Forming Parts</a></li>
             </ul>
          </li>
          <li class="<?=$page=="technology"?"active":"";?>">
              <a href="#">Technology</a>
              <ul class="drop-down">
                  <li><a href="<?=site_url('technology/engineering');?>">Engineering</a></li>
                  <li><a href="<?=site_url('technology/safety_quality');?>">Safety Quality</a></li>
             </ul>
          </li>
          <li class="<?=$page=="news_csr"?"active":"";?>">
              <a href="<?=site_url('news_csr');?>">News / CSR</a>
          </li>
          <li class="<?=$page=="career"?"active":"";?>">
              <a href="<?=site_url('career');?>">Career</a>
          </li>
          <li class="<?=$page=="contact"?"active":"";?>">
              <a href="<?=site_url('contact');?>">Contact</a>
          </li>
 </ul>

</div>
<div class="cell small-4 medium-2 large-1 po-relative">
    <div class="ilang" id="ilang">
        <img src="<?=base_url('img/'.$lg.'.png?v=11');?>"  alt="<?=$lg;?>"> <i class="fas fa-sort-down"></i>
    </div>
    <div id="nav-icon2">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    </div>
</div>

</div>
</div>
</header>
