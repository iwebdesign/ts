<?php
$ss = $this->db->order_by('sort','asc')->get_where('home_slide',array('status !='=>'close'))->result_array();
$bb = $this->db->get('box_intro')->result_array();
$intro = $this->db->get('home_intro')->row_array();
$introDesign = $this->db->get('home_intro_design')->result_array();
 ?>
<div class="slide">

<?php if($ss!=false): $i=1; foreach($ss as $s):?>
  <div class="item">
    <div class="text-slide">
      <?=$s['text_'.$lg];?>
    </div>
    <?php if($s['picture']!=""):?>
    <img <?=$i++>1?"data-lazy":"src";?>="<?=base_url('img/slide/'.$s['picture'].'?v=77');?>">
    <?php else: ?>
    <img src="<?=base_url('img/slide.jpg?v=77');?>">
    <?php endif;?>
  </div>
<?php endforeach; else: ?>
  <div class="item">
    <div class="text-slide">
      <h1>Introducing <font style="color:#FF9900">TS-Shape</font></h1>
      A joint venture between Thai Summit Group and Shape Corp. servicing the local and
      global crash management performance needs of automotive customers throughout India
      and the ASEAN region.
    </div>
    <img src="<?=base_url('img/slide.jpg?v=77');?>">
  </div>
<?php endif;?>

  <!-- <div class="item"><img data-lazy="<?=base_url('img/slide.jpg?v=77');?>"></div> -->

</div>
<script type="text/javascript">
$(document).ready(function(){
  $('.slide').slick({
    fade:true,lazyLoad: 'ondemand',speed:1000,dots:true
  });

});

$('.slide').on('init', function(event, slick){
  $('.slick-slide').eq(0).find('.text-slide').addClass('ani');
});


$('.slide').on('afterChange', function(event, slick, currentSlide){
  $('.slick-current').find('.text-slide').addClass('ani');
  // $('.slick-slide').siblings().find('.text-slide').removeClass('ani');

});
$('.slide').on('beforeChange', function(event, slick, currentSlide,nextSlide){
  // $('.slick-current').find('.text-slide').addClass('ani');
  $('.slick-slide').find('.text-slide').removeClass('ani');

});
</script>
<section class="intro-box po-relative">
  <div class="grid-container" style="margin-top:-10%;">
    <div class="grid-x grid-padding-x align-center" data-equalizer="foo" data-equalizer="bar"> <!-- Aligned to the center -->

<?php if($bb!=false): foreach($bb as $b): ?>

  <div class="cell small-4">
    <div class="in"  data-equalizer-watch="foo">
      <div class="icon-in"  data-equalizer-watch="bar">
        <?php if($b['icon']!=""): ?>
        <img src="<?=base_url('img/'.$b['icon']);?>?v=1">
        <?php endif;?>
      </div>
      <h2><?=$b['title_'.$lg];?></h2>
      <div class="itext">
      <?=$b['detail_'.$lg];?>
      </div>
    </div>
  </div>

<?php endforeach; else: ?>
      <div class="cell small-4">
        <div class="in"  data-equalizer-watch="foo">
          <div class="icon-in"  data-equalizer-watch="bar">
            <img src="<?=base_url('img/icon-engineer.svg');?>">
          </div>
          <h2>Engineering</h2>
          <div class="itext">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.
          </div>
        </div>
      </div>
      <div class="cell small-4">
        <div class="in" data-equalizer-watch="foo">
          <div class="icon-in" data-equalizer-watch="bar">
            <img src="<?=base_url('img/icon-trust.svg');?>">
          </div>
          <h2>Trusted</h2>
          <div class="itext">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.
          </div>
        </div>
      </div>
      <div class="cell small-4">
        <div class="in" data-equalizer-watch="foo">
          <div class="icon-in" data-equalizer-watch="bar">
            <img src="<?=base_url('img/quality.svg');?>">
          </div>
          <h2>&nbsp;&nbsp;Quality</h2>
          <div class="itext">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.
          </div>
        </div>
      </div>
<?php endif;?>
    </div>
  </div>
</section>


<section class="home-1">
  <div class="grid-container">
    <div class="grid-x grid-padding-x">
      <div class="cell small-12"><h1><?=$intro['title_'.$lg];?></h1></div>
      <div class="cell small-12 medium-3">

      </div>
      <div class="cell small-12 medium-9 text-normal">
        <?=$intro['detail_'.$lg];?><br/>
      </div>
    </div>
  </div>
</section>


<section class="home-2">
  <div class="grid-container">


    <div class="grid-x" data-equalizer="foo1">

<?php $x=1; foreach($introDesign as $r):  ?>


<?php if($x++%2==0): ?>
  <div class="cell small-6  po-relative in-home2" data-equalizer-watch="foo1">
    <?php if($r['picture']!=""): ?>
        <div class="in-ab2" style="background-image:url(<?=base_url('img/'.$r['picture'].'?v=1');?>)"></div>
    <?php else: ?>
        <div class="in-ab2" style="background-image:url(<?=base_url('img/home2-2.jpg?v=1');?>)"> </div>
      <?php endif; ?>

</div>

  <div class="cell small-6 padding-x" data-equalizer-watch="foo1">
    <div class="in" style="padding-left:20px;">
      <h2><?=$r['title_'.$lg];?></h2>
      <div class="text-home2">
          <?=$r['detail_'.$lg];?>
      </div>
      <div class="more"><a href="<?=site_url('technology/engineering');?>" title="<?=$r['title_'.$lg];?>"><i class="fas fa-plus"></i></a></div>
    </div>
  </div>

<?php else: ?>
  <div class="cell small-6 padding-x" data-equalizer-watch="foo1">
    <div class="in">
      <h2><?=$r['title_'.$lg];?></h2>
      <div class="text-home2">
          <?=$r['detail_'.$lg];?>
      </div>
      <div class="more"><a href="<?=site_url('roll_forming/why_roll_forming');?>" title="<?=$r['title_'.$lg];?>"><i class="fas fa-plus"></i></a></div>
    </div>
  </div>
  <div class="cell small-6 po-relative in-home2" data-equalizer-watch="foo1">
    <?php if($r['picture']!=""): ?>
        <div class="in-ab" style="background-image:url(<?=base_url('img/'.$r['picture'].'?v=1');?>)"></div>
    <?php else:?>
        <div class="in-ab" style="background-image:url(<?=base_url('img/home2-1.jpg?v=1');?>)">  </div>
    <?php endif; ?>
  </div>


<?php endif;?>






<?php endforeach;?>

    </div>
  </div>
</section>

<?php
$relate = $this->db->order_by('id','desc')->limit(3)->get_where('news',array('status !='=>'close'))->result_array();

 ?>
 <?php if($relate!=false): ?>
<section class="home-news">
  <div class="grid-container">
    <div class="grid-x grid-padding-x">
      <div class="cell small-12">
        <h1 class="title-h">NEWS / CSR</h1>
      </div>
    </div>
    <div style="height:30px"></div>
    <div class="grid-x grid-padding-x" data-equalizer>

      <?php $y=1;foreach($relate as $r): $xx=$y++;?>
       <div class="cell small-6 medium-4 <?=$xx==3?"hide-for-small-only":"";?> inews">
         <a href="<?=site_url('news_csr/detail/'.$id.'/'.url_title($r['title_'.$lg]));?>" class="in" data-equalizer-watch>
           <div class="pic">
             <?php if($r['picture_thumbnail']!=""): ?>
               <img src="<?=base_url('img/'.$r['picture_thumbnail'].'?v=1');?>" alt="">
             <?php else: ?>
               <img src="<?=base_url('img/n'.$xx.'.jpg?v=1');?>" alt="">
             <?php endif;?>

           </div>
           <h3><?=$r['title_'.$lg];?></h3>
           <div class="update-in">
             <font color="#009966"> <?=date("M d, Y", strtotime($r['datetime']));?></font> <font color="#999999">by <?=$r['post_by'];?> <?=$r['user_view'];?> view</font>
           </div>
           <div class="in-text">
             <?=$r['short_detail_'.$lg];?>

           </div>
           <div><div class="more"><a  title="News"><i class="fas fa-plus"></i></a></div></div>
         </a>
       </div>
     <?php endforeach;?>


    </div>

    <div class="grid-x grid-padding-x">
      <div class="cell small-12 text-center padding-top-2">
        <a class="more-all" href="<?=site_url('news_csr');?>">MORE</a>
      </div>
    </div>
  </div>
</section>

<?php endif;?>
