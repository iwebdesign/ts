<!DOCTYPE html><html lang="<?=$this->uri->segment(1);?>"><head><meta charset="UTF-8"><link href="/css/app.css?v=<?=rand();?>" rel="stylesheet"><link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png?v=1"><link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png?v=1"><link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png?v=1"><link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png?v=1"><link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png?v=1"><link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png?v=1"><link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png?v=1"><link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png?v=1"><link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png?v=1"><link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png?v=1"><link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=1"><link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png?v=1"><link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=1"><link rel="manifest" href="/manifest.json"><meta name="msapplication-TileColor" content="#FF9900"><meta name="msapplication-TileImage" content="/ms-icon-144x144.png?v=1"><meta name="theme-color" content="#FF9900"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="ie=edge"> <link rel="icon" href="<?=base_url('favicon.ico?v=1');?>" type="image/x-icon"> <?=isset($seo)?$seo:"<title>TS-Shape :: THAI SUMMIT SHAPE CORP CO.,LTD.</title>";?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" integrity="sha256-qM7QTJSlvtPSxVRjVWNM2OfTAz/3k5ovHOKmKXuYMO4=" crossorigin="anonymous"></script>
  <?php if(isset($css)): foreach($css as $c): ?>
    <link href="<?=base_url($c.'?v=1');?>" rel="stylesheet">
  <?php endforeach; endif;?>
  <script src="/js/jquery.js?v=1"></script>
  <script src="https://cdn.tiny.cloud/1/kgoa8i6nd8s0wjzwe134r8dl30eesiv60ve12otifac7s9ew/tinymce/5/tinymce.min.js"></script>
  <link href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
  <script>

  var emailHeaderConfig = {
    selector: '.tinymce-heading,.title-h,.list-company h2,.text-normal-about,.pic-engi h4,.pic-engi h5',
    menubar: false,
    inline: true,
    plugins: [
      'lists',
      'autolink'
    ],
    toolbar: 'undo redo | bold italic underline',
    valid_elements: 'strong,em,span[style],a[href]',
    valid_styles: {
      '*': 'font-size,font-family,color,text-decoration,text-align'
    },
    powerpaste_word_import: 'clean',
    powerpaste_html_import: 'clean',
    // content_css: [
    //   '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
    // ]
  };
  var emailBodyConfig = {
    selector: '.tinymce-body,.text-in',
    menubar: false,
    inline: true,
    plugins: [
      'link',
      'lists',
      // 'powerpaste',
      'autolink',
      // 'tinymcespellchecker'
    ],
    toolbar: [
      'undo redo | bold italic underline | fontselect fontsizeselect',
      'forecolor backcolor | alignleft aligncenter alignright alignfull | numlist bullist outdent indent'
    ],
    valid_elements: 'p[style],strong,em,span[style],a[href],ul,ol,li',
    valid_styles: {
      '*': 'font-size,font-family,color,text-decoration,text-align'
    },
    powerpaste_word_import: 'clean',
    powerpaste_html_import: 'clean',
    // content_css: [
    //   '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
    // ]
  };

  var emailImgConfig = {
    selector: '.tinymce-img',
    menubar: true,
    inline: false,
    plugins: [
      'image code'
      // 'tinymcespellchecker'
    ],
    toolbar: [
      'image code'
    ],
    images_upload_url: 'postAcceptor.php',

    /* we override default upload handler to simulate successful upload*/
    images_upload_handler: function (blobInfo, success, failure) {
      setTimeout(function () {
        /* no matter what you upload, we will turn it into TinyMCE logo :)*/
        success('http://moxiecode.cachefly.net/tinymce/v9/images/logo.png');
      }, 2000);
    }
  };

  tinymce.init(emailHeaderConfig);
  tinymce.init(emailBodyConfig);
  tinymce.init(emailImgConfig);
  </script>
  <style>
  body,html{
    position: inherit !important;
    width:auto;
    height:auto;
  }
  .fix-control{
    position:fixed;
    width:100%;
    padding:0 0 0;
    background:rgba(34, 44, 44, 1);
    z-index:999;
    left:0;
    top:0;
  }
  .back-web{
    padding:5px 10px;
    font-size: 12px;
    background-color:#424242;
  }
  .spinner {
    width: 70px;
    text-align: center;
    position:absolute;
    left:50%;
    top:15%;
    margin-top:-30px;
    margin-left:-35px;

  }

  .spinner > div {
    width: 18px;
    height: 18px;
    background-color: #fff;

    border-radius: 100%;
    display: inline-block;
    -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
    animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  }

  .spinner .bounce1 {
    -webkit-animation-delay: -0.32s;
    animation-delay: -0.32s;
  }

  .spinner .bounce2 {
    -webkit-animation-delay: -0.16s;
    animation-delay: -0.16s;
  }

  @-webkit-keyframes sk-bouncedelay {
    0%, 80%, 100% { -webkit-transform: scale(0) }
    40% { -webkit-transform: scale(1.0) }
  }

  @keyframes sk-bouncedelay {
    0%, 80%, 100% {
      -webkit-transform: scale(0);
      transform: scale(0);
      } 40% {
        -webkit-transform: scale(1.0);
        transform: scale(1.0);
      }
    }

    #loading{
      position:fixed;
      width:100%;
      height:100%;
      background-color: rgba(160, 92, 56, 0.7);
      background: rgba(160, 92, 56, 0.7);
      color: rgba(160, 92, 56, 0.7);
      z-index: 1000;
      left:0;top:0;
      display:none;
    }

    </style>
  </head>
  <body>
    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>

    </div>
    <?php
    $g = $this->db->get('roll')->row_array();
    $gal = $this->db->order_by('priority','asc')->get('gallery_roll')->result_array();



    $lg=$this->uri->segment(1);
    $mode= $this->uri->segment(4);

    if($mode==false){
      $backweb = 'style="display:none;"';
      $hideElement ='';
    }else{
      $backweb = '';
      $hideElement ='style="display:none;"';
    }

    // echo $_SERVER[REQUEST_URI];
    ?>
    <div class="fix-control">
      <div class="back-web" <?=$backweb;?>><a href="<?=site_url('backend/about');?>"><< Back </a></div>
      <div class="padding-top-1 padding-left-1" style="display:block;">
        <button class="button isave"><i class="far fa-save"></i> Save</button>
        <a class="button success" <?=$hideElement;?> target="_blank" href="<?=site_url('iframe_web/roll/full');?>"><i class="far fa-edit"></i> Full Page Edit</a>
        <div class="float-right po-relative padding-right-1">
          <a href="/<?=$this->lang->switch_uri('th');?>"><img  style="<?=$lg=="en"?"opacity:0.5":"";?>" src="<?=base_url('img/th.png?v=11');?>" width="30" alt="th"></a>
          |
          <a href="/<?=$this->lang->switch_uri('en');?>"><img style="<?=$lg=="th"?"opacity:0.5":"";?>"  src="<?=base_url('img/en.png?v=11');?>" width="30" alt="en"></a>
        </div>
      </div>
    </div>
    <div class="" style="min-height:100px;"></div>

    <section class="roll about-1 padding-bottom-2">
      <div id="roll_<?=$this->uri->segment(1);?>">
           <?=$g['roll_'.$this->uri->segment(1)];?>
      </div>
    </section>


    <section class="pic-gallery margin-bottom-2 margin-top-2">
      <div class="grid-x align-center align-middle list-gallery" id="gallery">
        <?php if($gal==false): ?>


        <?php else:?>
          <?php foreach($gal as $r): ?>

            <div class="cell small-6 medium-3 item">
              <a href="/img/gallery/<?=$r['url'];?>" data-fancybox="gallery" data-caption="<?=$r['title'];?>">
                <img src="/img/gallery/<?=$r['url'];?>" width="100%">
              </a>
            </div>

          <?php endforeach;?>
        <?php endif;?>


      </div>

    </section>

    <div class="grid-container">
      <div class="grid-x grid-padding-x">
        <div class="padding-top-2 padding-bottom-1" style="display:block;width:100%;border:1px #ccc dashed;">
          <div class="cell small-12 text-center">
            <a data-fancybox data-type="iframe" id="addGallery" data-src="<?=site_url('iframe_web/gallery_roll');?>" href="javascript:;" class="button secondary addgallery" > <i class="far fa-list-alt"></i> Add Image Gallery</a>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="reveal custom-modal edit-iframe-pic" id="modal" data-reveal>
      <form id="uploadImg" enctype="multipart/form-data">
        <div class="icontent-modal">
          <div class="grid-x align-middle">
            <div class="cell small-12 medium-7">
              <div class="show-img text-center margin-bottom-2"><img src="<?=base_url('img/no-image.png?v=1');?>"></div>
            </div>
            <div class="cell small-12 medium-5 padding-left-1">
              <label for="imgInp" class="button secondary">Select File</label>
              <input type="file" name="image" class="show-for-sr" id="imgInp">
            </div>
          </div>
          <input type="hidden"  id="idPic" data-class="" value="">

        </div>
        <div class=""></div>
        <hr/>
        <div class="grid-x">
          <div class="cell small-12 text-center">
            <button class="button" type="submit" id="submitUpload">
              <i class="fas fa-cloud-upload-alt"></i> Upload
            </button>
            <button class="button alert" data-close aria-label="Close modal" type="button">
              Cancel
            </button>
          </div>
        </div>
      </form>
    </div> -->

    <div class="reveal custom-modal edit-iframe-pic" id="modalFile" data-reveal>
      <form id="uploadFile" enctype="multipart/form-data" data-abide novalidate>
        <div class="icontent-modal">
          <div class="grid-x align-middle">

            <div class="cell small-12 medium-7">
              <div class="show-img text-center margin-bottom-2"><img src="<?=base_url('img/no-image.png?v=1');?>"></div>
            </div>
            <div class="cell small-12 medium-5 padding-left-1">
              <label for="imgInp" class="button secondary">Select File</label>
              <input type="file" name="image" class="show-for-sr" id="imgInp">
            </div>

            <div class="medium-12 cell">
              <label>Name File
                <input type="text" name="namefile" placeholder="" required>
              </label>
            </div>
            <div class="medium-5 cell padding-left-1">
              <small id="output" ></small>
            </div>
            <div class="cell small-12">
              <label>
                <input type="file" name="file" id="iFile"  required>
                <span class="form-error">
                  Please select file.
                </span>
              </label>
            </div>
            <div class="cell small-12">
              <small>*.png,*.jpg,*.pdf</small>
            </div>
          </div>
          <input type="hidden"  id="idFile" data-id="" value="">
        </div>
        <div class=""></div>
        <hr/>
        <div class="grid-x">
          <div class="cell small-12 text-center">
            <button class="button" type="submit" id="submitUploadFile" data-id="">
              <i class="fas fa-cloud-upload-alt"></i> Upload File
            </button>
            <button class="button alert" data-close aria-label="Close modal" type="button">
              Cancel
            </button>
          </div>
        </div>
      </form>
    </div>


    <div class="reveal custom-modal edit-iframe-pic" id="modalVideo" data-reveal>
      <form id="uploadVideo" enctype="multipart/form-data" data-abide novalidate>
        <div class="icontent-modal">
          <div class="grid-x align-middle">
            <div class="medium-12 cell">
              <label> Link Youtube or Vimeo
                <input type="text" name="youtube" id="inputYoutube" placeholder="" required>
              </label>
            </div>
            <div class="cell small-12 box-thumb" style="display:none"><img id="thumbImg" width="100%"></div>
            <!-- <div class="cell small-12 margin-top-2">
            <label>
            <input type="file" name="file" id="iFile">
            <span class="form-error">
            Please select file.
          </span>
        </label>
      </div>
      <div class="cell small-12">
      <small>*.png,*.jpg (Dimension 600x400 px)</small>
    </div> -->
  </div>
  <input type="hidden"  id="idVideo" data-class="" value="">
</div>
<div class=""></div>
<hr/>
<div class="grid-x">
  <div class="cell small-12 text-center">
    <button class="button" type="submit" id="submitUploadVideo" data-box="" data-id="">
      <i class="fas fa-cloud-upload-alt"></i> Update Video
    </button>
    <button class="button alert" data-close aria-label="Close modal" type="button">
      Cancel
    </button>
  </div>
</div>
</form>
</div>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script src="/js/foundation.min.js?v=1"></script>
<!-- {elapsed_time} -->
<script>


$(document).foundation();

$(document).on("submit", function(ev) {
  ev.preventDefault();

  if(ev.target.id=="uploadVideo"){
    var ibox = $('.box-video');

    $('#modalVideo').addClass('load');
    setTimeout(function(){
      ibox.find('a').attr('data-href',$('#inputYoutube').val());
      ibox.find('img').attr('src',$('#thumbImg').attr('src'));
      $('#modalVideo').removeClass('load').addClass('load-success');
    }, 1000);
    setTimeout(function(){
      ibox.find('a').attr('data-href',$('#inputYoutube').val());
      ibox.find('img').attr('src',$('#thumbImg').attr('src'));
      $('#modalVideo').foundation('close');
    }, 2000);


  }

  if(ev.target.id=="uploadFile"){
    var iBox = $('#submitUploadFile').attr('data-id');

    $('#modalFile').addClass('load');
    var data = new FormData($('#uploadFile')[0]);
    $.ajax({
      url:'<?=site_url('iframe_web/ajaxUploadFileAll');?>',
      type:"post",
      data: data,
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      success: function(data){
        var idata = JSON.parse(data);
        if(idata.status=="ok"){
          $('#modalFile').removeClass('load').addClass('load-success');

          $('.modal-pic').eq(iBox).attr('data-src',idata.pathFile);
          $('.modal-pic').eq(iBox).attr('data-caption',idata.namefile);
          $('.modal-pic').eq(iBox).find('img').attr('src',idata.pathImg);
          setTimeout(function(){
             $('#modalFile').foundation('close');
          }, 2000);



        }

        // if(idata.status=="ok"){
        //   setTimeout(function(){
        //
        //     $('#modalFile').removeClass('load').addClass('load-success');
        //     setTimeout(function(){
        //       $('#modalFile').foundation('close');
        //       var itype = idata.typefile;
        //
        //       var ipath = idata.path;
        //       var idiv = '<div class="out-button"><a href="'+ipath+'" target="_blank" class="button-web grid-x align-middle "><span class="text-uppercase">'+itype.replace('.','')+'</span><div class="cell small-9 text"><div class="padding-left-1">'+idata.namefile+'</div> </div><div class="cell small-1 po-relative"><div class="border-center"></div></div><div class="cell small-2 text-center"><img src="/img/download.svg?v=1" width="32"></div></a></div>';
        //       //var idiv = '<div class="out-button">  <a  class="button-web grid-x align-middle " href="'+idata.path+'" target="_blank"> <div class="cell small-2 text-right"><img src="/img/icon-web.svg" width="35"></div>    <div class="cell small-1 po-relative"><div class="border-center"></div></div><div class="cell small-9 text">'+result.value+'</div>';
        //       //   idiv+= '</a></div>';
        //
        //       var boxList =   $("."+$('#submitUploadFile').data('box'));
        //       boxList.append(idiv);
        //
        //
        //
        //     }, 1000);
        //
        //   }, 1000);
        //
        //
        //
        //
        //
        // }else{
        //   $('#modalFile').removeClass('load');
        //   Swal.fire({
        //     position: 'center-center',
        //     icon: 'warning',
        //     html: '<small>You must insert .jpg,.png,.pdf,.zip </small>',
        //   }).then((result) => {
        //     $('#submitUploadFile').hide();
        //   })
        // }


      }
    })


  }



});


$('.ipdf .modal-pic').on('click',function(){
  var $modal = $('#modalFile');
  $modal.foundation('open');
  $('#idFile').val($(this).data('id'));
  $('#idFile').attr('data-id',$(this).index());
  $('#submitUploadFile').attr('data-id',$(this).index());

})


//
// $('.modal-pic').on('click',function(){
//   var $modal = $('#modalFile');
//   $modal.foundation('open');
//   $('#idPic').val($(this).data('id'));
//   $('#idPic').attr('data-class',$(this).attr('class'));
//   $('#submitUpload').hide();
//
// })

$('.box-video').on('click',function(){
  var $modal = $('#modalVideo');
  $modal.foundation('open');
  $('#submitUploadVideo').attr('data-id',$(this).data('id'));
  // $('#submitUploadVideo').hide();

})

$(document)
.on("closed.zf.reveal", function(ev,elem) {
  $('#idPic').val();
  $('.show-img').html('<img src="<?=base_url('img/no-image.png?v=1');?>">');
  $('#submitUpload').hide();
  $('#modal').removeClass('load-success');


  $('#modalFile').removeClass('load-success');
  $('#uploadFile')[0].reset();
  $('#output').html('');


  $('#modalVideo').removeClass('load-success');
  $('#modalVideo').find('.box-thumb').hide();
  $('#modalVideo').find('#inputYoutube').val('');
  $('#thumbImg').attr("src"," ");
})
.on("open.zf.reveal", function(ev,elem) {

})

$('.pic-engineer').on('click',function(){
  var $modal = $('#modal');
  $modal.foundation('open');
  $('#idPic').val($(this).data('id'));
  $('#idPic').attr('data-class',$(this).attr('class'));
  $('#submitUpload').hide();

})




function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      // $('#blah').attr('src', e.target.result);
      $('.show-img').html('<img src="'+e.target.result+'" style="max-height:270px;">')
      $('#submitUpload').fadeIn();
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp").change(function() {
  readURL(this);
});



$(document).ready(function(){


  $(".addlink").click(function(){
    Swal.fire({
      title: 'Insert link',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'confirm',
      showLoaderOnConfirm: true,
      preConfirm: (itext) => {

        if(itext==""){
          Swal.showValidationMessage(
            `Request failed`
          )

        }else{
          return itext;
        }

      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.value) {
        var idiv = '<div class="out-button">  <a  class="button-web grid-x align-middle " href="//'+result.value+'" target="_blank"> <div class="cell small-2 text-right"><img src="/img/icon-web.svg" width="35"></div>    <div class="cell small-1 po-relative"><div class="border-center"></div></div><div class="cell small-9 text">'+result.value+'</div>';
        idiv+= '</a></div>';

        var boxList =   $("."+$(this).data('box'));
        boxList.append(idiv);
      }
    })
  });

  $(".removelink").click(function(){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        ).then((result) => {
          var boxList =   $("."+$(this).data('box'));
          boxList.html('');
        })
      }
    })
  })
});



$('.addfile').on('click',function(){
  var $modal = $('#modalFile');
  $modal.foundation('open');
  $('#submitUploadFile').hide();
  $('#submitUploadFile').attr('data-box',$(this).data('box'));


})

$("#iFile").change(function() {
  $('#submitUploadFile').fadeIn();
});


/**
* Load the mime type based on the signature of the first bytes of the file
* @param  {File}   file        A instance of File
* @param  {Function} callback  Callback with the result
* @author Victor www.vitim.us
* @date   2017-03-23
*/
function loadMime(file, callback) {

  //List of known mimes
  var mimes = [
    {
      mime: 'image/jpeg',
      pattern: [0xFF, 0xD8, 0xFF],
      mask: [0xFF, 0xFF, 0xFF],
    },
    {
      mime: 'image/png',
      pattern: [0x89, 0x50, 0x4E, 0x47],
      mask: [0xFF, 0xFF, 0xFF, 0xFF],
    }
    // you can expand this list @see https://mimesniff.spec.whatwg.org/#matching-an-image-type-pattern
  ];

  function check(bytes, mime) {
    for (var i = 0, l = mime.mask.length; i < l; ++i) {
      if ((bytes[i] & mime.mask[i]) - mime.pattern[i] !== 0) {
        return false;
      }
    }
    return true;
  }

  var blob = file.slice(0, 4); //read the first 4 bytes of the file

  var reader = new FileReader();
  reader.onloadend = function(e) {
    if (e.target.readyState === FileReader.DONE) {
      var bytes = new Uint8Array(e.target.result);

      for (var i=0, l = mimes.length; i<l; ++i) {
        if (check(bytes, mimes[i])) return callback("Mime: " + mimes[i].mime + " <br> Browser:" + file.type);
      }

      return callback("Mime: unknown <br> Browser:" + file.type);
    }
  };
  reader.readAsArrayBuffer(blob);
}

var fileInput = document.getElementById("iFile");
var output  = document.getElementById("output");
//when selecting a file on the input
fileInput.onchange = function() {
  loadMime(fileInput.files[0], function(mime) {

    //print the output to the screen
    output.innerHTML = mime;
  });
};



$('.isave').on('click',function(){

  $('#loading').fadeIn();
  var values = {
    roll_<?=$this->uri->segment(1);?>:$('#roll_'+'<?=$this->uri->segment(1);?>').html(),


  };
  $.ajax({
    url: "<?=site_url('backend/ajaxAddRoll');?>",
    type: "post",
    data: values ,
    success: function (response) {

      // You will get response from your PHP page (what you echo or print)
      $('#loading').delay(1000).fadeOut(300,function(){
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          html: '<small>Your work has been saved</small>',
          showConfirmButton: false,
          timer: 1500
        })
      })
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log(textStatus, errorThrown);
    }
  });
})



$(".addgallery").fancybox({
  beforeClose : function( instance, current, e ) {
    // $('#gallery').html(' ');
    ajaxReloadGal()
  }
});

function ajaxReloadGal(){
  $.ajax({
    url: '<?=site_url('iframe_web/ajaxReLoadGalRoll');?>',
    type: 'post',
    dataType : 'html',
    data: {param: 'reload'},
    success: function(data){
      if(data !="error"){
        $('#gallery').html(data);
        $("html, body").delay(200).animate({
          scrollTop: $('#gallery').offset().top
        }, 2000);
      }

    }
  })


}


$(document).ready(function() {
  var ibox = $('.box-video');
  var ihref = $('.box-video').find('a').attr('href');
  ibox.find('a').removeAttr('data-fancybox');
  ibox.find('a').attr('data-href',ihref);
  ibox.find('a').removeAttr('href');


  var ipdf = $('.ipdf');

  ipdf.find('a').removeAttr('data-fancybox');
  ipdf.find('a').removeAttr('href');
  ipdf.find('a').removeAttr('data-type');


});


$("#inputYoutube").on("keyup", function(){
  GetImageFromVideoURL()
})

//  https://player.vimeo.com/video/119870637
function GetImageFromVideoURL() {
  var i, image_url, isValidURL, isVValidURL, isEmbValidURL, uniqueIdLength, uniqueID;
  uniqueIdLength = 11;
  image_url = $('#inputYoutube').val();

  var url;
  if (image_url != null) {
    url = image_url;
  }
  else {
    url = "";
  }

  if (url.search("youtube") != -1) {
    isValidURL = image_url.indexOf("www.youtube.com/watch?v=");
    isVValidURL = image_url.indexOf("www.youtube.com/v/");
    isEmbValidURL = image_url.indexOf("www.youtube.com/embed/");

    if (isValidURL == -1 && isVValidURL == -1 && isEmbValidURL == -1) {
      alert("Invalid URL");
      return false;
    }

    if (isValidURL != -1) {
      i = image_url.indexOf("v=");
    }
    else if (isVValidURL != -1) {
      i = image_url.indexOf("v/");
    }
    else if (isEmbValidURL != -1) {
      i = image_url.indexOf("embed/");
      i = i + 4;
    }
    i = i + 2;

    uniqueID = image_url.substr(i, uniqueIdLength);
    imageURL = 'https://img.youtube.com/vi/' + uniqueID + '/maxresdefault.jpg';
    $('#thumbImg').attr("src", imageURL);
    $('.box-thumb').fadeIn();
    return true;
  }
  else if ((url.search("vimeo") != -1)) {
    isVimeoURL = image_url.indexOf("vimeo.com/video");
    isvVimeoURL = image_url.indexOf("www.vimeo.com/video");
    if (isVimeoURL == -1 && isvVimeoURL == -1) {
      alert("Invalid URL");
      return false;
    }

    if (isVimeoURL != -1) {
      i = image_url.indexOf("video/");
    }
    i = i + 6;

    uniqueID = image_url.substr(i, uniqueIdLength);

    $.ajax({
      type: 'GET',
      url: 'https://vimeo.com/api/v2/video/' + uniqueID + '.json',
      jsonp: 'callback',
      dataType: 'jsonp',
      success: function (data) {
        var thumbnail_src = data[0].thumbnail_large;
        $('#thumbImg').attr("src", thumbnail_src);
        $('.box-thumb').fadeIn();
      }
    });
    return true;
  }
  alert("Invalid URL");
  $('#inputYoutube').val("");
  return false;
}
</script>

</body>
</html>
