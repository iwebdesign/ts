<!DOCTYPE html><html lang="<?=$this->uri->segment(1);?>"><head><meta charset="UTF-8"><link href="/css/app.css?v=<?=rand();?>" rel="stylesheet"><link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png?v=1"><link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png?v=1"><link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png?v=1"><link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png?v=1"><link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png?v=1"><link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png?v=1"><link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png?v=1"><link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png?v=1"><link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png?v=1"><link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png?v=1"><link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=1"><link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png?v=1"><link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=1"><link rel="manifest" href="/manifest.json"><meta name="msapplication-TileColor" content="#FF9900"><meta name="msapplication-TileImage" content="/ms-icon-144x144.png?v=1"><meta name="theme-color" content="#FF9900"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="ie=edge"> <link rel="icon" href="<?=base_url('favicon.ico?v=1');?>" type="image/x-icon"> <?=isset($seo)?$seo:"<title>TS-Shape :: THAI SUMMIT SHAPE CORP CO.,LTD.</title>";?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" integrity="sha256-qM7QTJSlvtPSxVRjVWNM2OfTAz/3k5ovHOKmKXuYMO4=" crossorigin="anonymous"></script>
  <?php if(isset($css)): foreach($css as $c): ?>
    <link href="<?=base_url($c.'?v=1');?>" rel="stylesheet">
  <?php endforeach; endif;?>
  <script src="/js/jquery.js?v=1"></script>
  <script src="https://cdn.tiny.cloud/1/kgoa8i6nd8s0wjzwe134r8dl30eesiv60ve12otifac7s9ew/tinymce/5/tinymce.min.js"></script>
  <link href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/froala-editor@3.0.6/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@3.0.6/js/froala_editor.pkgd.min.js"></script>
  <script>

  var emailHeaderConfig = {
    selector: '.tinymce-heading,.title-h,.list-company h2,.text-normal-about,.pic-engi h4,.pic-engi h5',
    menubar: false,
    inline: true,
    plugins: [
      'lists',
      'autolink'
    ],
    toolbar: 'undo redo | bold italic underline',
    valid_elements: 'strong,em,span[style],a[href]',
    valid_styles: {
      '*': 'font-size,font-family,color,text-decoration,text-align'
    },
    powerpaste_word_import: 'clean',
    powerpaste_html_import: 'clean',
    // content_css: [
    //   '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
    // ]
  };
  var emailBodyConfig = {
    selector: '.tinymce-body,.text-in',
    menubar: false,
    inline: true,
    plugins: [
      'link',
      'lists',
      // 'powerpaste',
      'autolink',
      // 'tinymcespellchecker'
    ],
    toolbar: [
      'undo redo | bold italic underline | fontselect fontsizeselect',
      'forecolor backcolor | alignleft aligncenter alignright alignfull | numlist bullist outdent indent'
    ],
    valid_elements: 'p[style],strong,em,span[style],a[href],ul,ol,li',
    valid_styles: {
      '*': 'font-size,font-family,color,text-decoration,text-align'
    },
    powerpaste_word_import: 'clean',
    powerpaste_html_import: 'clean',
    // content_css: [
    //   '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
    // ]
  };

  var emailImgConfig = {
    selector: '.tinymce-img',
    menubar: true,
    inline: false,
    plugins: [
      'image code'
      // 'tinymcespellchecker'
    ],
    toolbar: [
      'image code'
    ],
    images_upload_url: 'postAcceptor.php',

    /* we override default upload handler to simulate successful upload*/
    images_upload_handler: function (blobInfo, success, failure) {
      setTimeout(function () {
        /* no matter what you upload, we will turn it into TinyMCE logo :)*/
        success('http://moxiecode.cachefly.net/tinymce/v9/images/logo.png');
      }, 2000);
    }
  };

  tinymce.init(emailHeaderConfig);
  tinymce.init(emailBodyConfig);
  tinymce.init(emailImgConfig);
  </script>
  <style>
  body,html{
    position: inherit !important;
    width:auto;
    height:auto;
  }
  .fr-popup.fr-active {
    z-index:99999 !important;
  }
  .fix-control{
    position:fixed;
    width:100%;
    padding:0 0 0;
    background:rgba(34, 44, 44, 1);
    z-index:999;
    left:0;
    top:0;
  }
  .back-web{
    padding:5px 10px;
    font-size: 12px;
    background-color:#424242;
  }
  .spinner {
    width: 70px;
    text-align: center;
    position:absolute;
    left:50%;
    top:15%;
    margin-top:-30px;
    margin-left:-35px;

  }

  .spinner > div {
    width: 18px;
    height: 18px;
    background-color: #fff;

    border-radius: 100%;
    display: inline-block;
    -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
    animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  }

  .spinner .bounce1 {
    -webkit-animation-delay: -0.32s;
    animation-delay: -0.32s;
  }

  .spinner .bounce2 {
    -webkit-animation-delay: -0.16s;
    animation-delay: -0.16s;
  }

  @-webkit-keyframes sk-bouncedelay {
    0%, 80%, 100% { -webkit-transform: scale(0) }
    40% { -webkit-transform: scale(1.0) }
  }

  @keyframes sk-bouncedelay {
    0%, 80%, 100% {
      -webkit-transform: scale(0);
      transform: scale(0);
      } 40% {
        -webkit-transform: scale(1.0);
        transform: scale(1.0);
      }
    }

    #loading{
      position:fixed;
      width:100%;
      height:100%;
      background-color: rgba(160, 92, 56, 0.7);
      background: rgba(160, 92, 56, 0.7);
      color: rgba(160, 92, 56, 0.7);
      z-index: 1000;
      left:0;top:0;
      display:none;
    }

    </style>
  </head>
  <body>
    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>

    </div>
    <?php
    $g = $this->db->get('roll_parts')->row_array();
    $gal = $this->db->order_by('priority','asc')->get('gallery_roll')->result_array();



    $lg=$this->uri->segment(1);
    $mode= $this->uri->segment(4);

    if($mode==false){
      $backweb = 'style="display:none;"';
      $hideElement ='';
    }else{
      $backweb = '';
      $hideElement ='style="display:none;"';
    }

    // echo $_SERVER[REQUEST_URI];
    ?>
    <div class="fix-control">
      <div class="back-web" <?=$backweb;?>><a href="<?=site_url('backend/about');?>"><< Back </a></div>
      <div class="padding-top-1 padding-left-1" style="display:block;">
        <button class="button isave"><i class="far fa-save"></i> Save</button>
        <a class="button success" <?=$hideElement;?> target="_blank" href="<?=site_url('iframe_web/roll_parts/full');?>"><i class="far fa-edit"></i> Full Page Edit</a>
        <div class="float-right po-relative padding-right-1">
          <a href="/<?=$this->lang->switch_uri('th');?>"><img  style="<?=$lg=="en"?"opacity:0.5":"";?>" src="<?=base_url('img/th.png?v=11');?>" width="30" alt="th"></a>
          |
          <a href="/<?=$this->lang->switch_uri('en');?>"><img style="<?=$lg=="th"?"opacity:0.5":"";?>"  src="<?=base_url('img/en.png?v=11');?>" width="30" alt="en"></a>
        </div>
      </div>
    </div>
    <div class="" style="min-height:100px;"></div>

    <section class="roll about-1 padding-bottom-2">
      <div id="roll_parts_<?=$this->uri->segment(1);?>">
       <?=$g['roll_parts_'.$this->uri->segment(1)];?>
      </div>
    </section>

    <div class="grid-container">
      <div class="grid-x grid-padding-x margin-bottom-3">
        <div class="padding-top-2 padding-bottom-1" style="display:block;width:100%;border:1px #ccc dashed;">
          <div class="cell small-12 text-center">
            <a  class="button secondary create-tab" data-open="modalTab"> <i class="far fa-list-alt"></i> Create tab</a>
          </div>
        </div>
      </div>
    </div>



    <div class="large reveal" id="modalTab" data-reveal>
      <form id="formTab" enctype="multipart/form-data" data-abide novalidate>
        <div class="grid-x ">
          <div class="cell small-12 padding-left-3">
            <label>Title
              <input type="text" name="title-tab"  id="titleTab" value="FRONT BUMPER" required>
            </label>
          </div>
          <div class="cell medium-5 padding-left-3 padding-bottom-3 padding-top-2" >
            <div class="po-relative">
              <div id="froala-editor-img" style="min-height:400px;">
                <img src="/img/no-image.png" width="100%">
              </div>
            </div>
          </div>

          <div class="cell medium-7 padding-top-2 padding-left-2">
            <div id="froala-editor" style="min-height:400px;">
              <h5 class="margin-bottom-2">Front Bumper</h5>
            <div>  Nec feugiat in fermentum posuere urna nec. Habitant morbi tristique senectus
              et netus et malesuada fames. Nunc faucibus a pellentesque sit amet porttitor eget. Ut lectus arcu bibendum at varius vel pharetra vel. In vitae turpis massa sed elementum tempus.</div>
            </div>
          </div>
        </div>
        <div class="grid-x">
          <input type="hidden" class="mode" value="add" data-idcon="" data-title="">
          <div class="cell small-12 text-center">
            <button class="button dark" type="submit" id="submitTab" data-id="">
              <i class="fas fa-plus"></i> Create Tab
            </button>
            <button class="button alert" data-close aria-label="Close modal" type="button">
              Cancel
            </button>
          </div>
        </div>
      </form>
    </div>


    <script>
    var ieditor =  new FroalaEditor('div#froala-editor', {
      attribution: false,
      toolbarBottom: true,
      fontFamily: {
        "Poppins":'Poppins',
        "Roboto,sans-serif": 'Roboto',
        "Oswald,sans-serif": 'Oswald',
        "Montserrat,sans-serif": 'Montserrat',
        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
      },
      // fontFamilySelection: true
    })

    FroalaEditor.DefineIcon('imageInfo', {NAME: 'info', SVG_KEY: 'help'});
    FroalaEditor.RegisterCommand('imageInfo', {
      title: 'Info',
      focus: false,
      undo: false,
      refreshAfterCallback: false,
      callback: function () {
        var $img = this.image.get();
        alert($img.attr('src'));
      }
    });

    var ieditor1  =  new FroalaEditor('div#froala-editor-img', {
      attribution: false,
      toolbarBottom: true,
      imageEditButtons: ['imageReplace', 'imageAlign', 'imageRemove', '|', 'imageLink', 'linkOpen', 'linkEdit', 'linkRemove', '-', 'imageDisplay', 'imageStyle', 'imageAlt', 'imageSize'],
      imageStyles: {
        thumbnail: 'thumbnail',

      },
      imagePasteProcess: true,
      imageUploadURL: '<?=site_url('iframe_web/upload_image');?>',

      imageUploadParams: {
        id: 'my_editor'
      }
    })

  </script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

  <script src="/js/foundation.min.js?v=1"></script>

  <script>


  $(document).foundation();

  $(document).on("submit", function(ev) {
    ev.preventDefault();

    if(ev.target.id=="formTab"){

      var mode = $('.mode');
      var itab;
      var itabContent;


      var htm = $('#froala-editor-img .fr-element').html();
      var htm1 = $('#froala-editor .fr-element').html();
      var titleTab  = $('#titleTab').val();

      if(mode.val()=="edit"){
        var idcon = $('.mode').attr('data-idcon');
        var title = $('.mode').attr('data-title');
        $('#'+idcon).find('.i-image').html(htm);
        $('#'+idcon).find('.i-content').html(htm1);
        $('#'+title).find('a').html(titleTab);
        $('#modalTab').foundation('close');

      }else{
        $('.tabs-title,.tabs-panel').removeClass('is-active')
        $('.tabs-title a').attr('aria-selected',false)
        $('.tabs-title a').attr('tabindex','-1')
        var numTab = $('#example-tabs li').length + 1;



        itab = '<li class="tabs-title is-active" id="t-'+numTab+'" role="presentation"><a href="#panel'+numTab+'v" role="tab" aria-controls="panel'+numTab+'v" aria-selected="true" id="panel'+numTab+'v-label" tabindex="0">'+titleTab+'</a></li>';
        $('#example-tabs').append(itab);


        itabContent = '<div class="tabs-panel is-active" id="panel'+numTab+'v" role="tabpanel" aria-labelledby="panel'+numTab+'v-label">';
        itabContent += '   <div class="grid-x">';
        itabContent += '   <div class="cell small-12 e-tab text-right text-right margin-bottom-1"><button class="button dark edit-tab" data-idcon="panel'+numTab+'v" data-title="t-'+numTab+'"><i class="far fa-edit"></i> Edit</button><button class="button alert delete-tab" data-idcon="panel'+numTab+'v" data-title="t-'+numTab+'"><i class="far fa-trash-alt"></i> Delete</button></div>';
        itabContent += '   <div class="cell small-12 medium-5 text-center padding-bottom-2 i-image">';
        itabContent += htm;
        itabContent += '  </div>';
        itabContent += '   <div class="cell small-12 medium-1"></div>';
        itabContent += '  <div class="cell small-12 medium-6 i-content">'+htm1+'</div>';
        itabContent += '  </div>';
        itabContent += '  </div>';
        $('.tabs-content').append(itabContent);
        $('#modalTab').foundation('close');


      }


    }







  });


  $(document)
  .on("closed.zf.reveal", function(ev,elem) {
    // $('#idPic').val();
    // $('.show-img').html('<img src="<?=base_url('img/no-image.png?v=1');?>">');
    // $('#submitUpload').hide();
    // $('#modal').removeClass('load-success');
    //
    //
    // $('#modalFile').removeClass('load-success');
    // $('#uploadFile')[0].reset();
    // $('#output').html('');

    // $('#thumbImg').attr("src"," ");

    ieditor.html.set(' ');
    ieditor1.html.set(' ');
    $('#titleTab').val();
  })
  .on("open.zf.reveal", function(ev,elem) {

    if(ev.target.id=="formTab"){
    // ieditor.html.set('<h5>Title </h5><div class="">custom text...</div>');
    // ieditor1.html.set('<img class="thumbnail" src="/img/reer.jpg">');
    // // $('#titleTab').attr('value','insert title');
  }

  })


$(document).on('click','.create-tab',function(){
$('#submitTab').html('<i class="fas fa-plus"></i> Create Tab');
$('#titleTab').val('Insert...')
$('.mode').val('add')
ieditor.html.set('<h5 class="margin-bottom-2">Title </h5><div class="">custom text...</div>');
ieditor1.html.set('<img class="thumbnail" src="/img/reer.jpg">');
})

$(document).on('click','.delete-tab',function(){
var numTab = $('#example-tabs li').length;
var idcon = $(this).data('idcon');
var title = $(this).data('title');

$('#'+title).remove();
$('#'+idcon).remove();


if(numTab==2){
$('#t-1').addClass('is-active');
$('#t-1 a').attr('aria-selected',"true");
$('#panel1v').addClass('is-active');
}
if(numTab>2){
var dTab = numTab -1;
var ilast,dlast;

ilast = $('.tabs-title').last();
dlast = $('.tabs-panel').last();

ilast.addClass('is-active');
ilast.find('a').attr('aria-selected',"true");
dlast.addClass('is-active');
dlast.removeAttr('aria-hidden');
dlast.siblings().attr('aria-hidden','true');
}



// console.log(idcon+' '+title)
})


$(document).on('click','.edit-tab',function(){

$('#submitTab').html('UPDATE');
var mode = $('.mode');
var imodal = $('#modalTab');
var idcon = $(this).attr('data-idcon');
var title = $(this).attr('data-title');
imodal.foundation('open');

mode.val('edit');
mode.attr('data-idcon',idcon);
mode.attr('data-title',title);


// $('#titleTab').val(title.find('a').html())
// idcon.find('.i-image').html();
// idcon.find('.i-content').html();
$('#titleTab').val($('#'+title).find('a').html());
mode.val('edit');

ieditor.html.set($('#'+mode.attr('data-idcon')).find('.i-content').html());
ieditor1.html.set($('#'+mode.attr('data-idcon')).find('.i-image').html());


// console.log(idcon+' '+title)
})

// title.find('a').html();


// ieditor.html.set('<h5>Title </h5><div class="">custom text...</div>');
// ieditor1.html.set('<img class="thumbnail" src="/img/reer.jpg">');


  $('.isave').on('click',function(){

    $('#loading').fadeIn();
    var values = {
      roll_parts_<?=$this->uri->segment(1);?>:$('#roll_parts_'+'<?=$this->uri->segment(1);?>').html(),


    };
    $.ajax({
      url: "<?=site_url('backend/ajaxAddRollParts');?>",
      type: "post",
      data: values ,
      success: function (response) {

        // You will get response from your PHP page (what you echo or print)
        $('#loading').delay(1000).fadeOut(300,function(){
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            html: '<small>Your work has been saved</small>',
            showConfirmButton: false,
            timer: 1500
          })
        })
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });
  })



</script>



</body>
</html>
