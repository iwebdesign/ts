<!DOCTYPE html><html lang="<?=$this->uri->segment(1);?>"><head><meta charset="UTF-8"><link href="/css/app.css?v=<?=rand();?>" rel="stylesheet"><link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png?v=1"><link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png?v=1"><link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png?v=1"><link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png?v=1"><link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png?v=1"><link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png?v=1"><link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png?v=1"><link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png?v=1"><link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png?v=1"><link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png?v=1"><link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=1"><link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png?v=1"><link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=1"><link rel="manifest" href="/manifest.json"><meta name="msapplication-TileColor" content="#FF9900"><meta name="msapplication-TileImage" content="/ms-icon-144x144.png?v=1"><meta name="theme-color" content="#FF9900"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="ie=edge"> <link rel="icon" href="<?=base_url('favicon.ico?v=1');?>" type="image/x-icon"> <?=isset($seo)?$seo:"<title>TS-Shape :: THAI SUMMIT SHAPE CORP CO.,LTD.</title>";?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" integrity="sha256-qM7QTJSlvtPSxVRjVWNM2OfTAz/3k5ovHOKmKXuYMO4=" crossorigin="anonymous"></script>
  <?php if(isset($css)): foreach($css as $c): ?>
    <link href="<?=base_url($c.'?v=1');?>" rel="stylesheet">
  <?php endforeach; endif;?>
  <script src="/js/jquery.js?v=1"></script>
  <script src="https://cdn.tiny.cloud/1/kgoa8i6nd8s0wjzwe134r8dl30eesiv60ve12otifac7s9ew/tinymce/5/tinymce.min.js"></script>
  <link href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/froala-editor@3.0.6/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@3.0.6/js/froala_editor.pkgd.min.js"></script>
  <script>

  tinymce.init({
    selector: '#local-upload',
    inline: true,
    plugins: 'image code',
    toolbar: 'undo redo | image code',

    /* without images_upload_url set, Upload tab won't show up*/
    // images_upload_url: '<?=site_url('iframe_web/upload_image_tiny');?>',

    /* we override default upload handler to simulate successful upload*/
    images_upload_handler: function (blobInfo, success, failure) {
    	var xhr, formData;
    	xhr = new XMLHttpRequest();
    	xhr.withCredentials = false;
    	xhr.open('POST', '<?=site_url('iframe_web/upload_image_tiny');?>');
    	xhr.onload = function() {
    	  var json;

    	  if (xhr.status != 200) {
    		failure('HTTP Error: ' + xhr.status);
    		return;
    	  }
    	  json = JSON.parse(xhr.responseText);

    	  if (!json || typeof json.location != 'string') {
    		failure('Invalid JSON: ' + xhr.responseText);
    		return;
    	  }
    	  success(json.location);
    	};
    	formData = new FormData();
      if( typeof(blobInfo.blob().name) !== undefined )
      fileName = blobInfo.blob().name;
      else
          fileName = blobInfo.filename();
      formData.append('file', blobInfo.blob(), fileName);

    	xhr.send(formData);
      }
  });





  var emailHeaderConfig = {
    selector: '.tinymce-heading,.title-h,.list-company h2,.text-normal-about,.pic-engi h4,.pic-engi h5',
    menubar: false,
    inline: true,
    plugins: [
      'lists',
      'autolink'
    ],
    toolbar: 'undo redo | bold italic underline',
    valid_elements: 'strong,em,span[style],a[href]',
    valid_styles: {
      '*': 'font-size,font-family,color,text-decoration,text-align'
    },
    powerpaste_word_import: 'clean',
    powerpaste_html_import: 'clean',
    // content_css: [
    //   '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
    // ]
  };
  var emailBodyConfig = {
    selector: '.tinymce-body,.text-in',
    menubar: false,
    inline: true,
    plugins: [
      'link',
      'lists',
      // 'powerpaste',
      'autolink',
      // 'tinymcespellchecker'
    ],
    toolbar: [
      'undo redo | bold italic underline | fontselect fontsizeselect',
      'forecolor backcolor | alignleft aligncenter alignright alignfull | numlist bullist outdent indent'
    ],
    valid_elements: 'p[style],strong,em,span[style],a[href],ul,ol,li',
    valid_styles: {
      '*': 'font-size,font-family,color,text-decoration,text-align'
    },
    powerpaste_word_import: 'clean',
    powerpaste_html_import: 'clean',
    // content_css: [
    //   '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
    // ]
  };

  var emailImgConfig = {
    selector: '.tinymce-img',
    menubar: true,
    inline: false,
    plugins: [
      'image code'
      // 'tinymcespellchecker'
    ],
    toolbar: [
      'image code'
    ],
    images_upload_url: 'postAcceptor.php',

    /* we override default upload handler to simulate successful upload*/
    images_upload_handler: function (blobInfo, success, failure) {
      setTimeout(function () {
        /* no matter what you upload, we will turn it into TinyMCE logo :)*/
        success('http://moxiecode.cachefly.net/tinymce/v9/images/logo.png');
      }, 2000);
    }
  };

  tinymce.init(emailHeaderConfig);
  tinymce.init(emailBodyConfig);
  tinymce.init(emailImgConfig);
  </script>
  <style>
  body,html{
    position: inherit !important;
    width:auto;
    height:auto;
  }
  .fix-control{
    position:fixed;
    width:100%;
    padding:0 0 0;
    background:rgba(34, 44, 44, 1);
    z-index:999;
    left:0;
    top:0;
  }
  .back-web{
    padding:5px 10px;
    font-size: 12px;
    background-color:#424242;
  }
  .spinner {
    width: 70px;
    text-align: center;
    position:absolute;
    left:50%;
    top:15%;
    margin-top:-30px;
    margin-left:-35px;

  }

  .spinner > div {
    width: 18px;
    height: 18px;
    background-color: #fff;

    border-radius: 100%;
    display: inline-block;
    -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
    animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  }

  .spinner .bounce1 {
    -webkit-animation-delay: -0.32s;
    animation-delay: -0.32s;
  }

  .spinner .bounce2 {
    -webkit-animation-delay: -0.16s;
    animation-delay: -0.16s;
  }

  @-webkit-keyframes sk-bouncedelay {
    0%, 80%, 100% { -webkit-transform: scale(0) }
    40% { -webkit-transform: scale(1.0) }
  }

  @keyframes sk-bouncedelay {
    0%, 80%, 100% {
      -webkit-transform: scale(0);
      transform: scale(0);
      } 40% {
        -webkit-transform: scale(1.0);
        transform: scale(1.0);
      }
    }

    #loading{
      position:fixed;
      width:100%;
      height:100%;
      background-color: rgba(160, 92, 56, 0.7);
      background: rgba(160, 92, 56, 0.7);
      color: rgba(160, 92, 56, 0.7);
      z-index: 1000;
      left:0;top:0;
      display:none;
    }

    </style>
  </head>
  <body>
    <div id="loading">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>

    </div>
    <?php
    $g = $this->db->get('safety')->row_array();
    $lg=$this->uri->segment(1);
    $mode= $this->uri->segment(4);

    if($mode==false){
      $backweb = 'style="display:none;"';
      $hideElement ='';
    }else{
      $backweb = '';
      $hideElement ='style="display:none;"';
    }

    // echo $_SERVER[REQUEST_URI];
    ?>
    <div class="fix-control">
      <div class="back-web" <?=$backweb;?>><a href="<?=site_url('backend/suppliers');?>"><< Back </a></div>
      <div class="padding-top-1 padding-left-1" style="display:block;">
        <button class="button isave"><i class="far fa-save"></i> Save</button>
        <a class="button success" <?=$hideElement;?> target="_blank" href="<?=site_url('iframe_web/safety/full');?>"><i class="far fa-edit"></i> Full Page Edit</a>
        <div class="float-right po-relative padding-right-1">
          <a href="/<?=$this->lang->switch_uri('th');?>"><img  style="<?=$lg=="en"?"opacity:0.5":"";?>" src="<?=base_url('img/th.png?v=11');?>" width="30" alt="th"></a>
          |
          <a href="/<?=$this->lang->switch_uri('en');?>"><img style="<?=$lg=="th"?"opacity:0.5":"";?>"  src="<?=base_url('img/en.png?v=11');?>" width="30" alt="en"></a>
        </div>
      </div>
    </div>
    <div class="" style="min-height:100px;"></div>
    <section class="about-1">

      <div id="safety_<?=$this->uri->segment(1);?>">

        <?=$g['safety_'.$this->uri->segment(1)];?>
      </div>

  </section>

  <div class="reveal custom-modal edit-iframe-pic" id="modal" data-reveal>
    <form id="uploadImg" enctype="multipart/form-data">
      <div class="icontent-modal">
        <div class="grid-x align-middle">
          <div class="cell small-12 medium-7">
            <div class="show-img text-center margin-bottom-2"><img src="<?=base_url('img/no-image.png?v=1');?>"></div>
          </div>
          <div class="cell small-12 medium-5 padding-left-1">
            <label for="imgInp" class="button secondary">Select File</label>
            <input type="file" name="image" class="show-for-sr" id="imgInp">
          </div>
        </div>
        <input type="hidden"  id="idPic" data-class="" value="">

      </div>
      <div class=""></div>
      <hr/>
      <div class="grid-x">
        <div class="cell small-12 text-center">
          <button class="button" type="submit" id="submitUpload">
            <i class="fas fa-cloud-upload-alt"></i> Upload
          </button>
          <button class="button alert" data-close aria-label="Close modal" type="button">
            Cancel
          </button>
        </div>
      </div>
    </form>
  </div>

  <div class="reveal custom-modal edit-iframe-pic" id="modalFile" data-reveal>
    <form id="uploadFile" enctype="multipart/form-data" data-abide novalidate>
      <div class="icontent-modal">
        <div class="grid-x align-middle">
          <div class="medium-7 cell">
            <label>Name File
              <input type="text" name="namefile" placeholder="" required>
            </label>
          </div>
          <div class="medium-5 cell padding-left-1">
            <small id="output" ></small>
          </div>
          <div class="cell small-12">
            <label>
              <input type="file" name="file" id="iFile"  required>
              <span class="form-error">
                Please select file.
              </span>
            </label>
          </div>
          <div class="cell small-12">
            <small>*.png,*.jpg,*.zip,*.pdf</small>
          </div>
        </div>
        <input type="hidden"  id="idFile" data-class="" value="">
      </div>
      <div class=""></div>
      <hr/>
      <div class="grid-x">
        <div class="cell small-12 text-center">
          <button class="button" type="submit" id="submitUploadFile" data-box="" data-id="">
            <i class="fas fa-cloud-upload-alt"></i> Upload File
          </button>
          <button class="button alert" data-close aria-label="Close modal" type="button">
            Cancel
          </button>
        </div>
      </div>
    </form>
  </div>


  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

  <script src="/js/foundation.min.js?v=1"></script>
  <!-- {elapsed_time} -->
  <script>


  $(document).ready(function() {

     $('.modal-pic1').find('a').removeAttr('data-fancybox')
     $('.modal-pic1').find('a').removeAttr('href')
     $('.icourosel').find('a').removeAttr('data-fancybox')
     $('.icourosel').find('a').removeAttr('href')
   })




  $(document).foundation();
  $(document).on("submit", function(ev) {
    ev.preventDefault();

    if(ev.target.id=="uploadImg"){
      $('#modal').addClass('load');
      var data = new FormData($('#uploadImg')[0]);
      $.ajax({
        url:'<?=site_url('iframe_web/ajaxUploadImage');?>',
        type:"post",
        data: data,
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success: function(data){
          var idata = JSON.parse(data);

          if(idata.status=="ok"){
            var iclass= $('#idPic').data('class');
            $('.'+iclass+'[data-id="'+$('#idPic').val()+'"]').find('img').attr('src',idata.path);
            setTimeout(function(){

              $('#modal').removeClass('load').addClass('load-success');
              setTimeout(function(){
                $('#modal').foundation('close')
              }, 1000);

            }, 1000);
          }else{
            $('#modal').removeClass('load');
            Swal.fire({
              position: 'center-center',
              icon: 'warning',
              html: '<small>You must insert .jpg,.png.gif </small>',
            }).then((result) => {
              $('.show-img').html('<img src="/img/no-image.png?v=1">');
              $('#submitUpload').hide();
            })
          }
        }
      });
    }


    if(ev.target.id=="uploadFile"){
      $('#modalFile').addClass('load');
      var data = new FormData($('#uploadFile')[0]);
      var dataId = $('#submitUploadFile').attr('data-id');
      $.ajax({
        url:'<?=site_url('iframe_web/ajaxUploadFile');?>',
        type:"post",
        data: data,
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success: function(data){
          var idata = JSON.parse(data);

          if(idata.status=="ok"){
            setTimeout(function(){

              $('#modalFile').removeClass('load').addClass('load-success');
              setTimeout(function(){
                $('#modalFile').foundation('close');
                var itype = idata.typefile;

                var ipath = idata.path;
                var idiv = '<div class="out-button"><a href="'+ipath+'" target="_blank" class="button-web grid-x align-middle "><span class="text-uppercase">'+itype.replace('.','')+'</span><div class="cell small-9 text"><div class="padding-left-1">'+idata.namefile+'</div> </div><div class="cell small-1 po-relative"><div class="border-center"></div></div><div class="cell small-2 text-center"><img src="/img/download.svg?v=1" width="32"></div></a></div>';
                //var idiv = '<div class="out-button">  <a  class="button-web grid-x align-middle " href="'+idata.path+'" target="_blank"> <div class="cell small-2 text-right"><img src="/img/icon-web.svg" width="35"></div>    <div class="cell small-1 po-relative"><div class="border-center"></div></div><div class="cell small-9 text">'+result.value+'</div>';
                //   idiv+= '</a></div>';

                var boxList =   $("."+$('#submitUploadFile').attr('data-box'));
                boxList.append(idiv);
                if($('.'+$('#submitUploadFile').attr('data-box')+' .out-button').length > 0){
                  $('#'+dataId).find('.removelink').show()
                }



              }, 1000);

            }, 1000);





          }else{
            $('#modalFile').removeClass('load');
            Swal.fire({
              position: 'center-center',
              icon: 'warning',
              html: '<small>You must insert .jpg,.png,.pdf,.zip </small>',
            }).then((result) => {
              $('#submitUploadFile').hide();
            })
          }


        }
      })


    }



  });


  $('.modal-pic1').on('click',function(){
    var $modal = $('#modal');
    $modal.foundation('open');
    $('#idPic').val($(this).data('id'));
    $('#idPic').attr('data-class',$(this).attr('class'));
    $('#submitUpload').hide();

  })

  $(document)
  .on("closed.zf.reveal", function(ev,elem) {
    $('#idPic').val();
    $('.show-img').html('<img src="<?=base_url('img/no-image.png?v=1');?>">');
    $('#submitUpload').hide();
    $('#modal').removeClass('load-success');


    $('#modalFile').removeClass('load-success');
    $('#uploadFile')[0].reset();
    $('#output').html('');
  })
  .on("open.zf.reveal", function(ev,elem) {

  })






  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        // $('#blah').attr('src', e.target.result);
        $('.show-img').html('<img src="'+e.target.result+'" style="max-height:270px;">')
        $('#submitUpload').fadeIn();
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function() {
    readURL(this);
  });


  $(".addlink").click(function(){
    Swal.fire({
      title: 'Insert link',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'confirm',
      showLoaderOnConfirm: true,
      preConfirm: (itext) => {

        if(itext==""){
          Swal.showValidationMessage(
            `Request failed`
          )

        }else{
          return itext;
        }

      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.value) {
        var idiv = '<div class="out-button">  <a  class="button-web grid-x align-middle " href="//'+result.value+'" target="_blank"> <div class="cell small-2 text-right"><img src="/img/icon-web.svg" width="35"></div>    <div class="cell small-1 po-relative"><div class="border-center"></div></div><div class="cell small-9 text">'+result.value+'</div>';
        idiv+= '</a></div>';

        var boxList =   $("."+$(this).data('box'));
        boxList.append(idiv);
      }
    })
  });

  $(document).on('click',".removelink",function(){
    var dataId = $(this).attr('data-id');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        ).then((result) => {
          var boxList =   $("."+$(this).attr('data-box'));
          boxList.html('');
          $(this).hide();
          $('#'+dataId).find('.removelink').hide()
        })
      }
    })
  })




  $(document).on('click','.addfile',function(){
    var $modal = $('#modalFile');
    $modal.foundation('open');
    $('#submitUploadFile').hide();
    var dataBox = $(this).attr('data-box');
    var dataId  = $(this).attr('data-id');
    $('#submitUploadFile').attr('data-box',dataBox);
    $('#submitUploadFile').attr('data-id',dataId);

  })


  $("#iFile").change(function() {
    $('#submitUploadFile').fadeIn();
  });

  $(document).on('click','.removediv',function(){
    var dataId = $(this).attr('data-id');
    Swal.fire({
      title: 'Are you sure delete list?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your list has been deleted.',
          'success'
        ).then((result)=>{
          $('#'+dataId).remove();
        })
      }
    })

    // alert(dataId)

  })


  $('.addlist').on('click',function(){
    var iaccord= $('#accord');
    var idList = makeid(6);

    var ilist = "";
    ilist += '<li class="accordion-item is-active '+idList+'-class" id="'+idList+'" data-accordion-item>';
    ilist += '<a href="#" class="accordion-title" aria-controls="'+idList+'-accordion" role="tab" id="'+idList+'-accordion-label" aria-expanded="true" aria-selected="true"><h6>Edit Title</h6></a>';
    ilist += '<div class="accordion-content" data-tab-content="" role="tabpanel" aria-hidden="false"  style="display:block;" id="'+idList+'-accordion" aria-labelledby="'+idList+'-accordion-label">';
    ilist += '   <div class="text-in-list">Edit text ....</div>';
    ilist += '<div class="list-out-'+idList+'"></div><div style="clear:both;"></div>';
    ilist += ' </div>';
    ilist += '<div class="padding-top-2 padding-bottom-1 box-module" style="display:block;width:100%;border:1px #ccc dashed;">';
    ilist += '  <div class="grid-x grid-padding-x">';
    ilist += '    <div class="cell small-12 text-center">';
    ilist += '      <button class="button secondary addfile" data-id="'+idList+'" data-box="list-out-'+idList+'"> <i class="far fa-file-alt"></i> Add File</button>';
    ilist += '      <button class="button alert removelink"  data-id="'+idList+'" style="display:none;" data-box="list-out-'+idList+'"> <i class="fas fa-plus-square"></i> Remove all File</button>';
    ilist += '      <button class="button alert removediv"   data-id="'+idList+'" data-box="list-out-'+idList+'"> <i class="far fa-trash-alt"></i> Delete this list </button>';
    ilist += '    </div>';
    ilist += '  </div>';
    ilist += '</div>';


    ilist += ' </li>';

    iaccord.append(ilist);

    var emailTextConfig = {
      selector: '.text-in-list, .accordion-title h6',
      menubar: false,
      inline: true,
      plugins: [
        'lists',
        'autolink'
      ],
      toolbar: 'undo redo | bold italic underline',
      valid_elements: 'strong,em,span[style],a[href]',
      valid_styles: {
        '*': 'font-size,font-family,color,text-decoration,text-align'
      },
    };


    tinymce.init(emailTextConfig);

  })

  $(document).on('click','.accordion-item',function(){
    $(this).addClass('is-active');
    $(this).find('.accordion-content').show();
    $(this).find('.box-module').show();
    $(this).siblings().removeClass('is-active').find('.accordion-content').hide();
    $(this).siblings().find('.box-module').hide();
  })


  function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }


  /**
  * Load the mime type based on the signature of the first bytes of the file
  * @param  {File}   file        A instance of File
  * @param  {Function} callback  Callback with the result
  * @author Victor www.vitim.us
  * @date   2017-03-23
  */
  function loadMime(file, callback) {

    //List of known mimes
    var mimes = [
      {
        mime: 'image/jpeg',
        pattern: [0xFF, 0xD8, 0xFF],
        mask: [0xFF, 0xFF, 0xFF],
      },
      {
        mime: 'image/png',
        pattern: [0x89, 0x50, 0x4E, 0x47],
        mask: [0xFF, 0xFF, 0xFF, 0xFF],
      }
      // you can expand this list @see https://mimesniff.spec.whatwg.org/#matching-an-image-type-pattern
    ];

    function check(bytes, mime) {
      for (var i = 0, l = mime.mask.length; i < l; ++i) {
        if ((bytes[i] & mime.mask[i]) - mime.pattern[i] !== 0) {
          return false;
        }
      }
      return true;
    }

    var blob = file.slice(0, 4); //read the first 4 bytes of the file

    var reader = new FileReader();
    reader.onloadend = function(e) {
      if (e.target.readyState === FileReader.DONE) {
        var bytes = new Uint8Array(e.target.result);

        for (var i=0, l = mimes.length; i<l; ++i) {
          if (check(bytes, mimes[i])) return callback("Mime: " + mimes[i].mime + " <br> Browser:" + file.type);
        }

        return callback("Mime: unknown <br> Browser:" + file.type);
      }
    };
    reader.readAsArrayBuffer(blob);
  }

  var fileInput = document.getElementById("iFile");
  var output  = document.getElementById("output");
  //when selecting a file on the input
  fileInput.onchange = function() {
    loadMime(fileInput.files[0], function(mime) {

      //print the output to the screen
      output.innerHTML = mime;
    });
  };

  // save web

  $('.isave').on('click',function(){

    $('#loading').fadeIn();
    var values = {
      safety_<?=$this->uri->segment(1);?>:$('#safety_'+'<?=$this->uri->segment(1);?>').html(),

    };
    $.ajax({
      url: "<?=site_url('backend/ajaxAddSafety');?>",
      type: "post",
      data: values ,
      success: function (response) {

        // You will get response from your PHP page (what you echo or print)
        $('#loading').delay(1000).fadeOut(300,function(){
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            html: '<small>Your work has been saved</small>',
            showConfirmButton: false,
            timer: 1500
          })
        })
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });
  })


  </script>

</body>
</html>
