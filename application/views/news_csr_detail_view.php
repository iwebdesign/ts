<?php
$this->db->where('id',$n['id']);
$this->db->set('user_view', 'user_view+1',false);
$this->db->update('news');

$headpic = "head_picture_news";
$h = $this->db->get($headpic)->row();
 ?>

<div class="slide other-page">
  <?php if($h->picture!=""): ?>
    <img src="<?=base_url('img/'.$h->picture.'?v=777');?>">
  <?php else: ?>
  <img src="<?=base_url('img/head-news.jpg?v=777');?>">
  <?php endif;?>
</div>

<section class="detail-news about-1">
  <div class="grid-container">
    <div class="grid-x grid-padding-x">
      <div class="cell small-12">
        <div class="head-pic <?=$n['picture']!=""?"padding-top-2 padding-bottom-2":"padding-bottom-1";?>">
          <?php if($n['picture']!=""): ?>
            <img src="<?=base_url('img/'.$n['picture'].'?v=1');?>" width="100%">
          <?php endif;?>
        </div>
      </div>
    </div>

    <div class="grid-x grid-padding-x padding-bottom-3">
      <div class="cell small-12">
        <h1><?=$n['title_'.$lg];?></h1>
        <div class="update-in">
          <font color="#009966"> <?=date("M d, Y", strtotime($n['datetime']));?></font> <font color="#999999">by <?=$n['post_by'];?> <?=$n['user_view'];?> view</font>
        </div>
      </div>

      <div class="cell small-12">
        <div class="text-normal padding-top-3">
          <?=$n['detail_'.$lg];?>
        </div>
      </div>

      <div class="cell small-12 text-center padding-bottom-3 padding-top-3">
        <a class="back text-uppercase"><< back</a>
      </div>


    </div>

  <?php

$relate = $this->db->order_by('id','random')->limit(3)->get_where('news',array('id !='=>$n['id'],'status !='=>'close'))->result_array();
  ?>

<?php if($relate!=false): ?>
    <div class="grid-x grid-padding-x">
      <div class="cell small-12 margin-bottom-1">
        <div class="list-company">
          <div class="po-relative border-gray">
            <h2>Related News</h2>
          </div>
        </div>

        <div class="grid-x grid-padding-x home-news" data-equalizer>

         <?php $i=1;foreach($relate as $r): $x=$i++;?>
          <div class="cell small-6 medium-4 <?=$x==3?"hide-for-small-only":"";?> inews">
            <a href="<?=site_url('news_csr/detail/'.$id.'/'.url_title($r['title_'.$lg]));?>" class="in" data-equalizer-watch>
              <div class="pic">
                <?php if($r['picture_thumbnail']!=""): ?>
                  <img src="<?=base_url('img/'.$r['picture_thumbnail'].'?v=1');?>" alt="">
                <?php else: ?>
                  <img src="<?=base_url('img/n'.$x.'.jpg?v=1');?>" alt="">
                <?php endif;?>

              </div>
              <h3><?=$r['title_'.$lg];?></h3>
              <div class="update-in">
                <font color="#009966"> <?=date("M d, Y", strtotime($r['datetime']));?></font> <font color="#999999">by <?=$r['post_by'];?> <?=$r['user_view'];?> view</font>
              </div>
              <div class="in-text">
                <?=$r['short_detail_'.$lg];?>

              </div>
              <div><div class="more"><a href="<?=site_url('news_csr/detail/'.$id.'/'.url_title($r['title_'.$lg]));?>"  title="News"><i class="fas fa-plus"></i></a></div></div>
            </a>
          </div>
        <?php endforeach;?>



        </div>



      </div>
    </div>
<?php endif;?>

  </div>
</section>
