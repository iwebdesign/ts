<?php
$headpic = "head_picture_news";
$h = $this->db->get($headpic)->row();
 ?>
<div class="slide other-page">
   <?php if($h->picture!=""): ?>
     <img src="<?=base_url('img/'.$h->picture.'?v=777');?>">
   <?php else: ?>
   <img src="<?=base_url('img/head-news.jpg?v=777');?>">
   <?php endif;?>
</div>

<section class="news about-1">
<div class="grid-container home-news" style="padding-top:0">
    <div class="grid-x grid-padding-x">
        <div class="cell small-12 margin-bottom-2">
            <h1 class="title-h">NEWS / CSR</h1>
        </div>
    </div>

    <div class="grid-x grid-padding-x" data-equalizer>

    <?php $i=1; foreach($nn as $n){

      $x = $i++;

      if($x>3){
        $i = 1;
        $x   = $i++;
      }

      ?>
        <div class="cell small-6 medium-4 inews">
            <a href="<?=site_url('news_csr/detail/'.$n['id'].'/'.url_title($n['title_en']));?>"  class="in" data-equalizer-watch>
                <div class="pic">
                   <?php if($n['picture_thumbnail']!=""): ?>
                     <img src="<?=base_url('img/'.$n['picture_thumbnail'].'?v=1');?>" alt="">
                   <?php else: ?>
                    <img src="<?=base_url('img/n'.$x.'.jpg?v=1');?>" alt="">
                   <?php endif;?>

                </div>
                <h3><?=$n['title_'.$lg];?></h3>
                <div class="update-in">
                <font color="#009966"><?=date("M d, Y", strtotime($n['datetime']));?></font> <font color="#999999">by <?=$n['post_by'];?> <?=$n['user_view'];?> view</font>
                </div>
                <div class="in-text">
                <?=$n['short_detail_'.$lg];?>
                </div>
                <div><div class="more"><a title="<?=$n['title_'.$lg];?>"><i class="fas fa-plus"></i></a></div></div>
                </a>
        </div>
      <?php } ?>



    </div>
    <div class="grid-x grid-padding-x margin-top-3" >
      <div class="cell small-12 text-center">
        <?=$this->pagination->create_links();?>
      </div>
    </div>

  </div>
</section>
