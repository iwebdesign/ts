<?php
$headpic = "head_picture_engineer";
$h = $this->db->get($headpic)->row();
$g = $this->db->get($guPage)->row_array();
$gal = $this->db->order_by('priority','asc')->get('gallery_engineer')->result_array();
 ?>

 <div class="slide other-page">
    <?php if($h->picture!=""): ?>
      <img src="<?=base_url('img/'.$h->picture.'?v=777');?>">
    <?php else: ?>
      <img src="<?=base_url('img/head-engineering.jpg?v=77');?>">
    <?php endif;?>
 </div>


<section class="roll about-1 padding-bottom-2">

  <div id="engineer_<?=$this->uri->segment(1);?>">

    <?=$g['engineer_'.$lg];?>

   </div>
</section>
<section class="pic-gallery margin-bottom-2 margin-top-2">
<div class="grid-x align-center align-middle list-gallery" id="gallery">
<?php if($gal==false): ?>


<?php else:?>
<?php foreach($gal as $r): ?>

<div class="cell small-6 medium-3 item">
<a href="/img/gallery/<?=$r['url'];?>" data-fancybox="gallery" data-caption="<?=$r['title'];?>">
  <img src="/img/gallery/<?=$r['url'];?>" width="100%">
</a>
</div>

<?php endforeach;?>
<?php endif;?>


</div>

</section>


<script>

$(document).ready(function() {

$('.box-module').remove();

  var i, isValidURL, isVValidURL, isEmbValidURL, uniqueIdLength, uniqueID;
  uniqueIdLength = 11;

  var idVideo;
  var video = $('.box-video');
  var ihref = video.find('a').attr('data-href');

  if (ihref.search("youtube") != -1) {
    isValidURL = ihref.indexOf("www.youtube.com/watch?v=");
    isVValidURL = ihref.indexOf("www.youtube.com/v/");
    isEmbValidURL = ihref.indexOf("www.youtube.com/embed/");

    if (isValidURL == -1 && isVValidURL == -1 && isEmbValidURL == -1) {

    }

    if (isValidURL != -1) {
      i = ihref.indexOf("v=");
    }
    else if (isVValidURL != -1) {
      i = ihref.indexOf("v/");
    }
    else if (isEmbValidURL != -1) {
      i = ihref.indexOf("embed/");
      i = i + 4;
    }
    i = i + 2;

    uniqueID = ihref.substr(i, uniqueIdLength);
    idVideo = 'https://www.youtube.com/watch?v='+uniqueID;
  }

  if ((ihref.search("vimeo") != -1)) {
    isVimeoURL = ihref.indexOf("vimeo.com/video");
    isvVimeoURL = ihref.indexOf("www.vimeo.com/video");
    if (isVimeoURL == -1 && isvVimeoURL == -1) {

    }

    if (isVimeoURL != -1) {
      i = ihref.indexOf("video/");
    }
    i = i + 6;

    uniqueID = ihref.substr(i, uniqueIdLength);
    idVideo = 'https://vimeo.com/'+uniqueID;

  }





// https://vimeo.com/191947042
  video.find('a').attr('data-fancybox','');
  video.find('a').attr('href',idVideo);
  video.find('a').removeAttr('data-href');
  // $('.box-video').find('a').attr('data-fancybox','');

  $('.ipdf').find('a').attr('data-fancybox','');
  $('.ipdf').find('a').attr('href','javascript:;');

});

function getIdFromVimeoURL(url) {
  return /(vimeo(pro)?\.com)\/(?:[^\d]+)?(\d+)\??(.*)?$/.exec(url)[3];
}





</script>
