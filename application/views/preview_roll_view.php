<?php
$headpic = "head_picture_roll";
$h = $this->db->get($headpic)->row();
$g = $this->db->get($guPage)->row_array();
$gal = $this->db->order_by('priority','asc')->get('gallery_roll')->result_array();
 ?>
 <div class="slide other-page">
    <?php if($h->picture!=""): ?>
      <img src="<?=base_url('img/'.$h->picture.'?v=777');?>">
    <?php else: ?>
      <img src="<?=base_url('img/head-roll.jpg?v=77');?>">
    <?php endif;?>
 </div>

 <section class="about-1">
   <div class="grid-container">
     <?=$g['roll_'.$lg];?>
   </div>
</section>
<section class="pic-gallery margin-bottom-2 margin-top-2">
<div class="grid-x align-center align-middle list-gallery" id="gallery">
<?php if($gal==false): ?>


<?php else:?>
<?php foreach($gal as $r): ?>

<div class="cell small-6 medium-3 item">
<a href="/img/gallery/<?=$r['url'];?>" data-fancybox="gallery" data-caption="<?=$r['title'];?>">
  <img src="/img/gallery/<?=$r['url'];?>" width="100%">
</a>
</div>

<?php endforeach;?>
<?php endif;?>


</div>

</section>
<script>

$(document).ready(function(){

$('.box-module').remove();
})

</script>
