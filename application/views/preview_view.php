<?php

$g = $this->db->get($guPage)->row_array();


 ?>
 <div class="slide other-page">
 <img src="<?=base_url('img/head-about.jpg?v=77');?>">
 </div>

 <section class="about-1">
   <div class="grid-container">
     <?=$g['company_profile_'.$lg];?>
     <?=$g['management_'.$lg];?>
     <?=$g['mission_vision_'.$lg];?>
     <?=$g['parent_companies_'.$lg];?>
     <?=$g['company_policy_'.$lg];?>
   </div>
</section>


<script type="text/javascript">
    $(document).ready(function(){
      $('.slide').slick({
          lazyLoad: 'ondemand'
      });
    });

    $(".modal-pic").on('click', function() {

     var img = $(this).find('img');
     var path= img.attr('src');
      $.fancybox.open([
        {
          src  : path,
          opts : {
            caption : '',
            // thumb   : path
          }
        }
      ], {
        thumbs : {
          autoStart : false
        }
      });

    });
  </script>
