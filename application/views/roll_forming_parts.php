<?php
$headpic = "head_picture_roll";
$h = $this->db->get($headpic)->row();
$g = $this->db->get('roll_parts')->row_array();
// $gal = $this->db->order_by('priority','asc')->get('gallery_roll')->result_array();
 ?>

  <div class="slide other-page">
     <?php if($h->picture!=""): ?>
       <img src="<?=base_url('img/'.$h->picture.'?v=777');?>">
     <?php else: ?>
       <img src="<?=base_url('img/head-roll.jpg?v=77');?>">
     <?php endif;?>
  </div>

<section class="roll about-1 padding-bottom-2">
     <?=$g['roll_parts_'.$lg];?>
</section>

<script>

$(document).ready(function(){

$('.box-module').remove();
$('.e-tab').remove();
})

</script>
