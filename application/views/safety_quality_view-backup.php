<div class="slide other-page">
  <img src="<?=base_url('img/head-engineering.jpg?v=77');?>">
</div>


<section class="roll about-1 padding-bottom-2">
  <div class="grid-container">
    <div class="grid-x grid-padding-x">
      <div class="cell small-12 margin-bottom-2">
        <h1 class="title-h">Safety Quality</h1>
      </div>
    </div>

    <div class="grid-x">
      <div class="cell small-12 medium-4">
        <div class="list-company">
          <div class="po-relative border-gray  margin-bottom-2">
            <h2 style="color:#999">DOWNLOAD.PDF</h2>
          </div>
        </div>

        <div class="out-button">
          <a href="#" class="button-web grid-x align-middle ">
            <span>PDF</span>
            <div class="cell small-9 text"><div class="padding-left-1">
              SAFETY QUALITY 1
            </div> </div>
            <div class="cell small-1 po-relative">
              <div class="border-center"></div>
            </div>
            <div class="cell small-2 text-center"><img src="<?=base_url('img/download.svg?v=1');?>" width="32"></div>
          </a>
        </div>

        <div class="out-button">
          <a href="#" class="button-web grid-x align-middle ">
            <span>PDF</span>
            <div class="cell small-9 text"><div class="padding-left-1">
              SAFETY QUALITY 2
            </div> </div>
            <div class="cell small-1 po-relative">
              <div class="border-center"></div>
            </div>
            <div class="cell small-2 text-center"><img src="<?=base_url('img/download.svg?v=1');?>" width="32"></div>
          </a>
        </div>

        <div class="out-button">
          <a href="#" class="button-web grid-x align-middle ">
            <span>PDF</span>
            <div class="cell small-9 text"><div class="padding-left-1">
              SAFETY QUALITY 3
            </div> </div>
            <div class="cell small-1 po-relative">
              <div class="border-center"></div>
            </div>
            <div class="cell small-2 text-center"><img src="<?=base_url('img/download.svg?v=1');?>" width="32"></div>
          </a>
        </div>

      </div>
      <div class="cell small-12 medium-1"></div>

      <div class="cell small-12 medium-7 padding-top-1">
        <a href="/img/safety-1.jpg" data-fancybox="images" data-caption="Safety">
          <img src="/img/safety-1.jpg" width="100%">
        </a>
      </div>
    </div>


    <div class="grid-x grid-padding-x">
      <div class="cell small-12 margin-bottom-1 margin-top-3">
        <div class="list-company">
          <div class="po-relative border-gray">
            <h2>Ts-shape Safety Policy</h2>
          </div>
          <div class="text-in">
            Globally positioned in-hou  ering services, allow TS-Shape to provide research and development, product design, optimization, CAE, and an array of physical testing options. This blend of services gets your project to the finish line on time, on spec and on budget.
            In fact, TS-Shape is trusted by many of the world’s largest automotive OEMs to tackle their toughest challenges.
            While cost is a critical consideration, it’s not the only focus at TS-Shape. Through the use of advanced high strength steel and a continuous push for inventive roll form engineering, TS-Shape is capable of helping you drive out mass and complexity. Plus increase component or full system performance.
            <br/><br/>
            No matter where you are in the development cycle, TS-Shape engineers can help you navigate the waters of today’s complex impact energy management requirements. Contact TS-Shape to learn how you can gain a competitive and technical advantage in the critical areas of your project.

          </div>
        </div>
      </div>
    </div>

    <div class="grid-x grid-padding-x">
      <div class="cell small-12 margin-bottom-1 margin-top-1">
        <div class="list-company">
          <div class="po-relative border-gray">
            <h2>TS-Shape Award</h2>
          </div>

          <div class="icourosel padding-bottom-3 padding-top-3" style="max-width:1000px;margin:auto;">
            <div class="grid-x grid-padding-x">

              <div class="cell auto">
                <a href="/img/award-1.jpg" data-fancybox="images" data-caption="Award 1">
                  <img src="/img/award-1.jpg">
                </a>
              </div>
              <div class="cell auto">
                <a href="/img/award-2.jpg" data-fancybox="images" data-caption="Award 2">
                  <img src="/img/award-2.jpg">
                </a>
              </div>
              <div class="cell auto">
                <a href="/img/award-3.jpg" data-fancybox="images" data-caption="Award 3">
                  <img src="/img/award-3.jpg">
                </a>
              </div>
              <div class="cell auto">
                <a href="/img/award-4.jpg" data-fancybox="images" data-caption="Award 4">
                  <img src="/img/award-4.jpg">
                </a>
              </div>
              <div class="cell auto">
                <a href="/img/award-5.jpg" data-fancybox="images" data-caption="Award 5">
                  <img src="/img/award-5.jpg">
                </a>
              </div>



            </div>


          </div>

          <div class="text-in">
            •  Senectus et netus et malesuada fames.<br/>
            •  Nisi porta lorem mollis aliquam ut porttitor leo. Malesuada fames ac turpis egestas maecenas pharetra. Nunc faucibus a pellentesque sit. <br/>
            Egestas tellus rutrum tellus pellentesque eu. Orci porta non pulvinar neque.<br/>
            •  Pharetra massa massa ultricies mi. Sagittis vitae et leo duis ut. Orci a scelerisque purus semper eget. Rhoncus dolor purus non enim. <br/>
            •  Eu turpis egestas pretium aenean pharetra magna ac placerat. Augue eget arcu dictum varius duis at consectetur lorem donec.<br/>
            •  Non pulvinar neque laoreet suspendisse interdum consectetur. Pretium aenean pharetra magna ac placerat vestibulum lectus.
            Etiam sit amet nisl purus in mollis nunc sed. Commodo ullamcorper a lacus vestibulum sed arcu non. Commodo sed egestas egestas -
            fringilla. Tempus iaculis urna id volutpat lacus laoreet non curabitur.<br/>
            •  Virtual and physical testing Gravida in fermentum et sollicitudin. Adipiscing diam donec adipiscing tristique risus. Et netus et malesuada -
            fames. Mauris a diam maecenas sed enim. Et pharetra pharetra massa massa ultricies.<br/>
          </div>
        </div>
      </div>
    </div>


  </div>
</section>
