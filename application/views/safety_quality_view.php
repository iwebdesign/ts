<?php
$headpic = "head_picture_engineer";
$h = $this->db->get($headpic)->row();
$g = $this->db->get('safety')->row_array();

 ?>

 <div class="slide other-page">
    <?php if($h->picture!=""): ?>
      <img src="<?=base_url('img/'.$h->picture.'?v=777');?>">
    <?php else: ?>
      <img src="<?=base_url('img/head-engineering.jpg?v=77');?>">
    <?php endif;?>
 </div>

 <section class="roll about-1 padding-bottom-2">

 <?=$g['safety_'.$lg];?>

</section>


<script>
$('.box-module,.fr-dropdown-menu,.fr-more-toolbar,.fr-toolbar,.fr-sticky-dummy').remove();

 $(".modal-pic1").on('click', function() {

  var img = $(this).find('img');
  var path= img.attr('src');
   $.fancybox.open([
     {
       src  : path,
       opts : {
         caption : '',
         // thumb   : path
       }
     }
   ], {
     thumbs : {
       autoStart : false
     }
   });

 });


  $(".icourosel a").on('click', function() {

   var img = $(this).find('img');
   var path= img.attr('src');
    $.fancybox.open([
      {
        src  : path,
        opts : {
          caption : '',
          // thumb   : path
        }
      }
    ], {
      thumbs : {
        autoStart : false
      }
    });

  });

</script>
