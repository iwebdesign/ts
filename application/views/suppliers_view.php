<?php
$headpic = "head_picture_about";
$h = $this->db->get($headpic)->row();
$g = $this->db->get('suppliers')->row_array();
 ?>
<div class="slide other-page">
   <?php if($h->picture!=""): ?>
     <img src="<?=base_url('img/'.$h->picture.'?v=777');?>">
   <?php else: ?>
   <img src="<?=base_url('img/head-about.jpg?v=77');?>">
   <?php endif;?>
</div>

<section class="suppliers-1 about-1">
     <?=$g['suppliers_'.$lg];?>
</section>



<script type="text/javascript">
    $(document).ready(function(){
      $('.slide').slick({
          lazyLoad: 'ondemand'
      });
      $('.box-module').remove();
    });
  </script>
