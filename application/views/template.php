<!DOCTYPE html><html lang="<?=$this->uri->segment(1);?>"><head><meta charset="UTF-8"><link href="/css/app.css?v=<?=rand();?>" rel="stylesheet"><link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png?v=1"><link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png?v=1"><link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png?v=1"><link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png?v=1"><link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png?v=1"><link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png?v=1"><link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png?v=1"><link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png?v=1"><link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png?v=1"><link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png?v=1"><link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=1"><link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png?v=1"><link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=1"><link rel="manifest" href="/manifest.json"><meta name="msapplication-TileColor" content="#FF9900"><meta name="msapplication-TileImage" content="/ms-icon-144x144.png?v=1"><meta name="theme-color" content="#FF9900"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="ie=edge"> <link rel="icon" href="<?=base_url('favicon.ico?v=777');?>" type="image/x-icon"> <?=isset($seo)?$seo:"<title>TS-Shape :: THAI SUMMIT SHAPE CORP CO.,LTD.</title>";?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" integrity="sha256-qM7QTJSlvtPSxVRjVWNM2OfTAz/3k5ovHOKmKXuYMO4=" crossorigin="anonymous"></script>
 <?php if(isset($css)): foreach($css as $c): ?>
<link href="<?=base_url($c.'?v=1');?>" rel="stylesheet">
<?php endforeach; endif;?>

<?php if($page=="contact"){ ?>
  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<?php }else{ ?>
<script src="/js/jquery.js?v=1"></script>
<link href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<?php } ?>
 <?php if(isset($script)): foreach($script as $s): ?>
<script src="<?=base_url($s.'?v=1');?>"></script>
<?php endforeach; endif;?>

<?php if($this->setting_model->get()['google_analytics']!=""):

echo $this->setting_model->get()['google_analytics'];

endif; ?>

</head>
<body>
<?php
$this->load->view('header');
?>
<section class="main">
<?php
$this->load->view($content);
?>
</section>
<?php
$this->load->view('footer');
?>
<div class="imodal">
      <div class="imodal-content">
            <ul class="list-lang">
                  <li class="<?=$lg=="th"?"active":"";?>">
                        <a href="/<?=$this->lang->switch_uri('th');?>">
                              <div class="pic">
                              <img class="centered-xy" src="<?=base_url('img/th.png?v=11');?>" width="35" alt="th">
                              </div>
                              <div class="language">
                               <div class="centered-xy">
                                ภาษาไทย
                               </div>
                              </div>
                         </a>
                  </li>
                  <li class="<?=$lg=="en"?"active":"";?>">
                        <a href="/<?=$this->lang->switch_uri('en');?>">
                              <div class="pic">
                              <img class="centered-xy" src="<?=base_url('img/en.png?v=11');?>" width="35" alt="en">
                              </div>
                              <div class="language">
                              <div class="centered-xy">
                               English
                              </div>
                              </div>
                         </a>
                  </li>
           </ul>
      </div>
</div>


<script src="/js/foundation.min.js?v=1"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<!-- {elapsed_time} -->
<script>
$(document).foundation();
$(document)
  // field element is invalid
  .on("invalid.zf.abide", function(ev,elem) {
    console.log("Field id "+ev.target.id+" is invalid");
  })
  // field element is valid
  .on("valid.zf.abide", function(ev,elem) {
    console.log("Field name "+elem.attr('name')+" is valid");
  })
  // form validation failed
  .on("forminvalid.zf.abide", function(ev,frm) {
    console.log("Form id "+ev.target.id+" is invalid");
  })
  // form validation passed, form will submit if submit event not returned false
  .on("formvalid.zf.abide", function(ev,frm) {
    console.log("Form id "+frm.attr('id')+" is valid");
    // ajax post form
  })
  // to prevent form from submitting upon successful validation
  .on("submit", function(ev) {
    ev.preventDefault();
    var idDiv = ev.target.id;

    if(idDiv=="formContact"){
       loadingSend()
       ajaxSendContact(idDiv)

    }



  });

function ajaxSendContact(idDiv){
  $.ajax({
    url: '<?=site_url('mailer/contact');?>',
    type: 'post',
    dataType: 'json',
    data: $('#'+idDiv).serialize()
  })
  .success(function(data) {
    // console.log(data.status);
    setTimeout(function(){
      Swal.hideLoading()
      if(data.status=="ok"){
        Swal.fire(
          'Success',
          'Send mail already.',
          'success'
        ).then((result) => {
           $('#formContact')[0].reset();
        })
      }
     }, 2000);

  });


}


function loadingSend(){
Swal.fire({
  html: '<h6 style="font-size:2rem;font-style:italic;color:#3498db"><span style="font-size:36px;"><i class="far fa-envelope"></i></span> Sending ...</h6>',
  allowOutsideClick:false,
  onBeforeOpen: () => {
    Swal.showLoading()
  },
  onClose: () => {
  }
})
}



$(".menu li").hover(function(){
  $(this).find('.drop-down').slideDown(250);
  }, function(){
  $(this).find('.drop-down').slideUp( "fast", function() {
    // Animation complete.
  });
});

$(document).on('click','#ilang',function(){
   $('.imodal').fadeIn()
   $('body').addClass('nosroll')
})

$(document).on('click','.imodal',function(){
   $('.imodal').fadeOut(200)
   $('body').removeClass('nosroll')
})

	$('#nav-icon2').click(function(){
		$(this).toggleClass('open');
            $('.menu').toggleClass('open')
            $('header').toggleClass('open')
	});
  $(".gotop").click(function () {
   $("html, body").animate({scrollTop: 0}, 1000);
});



$('.back').click(function(){
   window.history.back();
})



var editable_elements = document.querySelectorAll("[contenteditable=true]");
for(var i=0; i<editable_elements.length; i++){
      editable_elements[i].setAttribute("contenteditable", false);
}



</script>
</body>
</html>
